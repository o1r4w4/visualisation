# Biigle Visualisation Module PHP Unit Tests

This directory contains the documentation of all 
unit tests for the php classes and the results.

In the `results` folder results are 
ordered by the date taken and the class tested.