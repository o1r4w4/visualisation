<?php

class VisualisationServiceProviderTest extends TestCase {

    public function testServiceProvider()
    {
        $this->assertTrue(
            class_exists(
                'Biigle\Modules\Visualisation\VisualisationServiceProvider')
        );
    }
    public function testVisualisationVolumeController()
    {
        $this->assertTrue(
            class_exists(
                'Biigle\Modules\Visualisation\Http\Controllers\Views\VolumeController')
        );
    }
    public function testVisualisationLabelController()
    {
        $this->assertTrue(
            class_exists(
                'Biigle\Modules\Visualisation\Http\Controllers\Api\VisualisationLabelController')
        );
    }
}