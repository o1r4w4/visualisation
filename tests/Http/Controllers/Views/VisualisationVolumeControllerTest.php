<?php

namespace Biigle\Tests\Modules\Visualisation\Http\Controllers\Views;

use ApiTestCase;

class VisualisationVolumeControllerTest extends ApiTestCase
{
    public function testShow()
    {
        $id = $this->volume()->id;

        // not logged in
        $response = $this->get("volumes/{$id}/visualisation");
        $response->assertStatus(302);

        // doesn't belong to project
        $this->beUser();
        $response = $this->get("volumes/{$id}/visualisation");
        $response->assertStatus(403);

        $this->beEditor();
        $response = $this->get("volumes/{$id}/visualisation");
        $response->assertStatus(200);
    }
}
