<?php

namespace Biigle\Tests\Modules\Visualisation\Http\Controllers\Api;

use ApiTestCase;
use Carbon\Carbon;
use Biigle\Tests\LabelTest;
use Biigle\Tests\ImageTest;
use Biigle\Tests\AnnotationTest;
use Biigle\Tests\AnnotationLabelTest;
use Biigle\Tests\AnnotationSessionTest;

class VisualisationLabelControllerTest extends ApiTestCase
{
    public function testIndex()
    {
        $tid = $this->volume()->id;

        $image = ImageTest::create(['volume_id' => $tid]);
        $annotation = AnnotationTest::create(['image_id' => $image->id]);
        $label = LabelTest::create();
        AnnotationLabelTest::create([
            'annotation_id' => $annotation->id,
            'label_id' => $label->id,
        ]);
        // image ID should be returned only once, no matter how often the label is present,
        // count should be 2
        AnnotationLabelTest::create([
            'annotation_id' => $annotation->id,
            'label_id' => $label->id,
        ]);

        $lid = $label->id;

        // this image shouldn't appear
        $image2 = ImageTest::create(['volume_id' => $tid, 'filename' => 'b.jpg']);
        $annotation = AnnotationTest::create(['image_id' => $image2->id]);
        AnnotationLabelTest::create([
            'annotation_id' => $annotation->id,
            'user_id' => $this->admin()->id,
        ]);
        $this->doTestApiRoute('GET', "/api/v1/volumes/{$tid}/images/filter/visualisation-label/{$lid}");

        $this->beUser();
        $response = $this->get("/api/v1/volumes/{$tid}/images/filter/visualisation-label/{$lid}");
        $response->assertStatus(403);

        $id = strval($image->id);
        $expect = array(
            array("count(annotation_labels.label_id)"=>"2","attrs"=>null, "id"=>$id)
        );

        $this->beGuest();
        $response = $this->get("/api/v1/volumes/{$tid}/images/filter/visualisation-label/{$lid}")
            ->assertExactJson($expect);
        $response->assertStatus(200);
    }
}
