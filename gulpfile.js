"use strict"

var gulp = require('gulp');
var h = require('gulp-helpers');
var publish = h.publish('Biigle\\Modules\\Visualisation\\VisualisationServiceProvider');

h.paths.public = 'src/public/assets/';
h.paths.js = 'src/resources/assets/js/';
h.paths.sass = 'src/resources/assets/sass/';

gulp.task('sass', function () {
    h.sass('main.scss', 'main.css');
});

gulp.task('js-main', function (cb) {
    h.js('visualisation/**/*.js', 'main.js', cb);
});
gulp.task('js-volumes', function (cb) {
    h.js('volumes/**/*.js', 'volumes.js', cb);
});

gulp.task('js', ['js-main', 'js-volumes']);

gulp.task('watch', function () {
    gulp.watch(h.paths.sass + '**/*.scss', ['sass']);
    gulp.watch(h.paths.js + 'visualisation/**/*.js', ['js-main']);
    gulp.watch(h.paths.js + 'volumes/**/*.js', ['js-volumes']);
    gulp.watch(h.paths.public + '**/*', publish);
});

gulp.task('default', ['sass', 'js'], publish)
