<?php

namespace Biigle\Modules\Visualisation\Http\Controllers\Api;

use Biigle\Volume;
use Biigle\Annotation;
use Illuminate\Contracts\Auth\Guard;
use Biigle\Http\Controllers\Api\Controller;

class VisualisationLabelController extends Controller
{
    /**
     * List the IDs of images having one or more annotations with the specified label
     * and count the annotations per image.
     *
     * @api {get} volumes/:tid/images/filter/annotation-label/:lid Get all images having annotations with a certain label
     * @apiGroup Volumes
     * @apiName VolumeImagesHasLabel
     * @apiPermission projectMember
     * @apiDescription Returns IDs of images having one or more annotations with the specified label. If there is an active annotation session, images with annotations hidden by the session are not returned.
     *
     * @apiParam {Number} tid The volume ID
     * @apiParam {Number} lid The label ID
     *
     * @apiSuccessExample {json} Success response:
     * [1, 5, 6]
     *
     * @param Guard $auth
     * @param  int  $tid
     * @param  int  $lid
     * @return \Illuminate\Http\Response
     */
    public function index(Guard $auth, $tid, $lid)
    {
        $volume = Volume::findOrFail($tid);
        $this->authorize('access', $volume);

        $user = $auth->user();
        $session = $volume->getActiveAnnotationSession($user);

        if ($session) {
            $query = Annotation::allowedBySession($session, $user);
        } else {
            $query = Annotation::getQuery();
        }
        return $query
            ->join('images', 'images.id', '=', 'annotations.image_id')
            ->where('images.volume_id', $tid)
            ->join('annotation_labels', 'annotations.id', '=', 'annotation_labels.annotation_id')
            ->where('annotation_labels.label_id', $lid)
            ->groupBy('images.id', 'annotation_labels.label_id')
            ->selectRaw('images.id, images.attrs, count(annotation_labels.label_id)')
            ->get();
    }
}
