<?php
//Route::get('quotes', array(
//    'as'   => 'quotes',
//    'uses' => '\Biigle\Modules\Quotes\Http\Controllers\QuotesController@index'
//));
$router->group([
        'middleware' => 'auth',
        'namespace' => 'Views',
    ], function ($router) {

    $router->get('volumes/{id}/visualisation', [
        'as'   => 'volume-visualisation',
        'uses' => 'VolumeController@show',
    ]);
//    $router->get('volumes/{id}/visualisation-largo', [
//        'as'   => 'volume-visualisation-largo',
//        'uses' => 'LargoController@show',
//    ]);
//    $router->get('visualisation', [
//        'as'   => 'visualisation',
//        'uses' => 'VisualisationController@index',
//    ]);
});
$router->group([
    'middleware' => 'auth:web,api',
    'prefix' => 'api/v1',
    'namespace' => 'Api',
], function ($router) {

    $router->get('volumes/{id}/images/filter/visualisation-label/{id2}', [
        'uses' => 'VisualisationLabelController@index',
    ]);
//    $router->get('volumes/visualisation-tests/{data}', [
//        'uses' => 'VisualisationTestController@index',
//    ]);
});