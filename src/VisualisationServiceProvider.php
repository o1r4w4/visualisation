<?php

namespace Biigle\Modules\Visualisation;

use Biigle\Services\Modules;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class VisualisationServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application events.
     *
     * @param  \Biigle\Services\Modules  $modules
     * @param  \Illuminate\Routing\Router  $router
     *
     * @return void
     */
    public function boot(Modules $modules, Router $router)   {

        $this->loadViewsFrom(__DIR__.'/resources/views', 'visualisation');

        $this->publishes([
            __DIR__.'/public/assets' => public_path('vendor/visualisation'),
        ], 'public');

        $router->group([
            'namespace' => 'Biigle\Modules\Visualisation\Http\Controllers',
            'middleware' => 'web',
        ], function ($router) {
            require __DIR__.'/Http/routes.php';
        });
        $modules->register('visualisation', [
            'viewMixins' => [
                'volumesSidebar',
                'volumesScripts',
                'manualTutorial',
            ],
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('command.visualisation.publish', function ($app) {
            return new \Biigle\Modules\Visualisation\Console\Commands\Publish();
        });
        $this->commands('command.visualisation.publish');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'command.visualisation.publish',
        ];
    }
}
