var CHART = 'chart';
var VIS_BLANK = 'blank';
var VIS_BLOCK = 'block';
var VIS_SCATTER = 'scatter';
var VIS_LINE = 'line';
var VIS_NET = 'net';
var VIS_RADAR = 'radar';

var SIZE = 'size';
var MARGIN = 'margin';
var FONT = 'font';

var COLOR = 'color';
var NUMBER = 'number';
var TEXT = 'text';
var GROUP = 'group';
var CHECK = 'checkbox';
var RADIO = 'radio';

var NAME_BLOCK = 'Histogram';
var NAME_SCATTER = 'Scatter plot';
var NAME_LINE = 'Parallel coordinates';
var NAME_NET = 'Netmap display';
var NAME_RADAR = 'Radar display';
var NAME_GDP = 'Generalized drafter\'s plot';

var WIDTH = 'width';
var HEIGHT = 'height';
var USE_SIZE = 'useSize';

var MARGIN_TOP = 'marginTop';
var MARGIN_RIGHT = 'marginRight';
var MARGIN_BOT = 'marginBot';
var MARGIN_LEFT = 'marginLeft';

var FONT_SIZE_HEAD = 'fontSizeHead';
var FONT_SIZE_LABEL = 'fontSizeLabel';
var FONT_SIZE_TICK = 'fontSizeTick';

var TEXT_SPACE = ' ';
var TEXT_ANNO_PER_IMG = 'Annotations per Image';
var TEXT_NUMB_OF_IMGS = 'Number of Images';
var TEXT_LABELS = 'Labels';
var TEXT_HEADING_BLANK= 'Select label for visualisation';

var SETT_SHOW_VALUES = 'showValues';
var SETT_SHOW_0_VALUES = 'show0Values';
var SETT_GROUP = 'grouping';
var SETT_BLOCK_SCALE_X = 'blockScaleX';
var SETT_BLOCK_SCALE_Y = 'blockScaleY';
var SETT_SCATTER_SCALE_X = 'scatterScaleX';
var SETT_SCATTER_SCALE_Y = 'scatterScaleY';
var SETT_LINE_DRAW = 'lineDraw';
var SETT_LINE_SCALE_X = 'lineScaleX';
var SETT_LINE_SCALE_Y = 'lineScaleY';
var SETT_RADAR_SCALE = 'radarScale';
var SETT_AND_SEL = 'andSel';
var SETT_AREA_ANNO = 'areaAnno';
var SETT_RADIUS_MIN = 'radiusMin';
var SETT_RADIUS_MAX = 'radiusMax';

var COL_FONT_HEADING = 'fontHeading';
var COL_FONT_LABEL = 'fontLabel';
var COL_FONT_ELEMENT = 'fontElement';
var COL_FONT_TICK = 'fontTick';

var COL_MAIN = 'main';
var COL_MAIN_SEL = 'mainSel';
var COL_SEC = 'sec';
var COL_SEC_SEL = 'secSel';

var BAND = 'band';
var LIN = 'lin';
var LOG = 'log';
var CARD = 'card';

var TEST_STATE_FIRST = 0;
var TEST_STATE_SECOND = 1;
var TEST_STATE_EVAL = 2;
var TEST_STATE_END = 3;

var SCALE_LIN = {
    type: LIN,
    yOffset:false
};
var SCALE_BAND =  {
    type: BAND,
    yOffset:false,
    padding: 0.1
};
var SCALE_LOG = {
    type: LOG,
    yOffset:true,
    base: 2
};

var VIS_SEL_BLOCK = {
    name: VIS_BLOCK,
    text: NAME_BLOCK,
};
var VIS_SEL_SCATTER = {
    name: VIS_SCATTER,
    text: NAME_GDP,
};
var VIS_SEL_LINE = {
    name: VIS_LINE,
    text: NAME_LINE,
};
var VIS_SEL_NET = {
    name: VIS_NET,
    text: NAME_NET,
};
var VIS_SEL_RADAR = {
    name: VIS_RADAR,
    text: NAME_RADAR,
};


// module.exports = {
//     CHART: CHART,
//     GROUP: GROUP,
//     CHECK: CHECK,
//     COLOR: COLOR,
//     RADIO: RADIO,
//     NUMBER: NUMBER,
//     TEXT: TEXT,
//     TEST_STATE_FIRST: TEST_STATE_FIRST,
//     TEST_STATE_SECOND: TEST_STATE_SECOND,
//     TEST_STATE_EVAL: TEST_STATE_EVAL,
//     TEST_STATE_END: TEST_STATE_END,
//     VIS_BLANK: VIS_BLANK,
//     VIS_SEL_BLOCK: VIS_SEL_BLOCK,
//     VIS_SEL_SCATTER: VIS_SEL_SCATTER,
//     VIS_SEL_LINE: VIS_SEL_LINE,
//     VIS_SEL_NET: VIS_SEL_NET,
//     VIS_SEL_RADAR: VIS_SEL_RADAR,
//     SETT_AND_SEL: SETT_AND_SEL,
// };