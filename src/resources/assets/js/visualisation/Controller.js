/**
 * controller for visualisation
 */
biigle.$viewModel('visualisation-controller', function (element) {
    var events = biigle.$require('events');
    var volume = biigle.$require('visualisation.volume');
    var imagesWithLabel = biigle.$require('visualisation.api.imagesWithLabel');
    var messages = biigle.$require('messages.store');
    var imageKey = 'biigle.visualisation.imageSequence.' + volume.id;
    var colorKey = 'biigle.visualisation.colorGroups';
    var settingsKey = 'biigle.visualisation.settings';
    new Vue({
        el: element,
        data: {
            labelTrees: biigle.$require('visualisation.labelTrees'),
            allImages: biigle.$require('visualisation.images'),
            selectModel: createSelectModel(),
            sizeModels: createSizeModels(),
            colorModels: createColorModels(),
            settingsModels: createSettingsModels(),
            exportModel: createExportModel(),
            testModel: createTestModel(),
            colors: getDefColors(colorKey),
            settings: getDefSettings(settingsKey),
        },
        computed: {
            selectedImages: function () {
                return JSON.parse(localStorage.getItem(imageKey)) || [];
            },
            images: function () {
                if (this.selectedLabels.length > 0) {
                    var self = this;
                    return this.allImages.filter(function (item) {
                        return self.curImageIds.indexOf(item.id) !== -1;
                    });
                }
                return this.allImages;
            }
        },
        components: {
            sidebar: biigle.$require('core.components.sidebar'),
            sidebarTab: biigle.$require('core.components.sidebarTab'),
            labelTrees: biigle.$require('labelTrees.components.labelTrees'),
            visualisationSelect:
                biigle.$require('visualisation.components.visualisationSelect'),
            visualisationChart:
                biigle.$require('visualisation.components.visualisationChart'),
            visualisationValueEdit:
                biigle.$require('visualisation.components.visualisationValueEdit'),
            visualisationValueEditGroup:
                biigle.$require('visualisation.components.visualisationValueEditGroup')
        },
        methods: {
            handleSelectedLabel: function (label) {
                this.labelHandler.updateLabel(label, events, messages,volume.id, this);
            },
            handleDeselectedLabel: function (label) {
                this.labelHandler.removeLabel(label, this);
            },
            handleClearedLabels: function () {
                this.labelHandler.clearLabels(this);
            },
            updateSelectModels: function () {
                this.selectHandler.updateModels(this.labelHandler.selectedLabels.length, this);
            },
            showVisualisation: function (update, name) {
                if(this.blocked)return;
                this.updateSelectedImages();
                this.checkToggleSettings(name);
                this.exportHandler.updateName(name, this.labelHandler.selectedLabels);
                var data = null;
                var callBack = this.chart.updateBlankChart;
                switch (name){
                    case VIS_BLOCK:
                        data = this.dataHandler.getBlockData(
                            this.labelHandler.selectedLabels[0],
                            this.settings[SETT_GROUP],
                            this.settings[SETT_SHOW_0_VALUES]);
                        callBack = this.chart.updateBlockChart;
                        break;
                    case VIS_SCATTER:
                        data = this.dataHandler.getGDPData(
                            this.labelHandler.selectedLabels,
                            this.settings[SETT_GROUP],
                            this.settings[SETT_AREA_ANNO],
                            this.settings[SETT_SHOW_0_VALUES]);
                        callBack = this.chart.updateGDPChart;
                        break;
                    case VIS_LINE:
                        data = this.dataHandler.getLineData(
                            this.labelHandler.selectedLabels,
                            this.settings[SETT_GROUP],
                            this.settings[SETT_AREA_ANNO]);
                        callBack = this.chart.updateLineChart;
                        break;
                    case VIS_NET:
                        data = this.dataHandler.getNetData(
                            this.labelHandler.selectedLabels,
                            this.settings[SETT_GROUP]);
                        callBack = this.chart.updateNetChart;
                        break;
                    case VIS_RADAR:
                        data = this.dataHandler.getRadarData(
                            this.labelHandler.selectedLabels,
                            this.settings[SETT_GROUP]);
                        callBack = this.chart.updateRadarChart;
                        break;
                }
                this.$nextTick(function () {
                    callBack(data);
                    if (update)this.updateSelect();
                });
            },
            checkToggleSettings: function (name) {
                var model;
                for (var key0 in this.colorModels){
                    model = this.colorModels[key0];
                    model.show = model.name == name || model.name == CHART;
                }
                for (var i=0;i<this.settingsModels.length;i++){
                    model = this.settingsModels[i];
                    model.show = model.name == name || model.name == CHART;
                }
            },
            updateSelect: function () {
                if(this.blocked)return;
                var current = this.selectHandler.getCurrent();
                this.selectModel.models = current.getVis();
                this.selectModel.value = current.getCurVis();
            },
            handleSelectChanged: function (model) {
                if(this.blocked)return;
                var current = this.selectHandler.getCurrent();
                current.updateVisualisation(model.value, this);
            },
            redraw: function () {
                this.updateSelectedImages();
                this.$nextTick(function () {
                    this.chart.redraw();
                });
            },
            sideBarToggle: function () {
                this.$nextTick(function () {
                    this.redraw();
                });
            },
            updateSelectedImages: function (ids) {
                if(ids == null)
                    ids = [];
                localStorage.setItem(imageKey, JSON.stringify(ids));
            },
            chartSizeUpdate: function (width, height) {
                this.sizeModel.get(WIDTH).value = width;
                this.sizeModel.get(HEIGHT).value = height;
            },
            handleColorChange: function (model) {
                for (var key in model.models)
                    this.colors[model.name][key] = model.models[key].value;
                if(this.selectHandler.checkVisualisationName(model.name))
                    this.showVisualisation(false,
                        this.selectHandler.getCurrent().getCurVisName());
                localStorage.setItem(colorKey, JSON.stringify(this.colors));
            },
            handleSettingsChange: function (model, multi) {
                if(model.name == SIZE)this.settings[USE_SIZE] = true;
                if(multi){
                    for (var key in model.models){
                        var subModel = model.models[key];
                        this.settings[subModel.name] = subModel.value;
                    }
                }else {
                    this.settings[model.name] = model.value;
                }
                if(this.selectHandler.checkVisualisationName(model.visualisation)){
                    this.showVisualisation(false,
                        this.selectHandler.getCurrent().getCurVisName());
                }
                localStorage.setItem(settingsKey, JSON.stringify(this.settings));
            },
            handleResetSettings: function (model) {
                if(model.name == SIZE)this.settings[USE_SIZE] = false;
                this.showVisualisation(false,
                    this.selectHandler.getCurrent().getCurVisName());
                localStorage.setItem(settingsKey, JSON.stringify(this.settings));
            },
            handleExport: function (type) {
                this.exportHandler.export(type);
            },
            updateColorModels: function () {
                console.log('colors loaded');
                for (var key0 in this.colorModels)
                    if(this.colors[key0]!=null){
                        var group = this.colors[key0];
                        model = this.colorModels[key0];
                        for (var key1 in model.models)
                            if(group[key1]!=null)
                                model.models[key1].value = group[key1];
                    }
            },
            updateSettingsModels: function () {
                console.log('settings loaded');
                var i, key0;
                for (i=0;i< this.sizeModels.length;i++){
                    model = this.sizeModels[i];
                    for (key0 in model.models){
                        if(this.settings[key0]!=null)
                            model.models[key0].value = this.settings[key0];
                    }
                }
                for (i=0;i< this.settingsModels.length;i++){
                    model = this.settingsModels[i];
                    for (key0 in model.models)
                        if(this.settings[key0]!=null)
                            model.models[key0].value = this.settings[key0];
                }

            }
        },
        created: function () {
            this.curImageIds = [];

            window.addEventListener("resize", this.redraw);
        },
        mounted: function () {
            this.chart = this.$refs.visualisation_chart;

            this.dataHandler = new VisualisationDataHandler(this.allImages);
            this.selectHandler = new VisualisationSelectHandler();
            this.labelHandler = new VisualisationLabelHandler(imagesWithLabel);
            this.exportHandler = new VisualisationExportHandler(volume.name,
                this.exportModel);

            if(COLORS_LOADED)
                this.updateColorModels();
            if(SETTINGS_LOADED)
                this.updateSettingsModels();
            this.sizeModel = this.sizeModels[0];

            this.updateSelectModels();
        }
    });
});
