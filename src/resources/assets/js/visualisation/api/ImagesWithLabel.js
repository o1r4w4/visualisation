/**
 * Resource for images having annotations with a certain label.
 *
 * var resource = biigle.$require('visualisation.api.imageWithLabel');
 *
 * Get image IDs:
 *
 * resource.query({tid: volumeId, lid: labelId}, {}).then(...)
 *
 * @type {Vue.resource}
 */
biigle.$declare('visualisation.api.imagesWithLabel', Vue.resource(
    'api/v1/volumes{/tid}/images/filter/visualisation-label{/lid}', {}
));