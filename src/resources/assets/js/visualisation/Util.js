var createSizeModels = function () {
    var sizeModels = [];

    var sizeModel = new VisualisationGroupModel(SIZE, 'Size');
    sizeModels.push(sizeModel);
    var widthModel = new VisualisationValueModel(WIDTH, 0, NUMBER, 'Width');
    sizeModel.add(widthModel);
    var heightModel = new VisualisationValueModel(HEIGHT, 0, NUMBER, 'Height');
    sizeModel.add(heightModel);

    var marginModel = new VisualisationGroupModel(MARGIN, 'Margin', true);
    sizeModels.push(marginModel);
    var topModel = new VisualisationValueModel(MARGIN_TOP, 64, NUMBER, 'Top');
    marginModel.add(topModel);
    var rightModel = new VisualisationValueModel(MARGIN_RIGHT, 64, NUMBER, 'Right');
    marginModel.add(rightModel);
    var botModel = new VisualisationValueModel(MARGIN_BOT, 64, NUMBER, 'Bot');
    marginModel.add(botModel);
    var leftModel = new VisualisationValueModel(MARGIN_LEFT, 64, NUMBER, 'Left');
    marginModel.add(leftModel);

    var fontModel = new VisualisationGroupModel(FONT, 'Font', true);
    sizeModels.push(fontModel);
    var headModel = new VisualisationValueModel(FONT_SIZE_HEAD, 24,
        NUMBER, 'Heading');
    fontModel.add(headModel);
    var labelModel = new VisualisationValueModel(FONT_SIZE_LABEL, 16,
        NUMBER, 'Label');
    fontModel.add(labelModel);
    var tickModel = new VisualisationValueModel(FONT_SIZE_TICK, 10,
        NUMBER, 'Tick');
    fontModel.add(tickModel);

    return sizeModels;
};

var createColorModels = function () {
    var colorModels = [];

    var chartModel = new VisualisationGroupModel(CHART, 'Chart', true);
    colorModels.push(chartModel);
    var bgModel = new VisualisationValueModel(CHART, '#ffffff', COLOR, 'Backgroud');
    chartModel.add(bgModel);
    var headModel = new VisualisationValueModel(COL_FONT_HEADING, '#000000',
        COLOR, 'Heading');
    chartModel.add(headModel);
    var labelModel = new VisualisationValueModel(COL_FONT_LABEL, '#000000',
        COLOR, 'Label');
    chartModel.add(labelModel);
    var elemModel = new VisualisationValueModel(COL_FONT_ELEMENT, '#ffffff',
        COLOR, 'Element');
    chartModel.add(elemModel);
    var tickModel = new VisualisationValueModel(COL_FONT_TICK, '#000000',
        COLOR, 'Tick');
    chartModel.add(tickModel);

    var blockModel = new VisualisationGroupModel(VIS_BLOCK, NAME_BLOCK, true);
    colorModels.push(blockModel);
    var blockMainModel = new VisualisationValueModel(COL_MAIN, '#4682b4',
        COLOR, 'Bar');
    blockModel.add(blockMainModel);
    var blockMainSelModel = new VisualisationValueModel(COL_MAIN_SEL, '#339933',
        COLOR, 'Bar selected');
    blockModel.add(blockMainSelModel);
    var blockBackModel = new VisualisationValueModel(COL_SEC, '#000000',
        COLOR, 'Back');
    blockModel.add(blockBackModel);
    var blockBackSelModel = new VisualisationValueModel(COL_SEC_SEL, '#555555',
        COLOR, 'Back selected');
    blockModel.add(blockBackSelModel);

    var scatterModel = new VisualisationGroupModel(VIS_SCATTER, NAME_SCATTER, true);
    colorModels.push(scatterModel);
    var scMainModel = new VisualisationValueModel(COL_MAIN, '#4682b4',
        COLOR, 'Circle');
    scatterModel.add(scMainModel);
    var scMainSelModel = new VisualisationValueModel(COL_MAIN_SEL, '#339933',
        COLOR, 'Circle selected');
    scatterModel.add(scMainSelModel);

    var lineModel = new VisualisationGroupModel(VIS_LINE, NAME_LINE, true);
    colorModels.push(lineModel);
    var lineMainModel = new VisualisationValueModel(COL_MAIN, '#4682b4',
        COLOR, 'Point');
    lineModel.add(lineMainModel);
    var lineMainSelModel = new VisualisationValueModel(COL_MAIN_SEL, '#339933',
        COLOR, 'Point selected');
    lineModel.add(lineMainSelModel);
    var lineBackModel = new VisualisationValueModel(COL_SEC, '#555555',
        COLOR, 'Line');
    lineModel.add(lineBackModel);
    var lineBackSelModel = new VisualisationValueModel(COL_SEC_SEL, '#4682b4',
        COLOR, 'Line selected');
    lineModel.add(lineBackSelModel);

    var netModel = new VisualisationGroupModel(VIS_NET, NAME_NET, true);
    colorModels.push(netModel);
    var netMainModel = new VisualisationValueModel(COL_MAIN, '#4682b4',
        COLOR, 'Line');
    netModel.add(netMainModel);
    var netMainSelModel = new VisualisationValueModel(COL_MAIN_SEL, '#339933',
        COLOR, 'Line selected');
    netModel.add(netMainSelModel);
    var netBackModel = new VisualisationValueModel(COL_SEC, '#555555',
        COLOR, 'Fill');
    netModel.add(netBackModel);
    var netBackSelModel = new VisualisationValueModel(COL_SEC_SEL, '#4682b4',
        COLOR, 'Fill selected');
    netModel.add(netBackSelModel);

    var radModel = new VisualisationGroupModel(VIS_RADAR, NAME_RADAR, true);
    colorModels.push(radModel);
    var radMainModel = new VisualisationValueModel(COL_MAIN, '#4682b4',
        COLOR, 'Point');
    radModel.add(radMainModel);
    var radMainSelModel = new VisualisationValueModel(COL_MAIN_SEL, '#339933',
        COLOR, 'Point selected');
    radModel.add(radMainSelModel);
    var radBackModel = new VisualisationValueModel(COL_SEC, '#555555',
        COLOR, 'Line');
    radModel.add(radBackModel);
    var radBackSelModel = new VisualisationValueModel(COL_SEC_SEL, '#4682b4',
        COLOR, 'Line selected');
    radModel.add(radBackSelModel);

    return colorModels;
};

var createSettingsModels = function () {
    var settingsModels = [];

    var checkBoxModels = new VisualisationGroupModel(CHART, 'Chart');
    checkBoxModels.showUpdate = false;
    checkBoxModels.showReset = false;
    checkBoxModels.updateValue = true;
    settingsModels.push(checkBoxModels);

    var showValueModel = new VisualisationValueModel(SETT_SHOW_VALUES, true,
        CHECK, 'Show values');
    showValueModel.visualisation = CHART;
    checkBoxModels.add(showValueModel);
    var show0ValueModel = new VisualisationValueModel(SETT_SHOW_0_VALUES, true,
        CHECK, 'Show 0 values');
    show0ValueModel.visualisation = CHART;
    show0ValueModel.updateData = true;
    checkBoxModels.add(show0ValueModel);
    var andSelModel = new VisualisationValueModel(SETT_AND_SEL, true,
        CHECK, 'AND selection');
    andSelModel.visualisation = CHART;
    checkBoxModels.add(andSelModel);
    var areaAnnoModel = new VisualisationValueModel(SETT_AREA_ANNO, false,
        CHECK, 'Anno/Area');
    areaAnnoModel.visualisation = CHART;
    areaAnnoModel.updateData = true;
    checkBoxModels.add(areaAnnoModel);

    var groupGroupModel = new VisualisationGroupModel(CHART, 'Grouping', true);
    groupGroupModel.visualisation = CHART;
    groupGroupModel.updateData = true;
    settingsModels.push(groupGroupModel);
    var groupModel = new VisualisationValueModel(SETT_GROUP, 1,
        NUMBER, 'Axis grouping');
    groupGroupModel.add(groupModel);

    var radiusModel = new VisualisationGroupModel(CHART, 'Radius range', true);
    radiusModel.visualisation = CHART;
    settingsModels.push(radiusModel);
    var radiusMinModel = new VisualisationValueModel(SETT_RADIUS_MIN, 8,
        NUMBER, 'Radius min');
    radiusModel.add(radiusMinModel);
    var radiusMaxModel = new VisualisationValueModel(SETT_RADIUS_MAX, 16,
        NUMBER, 'Radius min');
    radiusModel.add(radiusMaxModel);

    var blockModel = new VisualisationGroupModel(VIS_BLOCK, NAME_BLOCK, true);
    blockModel.showUpdate = false;
    blockModel.showReset = false;
    blockModel.updateValue = true;
    settingsModels.push(blockModel);
    var blockScaleModel = new VisualisationRadioModel(SETT_BLOCK_SCALE_Y, SCALE_LIN, 'Y scale');
    blockScaleModel.visualisation = VIS_BLOCK;
    blockScaleModel.addRadio(SCALE_LIN, 'Lin', 'Linear');
    blockScaleModel.addRadio(SCALE_LOG, 'Log', 'Logarithmic');
    blockModel.add(blockScaleModel);

    var lineModel = new VisualisationGroupModel(VIS_LINE, NAME_LINE, true);
    lineModel.showUpdate = false;
    lineModel.showReset = false;
    lineModel.updateValue = true;
    settingsModels.push(lineModel);
    var lineDrawModel = new VisualisationRadioModel(SETT_LINE_DRAW, CARD, 'Draw mode');
    lineDrawModel.visualisation = VIS_LINE;
    lineDrawModel.addRadio(LIN, 'Lin', 'Linear');
    lineDrawModel.addRadio(CARD, 'Card', 'Cardinal');
    lineModel.add(lineDrawModel);

    return settingsModels;
};

var createSelectModel = function () {
    return new VisualisationGroupModel('lineModels', NAME_LINE, true);
};

var createExportModel = function () {
    return new VisualisationValueModel(CHART, '', TEXT, 'Filename');
};

var createTestModel = function(){
    var nameModel = new VisualisationValueModel('Name', null, TEXT);

    var textModel = new VisualisationValueModel('Text', '', TEXT);

    var selModel = new VisualisationGroupModel('selModel');
    selModel.value = new VisualisationValueModel(1, 1, 'number', 'img01');
    selModel.add(selModel.value);
    for(var i=2;i<21;i++){
        if(i<10)
            selModel.add(new VisualisationValueModel(i, i, 'number', 'img0'+i));
        else
            selModel.add(new VisualisationValueModel(i, i, 'number', 'img'+i));
    }

    var radioModel = new VisualisationRadioModel('radModel', 3);
    radioModel.addRadio(5, 'Extremely');
    radioModel.addRadio(4, 'Very');
    radioModel.addRadio(3, 'Moderately');
    radioModel.addRadio(2, 'Slightly');
    radioModel.addRadio(1, 'Not at all');
    var radioShowModel = new VisualisationRadioModel('showModel', 'time', 'Value');
    radioShowModel.addRadio('time', 'Time');
    radioShowModel.addRadio('quota', 'Quota');
    radioShowModel.addRadio('note', 'Note');
    var radioShowAxisModel = new VisualisationRadioModel('showModelAxis', 'tester', 'Axis');
    radioShowAxisModel.addRadio('tester', 'Tester');
    radioShowAxisModel.addRadio('quest', 'Question');

    var numberShowModel = new VisualisationGroupModel('group', null, true);
    numberShowModel.add(new VisualisationValueModel('group', 1, NUMBER, 'Group'));
    return {
        title: null,
        quest: null,
        hint: null,
        hideHead: false,
        hideBody: true,
        text: textModel,
        testing: true,
        name: nameModel,
        select: selModel,
        selections:[],
        radio: radioModel,
        radioShow: radioShowModel,
        radioShowAxis: radioShowAxisModel,
        numberShow: numberShowModel
    };
};

var SETTINGS_LOADED = false;
var getDefSettings = function(key){
    var settings = JSON.parse(localStorage.getItem(key));
    if(settings!=null){
        SETTINGS_LOADED = true;
        return settings;
    }
    settings = {};
    settings[SETT_SHOW_VALUES] = true;
    settings[SETT_SHOW_0_VALUES] = true;
    settings[SETT_AND_SEL] = true;
    settings[SETT_AREA_ANNO] = false;
    settings[SETT_GROUP] = 1;
    settings[SETT_RADIUS_MIN] = 8;
    settings[SETT_RADIUS_MAX] = 16;

    settings[SETT_BLOCK_SCALE_X] = SCALE_BAND;
    settings[SETT_BLOCK_SCALE_Y] = SCALE_LIN;

    settings[SETT_SCATTER_SCALE_X] = SCALE_LIN;
    settings[SETT_SCATTER_SCALE_Y] = SCALE_LIN;

    settings[SETT_LINE_DRAW] = CARD;
    settings[SETT_LINE_SCALE_X] = SCALE_LIN;
    settings[SETT_LINE_SCALE_Y] = SCALE_LIN;

    settings[SETT_RADAR_SCALE] = SCALE_LIN;


    settings[WIDTH] = 0;
    settings[HEIGHT] = 0;
    settings[USE_SIZE] = false;

    settings[MARGIN_TOP] = 64;
    settings[MARGIN_RIGHT] = 64;
    settings[MARGIN_BOT] = 64;
    settings[MARGIN_LEFT] = 64;

    settings[FONT_SIZE_HEAD] = 24;
    settings[FONT_SIZE_LABEL] = 16;
    settings[FONT_SIZE_TICK] = 10;

    return settings;
};


var getDefTests = function () {
    var test0 = new VisualisationTestModel();
    var values = [
        [
            ['exactly 1 Habitat'],
            [3, 16]
        ],
        [
            [
                'less then 4 Crinoidea',
                'more then 2 Echinodermata',
            ],
            [2, 7, 19]
        ],
        [
            [
                'exactly 1 Chordata',
                'more then 1 Mollusca',
                'less then 3 Arthropoda',
            ],
            [8, 20]
        ],
    ];
    var i;
    for(i=0;i<values.length;i++)
        test0.questions.push(new VisualisationMultiQuestModel(
            values[i][0],
            values[i][1]
        ));

    var test1 = new VisualisationTestModel();
    values = [
        [
            ['exactly 2 Unknown'],
            [2, 6, 12]
        ],
        [
            [
                'exactly 2 Bedforms',
                'more then 2 Relief',
            ],
            [3, 9, 17]
        ],
        [
            [
                'exactly 2 Annelida',
                'less then 4 Porifera',
                'more then 2 Holothuroidea',
            ],
            [1, 6, 15]
        ],
    ];
    for(i=0;i<values.length;i++)
        test1.questions.push(new VisualisationMultiQuestModel(
            values[i][0],
            values[i][1]
        ));

    var test2= new VisualisationTestModel();
    test2.questions.push(new VisualisationQuestModel(
        'The manual of this module is helpful.'
    ));
    test2.questions.push(new VisualisationQuestModel(
        'Learning to use this module is easy.'
    ));
    test2.questions.push(new VisualisationQuestModel(
        'Using this module is easy and intuitive.'
    ));
    test2.questions.push(new VisualisationQuestModel(
        'Using this module would improve my job performance.'
    ));
    test2.questions.push(new VisualisationQuestModel(
        'I would use this module frequently.'
    ));

    var test3 = new VisualisationTestModel();
    test3.questions.push(new VisualisationQuestModel(
        'Any other comments?'
    ));

    return [
        test0,test1,test2,test3
    ];
};
var COLORS_LOADED = false;
var getDefColors = function (key) {
    var colors = JSON.parse(localStorage.getItem(key));
    if(colors!=null){
        COLORS_LOADED = true;
        return colors;
    }
    colors= {};

    colors[CHART] = {};
    colors[CHART][CHART] = '#ffffff';
    colors[CHART][COL_FONT_HEADING] = '#000000';
    colors[CHART][COL_FONT_LABEL] = '#000000';
    colors[CHART][COL_FONT_ELEMENT] = '#ffffff';
    colors[CHART][COL_FONT_TICK] = '#000000';


    colors[VIS_BLOCK] = {};
    colors[VIS_BLOCK][COL_MAIN] = '#4682b4';
    colors[VIS_BLOCK][COL_MAIN_SEL] = '#339933';
    colors[VIS_BLOCK][COL_SEC] = '#000000';
    colors[VIS_BLOCK][COL_SEC_SEL] = '#555555';

    colors[VIS_SCATTER] = {};
    colors[VIS_SCATTER][COL_MAIN] = '#4682b4';
    colors[VIS_SCATTER][COL_MAIN_SEL] = '#339933';

    colors[VIS_LINE] = {};
    colors[VIS_LINE][COL_MAIN] = '#4682b4';
    colors[VIS_LINE][COL_MAIN_SEL] = '#339933';
    colors[VIS_LINE][COL_SEC] = '#555555';
    colors[VIS_LINE][COL_SEC_SEL] = '#4682b4';

    colors[VIS_RADAR] = {};
    colors[VIS_RADAR][COL_MAIN] = '#4682b4';
    colors[VIS_RADAR][COL_MAIN_SEL] = '#339933';
    colors[VIS_RADAR][COL_SEC] = '#555555';
    colors[VIS_RADAR][COL_SEC_SEL] = '#4682b4';

    colors[VIS_NET] = {};
    colors[VIS_NET][COL_MAIN] = '#4682b4';
    colors[VIS_NET][COL_MAIN_SEL] = '#339933';
    colors[VIS_NET][COL_SEC] = '#555555';
    colors[VIS_NET][COL_SEC_SEL] = '#4682b4';

    return colors;
};