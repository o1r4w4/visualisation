// var Constants = require('../Constants');
// var VIS_BLANK = Constants.VIS_BLANK;
// var VIS_SEL_BLOCK = Constants.VIS_SEL_BLOCK;
// var VIS_SEL_SCATTER = Constants.VIS_SEL_SCATTER;
// var VIS_SEL_LINE = Constants.VIS_SEL_LINE;
// var VIS_SEL_NET = Constants.VIS_SEL_NET;
// var VIS_SEL_RADAR = Constants.VIS_SEL_RADAR;

/**
 * class for handling the visualisation select.
 * has sub handler with selectable visualisation
 * for 0, 1, 2 and more then 2 selected labels
 * @constructor
 */
function VisualisationSelectHandler(){
    this.blankHandler = new VisualisationSelectSubHandler(
        []
    );

    this.singleHandler = new VisualisationSelectSubHandler(
        [
            VIS_SEL_BLOCK
        ]
    );
    this.dualHandler = new VisualisationSelectSubHandler(
        [
            VIS_SEL_SCATTER,
            VIS_SEL_LINE,
            VIS_SEL_NET,
        ]
    );
    this.multiHandler = new VisualisationSelectSubHandler(
        [
            VIS_SEL_SCATTER,
            VIS_SEL_LINE,
            VIS_SEL_NET,
            VIS_SEL_RADAR,
        ]
    );

    this.currentHandler = this.blankHandler;

    /**
     * update current handler according to given number of selected labels
     * and call sub handlers parseVisualisation function
     * @param {Number} count
     * number of selected labels
     * @param {Object} controller
     * visualisation controller
     */
    this.updateModels = function(count, controller){
        var oldHandler = this.currentHandler;
        switch (count){
            case 0:
                this.currentHandler = this.blankHandler;
                break;
            case 1:
                this.currentHandler = this.singleHandler;
                break;
            case 2:
                this.currentHandler = this.dualHandler;
                break;
            default:
                this.currentHandler = this.multiHandler;
                break;
        }
        this.currentHandler.parseVisualisation(oldHandler != this.currentHandler,
            oldHandler.getCurVisName(), controller);
    };

    /**
     * get current sub handler
     * @returns {VisualisationSelectSubHandler}
     */
    this.getCurrent = function () {
        return this.currentHandler;
    };

    /**
     * check if given name matches current visualisation name or 'chart'
     * @param {String} name
     * name to check
     * @returns {boolean}
     * true if match or equal to 'chart'
     */
    this.checkVisualisationName = function(name){
        if(name == 'chart')return true;
        return this.currentHandler.getCurVisName() == name;
    };
}

/**
 * class for handling the selectable visualisation for a number of labels.
 * @param{Array} visualisations
 * array of selectable visualisation
 * @constructor
 */
function VisualisationSelectSubHandler(visualisations){
    this.visualisations = visualisations;
    this.curVisName = VIS_BLANK;
    this.curVis=null;

    if(visualisations.length>0){
        this.curVis = this.visualisations[0];
        this.curVisName = this.curVis.name;
    }

    /**
     * check if old visualisation is available or current visualisation is null
     * and set current visualisation to old or first one
     * then call showVisualisation of controller
     * @param {boolean} update
     * true if visualisation select should be updated
     * @param {String} oldVisName
     * old visualisation name to check for
     * @param {Object} controller
     * visualisation controller
     */
    this.parseVisualisation = function(update, oldVisName, controller){
        if(oldVisName!=null)
            for(var i=0;i<this.visualisations.length;i++) {
                var vis = this.visualisations[i];
                if (vis.name == oldVisName) {
                    this.curVis = vis;
                    this.curVisName = oldVisName;
                    break;
                }
            }
        if(this.curVis==null && this.visualisations.length>0){
            this.curVis = this.visualisations[0];
            this.curVisName = this.curVis.name;
        }
        controller.showVisualisation(update, this.curVisName);
    };

    /**
     * set current visualisation and call showVisualisation of controller
     * if model is different to current visualisation and not null
     * @param {Object} model
     * model to set current visualisation to
     * @param {Object} controller
     * visualisation controller
     */
    this.updateVisualisation = function(model, controller){
        if(this.curVis != model && model != null){
            this.curVis = model;
            this.curVisName = model.name;
            controller.showVisualisation(false, this.curVisName);
        }
    };

    /**
     * get the name of current visualisation
     * @returns {String}
     */
    this.getCurVisName = function(){
        return this.curVisName;
    };

    /**
     * get array of visualisations
     * @returns {Array}
     */
    this.getVis = function(){
        return this.visualisations;
    };

    /**
     * get current visualisation
     * @returns {Object}
     */
    this.getCurVis = function () {
        return this.curVis;
    };
}

// module.exports = {
//     VisualisationSelectSubHandler: VisualisationSelectSubHandler,
//     VisualisationSelectHandler: VisualisationSelectHandler,
// };