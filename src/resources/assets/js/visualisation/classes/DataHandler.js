// var Models = require('../classes/Models');
// var VisualisationBlockDataModel = Models.VisualisationBlockDataModel;
// var VisualisationBlockModel = Models.VisualisationBlockModel;
// var VisualisationScatterDataModel = Models.VisualisationScatterDataModel;
// var VisualisationPointModel = Models.VisualisationPointModel;
// var VisualisationLineDataModel = Models.VisualisationLineDataModel;
// var VisualisationLinePointModel = Models.VisualisationLinePointModel;
// var VisualisationLineModel = Models.VisualisationLineModel;

/**
 * class for creating data models of visualisations
 * @param {Array} allImages
 * array with all image of volume for 0 values
 * @constructor
 */
function VisualisationDataHandler(allImages){
    var self = this;

    this.allImages = allImages;
    this.imagesWithLabelCache= {};

    /**
     * checks if the imagesWithLabelCache has a property for the given label id
     * @param {number} labelId
     * the label id to check for
     * @returns {boolean}
     * true if data for label is cached, else false
     */
    this.hasCached = function(labelId){
        return this.imagesWithLabelCache.hasOwnProperty(labelId);
    };
    /**
     * set the property of the imagesWithLabelCache with given label id and data
     * @param {Number} labelId
     * the label id to cache the data for
     * @param {Array} data
     * the response from the LabelController for the given label id
     */
    this.cache = function(labelId, data){
        this.imagesWithLabelCache[labelId] = data;
    };
    /**
     * get the annotation count per area from a line of a cached data array
     * by parsing the attrs property via JSON and dividing the count
     * by the laserpoints area property
     * @param {Object} line
     * a line from a data array of the imagesWithLabelCache to get count per area from
     */
    this.getAreaCount = function (line) {
        var count = line.count;
        var attrs = JSON.parse(line.attrs);
        if(attrs != null && attrs.laserpoints != null){
            var area = +attrs.laserpoints.area;
            if(area>0)count /= area;
        }
        return count;
    };

    /**
     * computes the data model of the block diagram for a given label
     * @param {Object} label
     * label to get data model for
     * @param {Number} group
     * number for grouping values on the y-axis, default is 1 for no grouping
     * @param {Boolean} show0
     * true if images with no annotations with the given label should be computed
     * @returns {VisualisationBlockDataModel}
     * data model for given label
     */
    this.getBlockData = function(label, group, show0) {
        var data = this.imagesWithLabelCache[label.id];
        var blockData = new VisualisationBlockDataModel(label.name);
        var subData = blockData.data;
        var ids = [];
        var xMax = 0;
        var yMax = 0;
        var tmpData = [];
        for (var j = 0; j < data.length; j++) {
            var line = data[j];
            var count = Math.floor(line.count/group);
            var imageId = line.id;
            //init new data line if subData at count is empty else push image id
            if (tmpData[count] == null){
                tmpData[count] = new VisualisationBlockModel(count, imageId);
                subData.push(tmpData[count]);
            }
            else tmpData[count].add(imageId);
            //compute max values for x and y axis
            xMax = Math.max(xMax, count);
            yMax = Math.max(yMax, tmpData[count].ids.length);
            //save that image id was used for 0 values
            ids[imageId] = true;
        }
        //iterate through data lines to fill x domain
        for (j = 0; j <= xMax; j++)
            blockData.xDomain.push(j);
        //add image ids for 0 count
        if(show0){
            if(tmpData[0]==null)tmpData[0] = new VisualisationBlockModel();
            for (j = 0; j < this.allImages.length; j++) {
                var image = this.allImages[j];
                if (!ids[image.id]) tmpData[0].add(image.id);
            }
            yMax = Math.max(yMax, tmpData[0].ids.length);
            subData.push(tmpData[0]);
        }
        //set y domain
        blockData.yDomain[1] = yMax;
        // console.log('blockData');
        // console.log(blockData);
        return blockData;
    };

    /**
     * computes the data model of the scatter plot for two or more given labels
     * @param {Array} selectedLabels
     * array with the selected labels objects
     * @param {Number} group
     * number for grouping values on the y-axis, default is 1 for no grouping
     * @param {Boolean} computeArea
     * true if annotations per area should be computed
     * @param {Boolean} show0
     * true if images with no annotations with the given label should be computed
     * @returns {VisualisationScatterDataModel}
     * data model for given labels
     */
    this.getScatterData = function(selectedLabels, group, computeArea, show0) {
        var scatterData = new VisualisationScatterDataModel();
        var labelNames = scatterData.labelNames;
        var subData = scatterData.data;
        var yMax = 0;
        var zMax = 0;
        for (var j = 0; j < selectedLabels.length; j++) {
            var label = selectedLabels[j];
            if (labelNames.indexOf(label.name) == -1)
                labelNames.push(label.name);
            var i;
            var ids = [];
            var countInd = 0;
            //create new scatter data for label id
            var tmpData = [];
            var data = this.imagesWithLabelCache[label.id];
            for (i = 0; i < data.length; i++) {
                var line = data[i];
                var count = line.count;
                if(computeArea)
                    count = this.getAreaCount(line);
                if(group>1)
                    count = Math.floor(count/group);
                var imageId = line.id;
                //init new data line if tmpData at count is empty else push image id
                if (tmpData[count] == null) {
                    tmpData[count] = new VisualisationPointModel(j, countInd, count, imageId);
                    subData.push(tmpData[count]);
                    countInd++;
                }
                else {
                    tmpData[count].add(imageId);
                }
                ids[imageId] = true;
                //compute max values
                yMax = Math.max(yMax, count);
                zMax = Math.max(zMax, tmpData[count].ids.length);
            }
            if(show0){
                if(tmpData[0]==null){
                    tmpData[0] = new VisualisationPointModel(j, countInd);
                    subData.push(tmpData[0]);
                    countInd++;
                }
                for (i = 0; i < this.allImages.length; i++) {
                    var image = this.allImages[i];
                    if (!ids[image.id]) tmpData[0].add(image.id);
                }
                zMax = Math.max(zMax, tmpData[0].ids.length);
            }
        }
        //set x and y domain
        scatterData.xDomain[0] = -1;
        scatterData.xDomain[1] = labelNames.length;
        scatterData.yDomain[0] = 0;
        scatterData.yDomain[1] = yMax + 1;
        scatterData.zDomain[0] = 1;
        scatterData.zDomain[1] = zMax;
        // console.log('scatterData');
        // console.log(scatterData);
        return scatterData;
    };

    /**
     * computes the data model of the gdp for two given labels
     * @param {Array} selectedLabels
     * array with the two selected label objects
     * @param {Number} group
     * number for grouping values on the y-axis, default is 1 for no grouping
     * @param {Boolean} computeArea
     * true if annotations per area should be computed
     * @param {Boolean} show0
     * true if images with no annotations with the given label should be computed
     * @returns {Object}
     * data model for given labels
     */
    this.getGDPData = function(selectedLabels, group, computeArea, show0) {
        var data = [];
        var mainData = {
            n: selectedLabels.length-1,
            data: data
        };
        var labels = [];
        this.getGDPLabels(labels, selectedLabels);
        for(var i=0;i<labels.length;i++)
            data.push(this.getGDPSubData(labels[i][0], labels[i][1],
                group, computeArea, show0));
        return mainData;
    };

    this.getGDPLabels = function (labels, selectedLabels) {
        var newSelLabels = [];
        for(var i=1;i<selectedLabels.length;i++){
            labels.push([
                selectedLabels[0],
                selectedLabels[i],
            ]);
            newSelLabels.push(selectedLabels[i]);
        }
        if(newSelLabels.length>1)
            this.getGDPLabels(labels, newSelLabels);
    };

    /**
     * computes the data model of the gdp for two given labels
     * @param {Object} label0
     * first selected label
     * @param {Object} label1
     * second selected label
     * @param {Number} group
     * number for grouping values on the y-axis, default is 1 for no grouping
     * @param {Boolean} computeArea
     * true if annotations per area should be computed
     * @param {Boolean} show0
     * true if images with no annotations with the given label should be computed
     * @returns {VisualisationScatterDataModel}
     * data model for given labels
     */
    this.getGDPSubData = function(label0, label1, group, computeArea, show0) {
        var scatterData = new VisualisationScatterDataModel();
        var labelNames = scatterData.labelNames;
        var subData = scatterData.data;
        var xMax = 0;
        var zMax = 0;
        labelNames.push(label0.name);
        labelNames.push(label1.name);

        var tmpData = [];
        var ids = [];
        var data = this.imagesWithLabelCache[label0.id];
        var i, line, count, imageId, model;
        for (i = 0; i < data.length; i++) {
            line = data[i];
            count = line.count;
            if(computeArea)
                count = this.getAreaCount(line);
            if(group>1)
                count = Math.floor(count/group);
            imageId = line.id;
            if(tmpData[count]==null)
                tmpData[count] = [];

            if(tmpData[count][0]==null){
                model = new VisualisationPointModel(count, 0, 0, imageId);
                subData.push(model);
                tmpData[count][0] = model;
            }else{
                model = tmpData[count][0];
                model.add(imageId);
            }
            if(ids[imageId]==null)ids[imageId]=model;
            else console.log('should not happen.');
            xMax = Math.max(xMax, count);
        }
        data = this.imagesWithLabelCache[label1.id];
        for (i = 0; i < data.length; i++) {
            line = data[i];
            count = line.count;
            if(computeArea)
                count = this.getAreaCount(line);
            if(group>1)
                count = Math.floor(count/group);
            imageId = line.id;
            if(ids[imageId]==null){
                if(tmpData[0]==null)
                    tmpData[0] = [];

                if(tmpData[0][count]==null){
                    model = new VisualisationPointModel(
                        0, 0, count, imageId);
                    subData.push(model);
                    tmpData[0][count] = model;
                }else{
                    model = tmpData[0][count];
                    model.add(imageId);
                }
                ids[imageId]=model;
            }else{
                var lastModel = ids[imageId];
                var index = lastModel.ids.indexOf(imageId);
                lastModel.ids.splice(index, 1);

                if(lastModel.ids.length==0){
                    index = subData.indexOf(lastModel);
                    subData.splice(index, 1);
                }

                if(tmpData[lastModel.label][count]==null){
                    model = new VisualisationPointModel(
                        lastModel.label, 0, count, imageId);
                    // pointInd++;
                    subData.push(model);
                    tmpData[lastModel.label][count] = model;
                }else{
                    model = tmpData[lastModel.label][count];
                    model.add(imageId);
                }
            }
            xMax = Math.max(xMax, count);
        }
        var pointInd = 0;
        for (i = 0; i < subData.length; i++){
            zMax = Math.max(zMax, subData[i].ids.length);
            subData[i].index = pointInd;
            pointInd++;
        }
        if(show0){
            var model0;
            if(tmpData[0] != null && tmpData[0][0] != null)
                model0 = tmpData[0][0];
            else{
                model0 = new VisualisationPointModel(0, pointInd);
                subData.push(model0);
            }
            for (i = 0; i < this.allImages.length; i++) {
                var image = this.allImages[i];
                if (!ids[image.id]) model0.add(image.id);
            }
            zMax = Math.max(zMax, model0.ids.length);
        }
        //set x and y domain
        scatterData.xDomain[0] = -1;
        scatterData.xDomain[1] = xMax + 1;
        scatterData.yDomain[0] = -1;
        scatterData.yDomain[1] = xMax + 1;
        scatterData.zDomain[0] = 1;
        scatterData.zDomain[1] = zMax;
        // console.log('scatterData');
        // console.log(scatterData.data);
        return scatterData;
    };

    /**
     * computes the data model of the line diagram for two or more given labels
     * @param {Array} selectedLabels
     * array with the two selected label objects
     * @param {Number} group
     * number for grouping values on the y-axis, default is 1 for no grouping
     * @param {Boolean} computeArea
     * true if annotations per area should be computed
     * @returns {VisualisationLineDataModel}
     * data model for given labels
     */
    this.getLineData = function(selectedLabels, group, computeArea) {
        var tmpPointData = [];
        var tmpLineData = [];
        var mainData = new VisualisationLineDataModel();
        var labelNames = mainData.labelNames;
        var pointData = mainData.pointData;
        var lineData = mainData.lineData;
        var yMax = 0;
        var zMax = 0;
        var j, i;
        //iterate through every data line for selected labels
        for (j = 0; j < selectedLabels.length; j++) {
            var label = selectedLabels[j];
            if (labelNames.indexOf(label.name) == -1)
                labelNames.push(label.name);
            // var countInd = 0;
            var data = this.imagesWithLabelCache[label.id];
            for (i = 0; i < data.length; i++) {
                var line = data[i];
                var count = line.count;
                if(computeArea)
                    count = this.getAreaCount(line);
                if(group>1)count = Math.floor(count/group);
                var imageId = line.id;
                //init new data line if tmpLineData at image id is empty
                if (tmpLineData[imageId] == null) {
                    //create new line data with array for points
                    tmpLineData[imageId] = new VisualisationLineModel(imageId);
                    //add dummy points with 0 annotations for each selected label
                    for (var k = 0; k < selectedLabels.length; k++) {
                        tmpLineData[imageId].points[k] = new VisualisationLinePointModel(
                            k, -1, 0, imageId);
                    }
                    lineData.push(tmpLineData[imageId]);
                }
                tmpLineData[imageId].points[j] = new VisualisationLinePointModel(
                    j, -1, count, imageId);
                yMax = Math.max(yMax, count);
            }
        }
        //iterate through line data to eliminate redundant points
        var pointInd = 0;
        for (j = 0; j < lineData.length; j++) {
            var imgId = lineData[j].id;
            for (i = 0; i < lineData[j].points.length; i++) {
                var lineDataLine = lineData[j].points[i];
                var labelInd = lineDataLine.label;
                var annotations = lineDataLine.count;
                if (tmpPointData[labelInd] == null) {
                    tmpPointData[labelInd] = [];
                }
                var newData = null;
                if (tmpPointData[labelInd][annotations] == null) {
                    newData = lineDataLine;
                    newData.index = pointInd;
                    pointInd++;
                    pointData.push(newData);
                    zMax = Math.max(zMax, newData.ids.length);
                    tmpPointData[labelInd][annotations] = newData;
                } else {
                    newData = tmpPointData[labelInd][annotations];
                    newData.add(imgId);
                    zMax = Math.max(zMax, newData.ids.length);
                }
                lineData[j].points[labelInd] = newData;
            }
        }
        mainData.xDomain[0] = -1;
        mainData.xDomain[1] = labelNames.length;
        mainData.yDomain[0] = 0;
        mainData.yDomain[1] = yMax + 1;
        mainData.zDomain[0] = 1;
        mainData.zDomain[1] = zMax;
        // console.log('lineData');
        // console.log(lineData);
        return mainData;
    };

    /**
     * computes the data model of the net map for two or more given labels
     * @param {Array} selectedLabels
     * array with the two selected label objects
     * @param {Number} group
     * number for grouping values on the y-axis, default is 1 for no grouping
     * @returns {VisualisationLineDataModel}
     * data model for given labels
     */
    this.getNetData = function(selectedLabels, group) {
        var tmpPointData = [];
        var tmpNetData = [];
        var mainData = new VisualisationLineDataModel();
        var labelNames = mainData.labelNames;
        var pointData = mainData.pointData;
        var netData = mainData.lineData;
        var newData;
        var j, i;
        for (j = 0; j < selectedLabels.length; j++) {
            var label = selectedLabels[j];
            if (labelNames.indexOf(label.name) == -1)
                labelNames.push(label.name);
            var data = this.imagesWithLabelCache[label.id];
            for (i = 0; i < data.length; i++) {
                var line = data[i];
                var count = line.count;
                var imageId = line.id;
                if (tmpNetData[imageId] == null) {
                    //create new line data with array for points
                    tmpNetData[imageId] = new VisualisationLineModel(imageId);
                }
                tmpNetData[imageId].points.push(
                    new VisualisationLinePointModel(
                        j, -1, count, imageId)
                );
                if(tmpNetData[imageId].points.length==2)
                    netData.push(tmpNetData[imageId]);
            }
        }
        var ind = 0;
        for (j = 0; j < netData.length; j++) {
            var imgId = netData[j].id;
            for (i = 0; i < netData[j].points.length; i++) {
                var netDataLine = netData[j].points[i];
                var labelInd = netDataLine.label;
                var annotations = Math.floor(netDataLine.count/group);
                if (tmpPointData[labelInd] == null) {
                    tmpPointData[labelInd] = [];
                }
                newData = null;
                if (tmpPointData[labelInd][annotations] == null) {
                    newData = netDataLine;
                    newData.index = ind;
                    newData.count = annotations;
                    ind++;
                    tmpPointData[labelInd][annotations] = newData;
                } else {
                    newData = tmpPointData[labelInd][annotations];
                    newData.add(imgId);
                }
                netData[j].points[i] = newData;
            }
            if(netData[j].points.length>2)
                netData[j].points.push(netData[j].points[0]);
        }
        var pointInd = 0;
        for (j = 0; j < tmpPointData.length; j++) {
            for (i = 0; i < tmpPointData[j].length; i++) {
                newData = tmpPointData[j][i];
                if(newData!=null){
                    pointData.push(newData);
                    newData.index = pointInd;
                    pointInd++;
                }
            }
        }
        // console.log('netmapData');
        // console.log(mainData);
        // console.log('netData');
        // console.log(netData[0]);
        return mainData;
    };

    /**
     * computes the data model of the net map for three or more given labels
     * @param {Array} selectedLabels
     * array with the two selected label objects
     * @param {Number} group
     * number for grouping values on the y-axis, default is 1 for no grouping
     * @param {Boolean} computeArea
     * true if annotations per area should be computed
     * @returns {VisualisationLineDataModel}
     * data model for given labels
     */
    this.getRadarData = function (selectedLabels, group, computeArea) {
        var tmpPointData = [];
        var tmpNetData = [];
        var mainData = new VisualisationLineDataModel();
        var labelNames = mainData.labelNames;
        var pointData = mainData.pointData;
        var netData = mainData.lineData;
        var j, i, yMax = 0;
        for (j = 0; j < selectedLabels.length; j++) {
            var label = selectedLabels[j];
            if (labelNames.indexOf(label.name) == -1)
                labelNames.push(label.name);
            var data = this.imagesWithLabelCache[label.id];
            for (i = 0; i < data.length; i++) {
                var line = data[i];

                var count = line.count;
                if(computeArea){
                    count = this.getAreaCount(line);
                }
                if(group>1)count = Math.floor(count/group);

                var imageId = line.id;
                if (tmpNetData[imageId] == null) {
                    //create new line data with array for points
                    tmpNetData[imageId] = new VisualisationLineModel(imageId);
                    //add dummy points with 0 annotations for each selected label
                    for (var k = 0; k < selectedLabels.length; k++) {
                        tmpNetData[imageId].points[k] = new VisualisationLinePointModel(
                            k, -1, 0, imageId);
                    }
                    netData.push(tmpNetData[imageId]);
                }
                tmpNetData[imageId].points[j] = new VisualisationLinePointModel(
                    j, -1, count, imageId);
                yMax = Math.max(yMax, count);
            }
        }
        var zMax = 0;
        var pointInd = 0;
        for (j = 0; j < netData.length; j++) {
            var imgId = netData[j].id;
            for (i = 0; i < netData[j].points.length; i++) {
                var lineDataLine = netData[j].points[i];
                var labelInd = lineDataLine.label;
                var annotations = lineDataLine.count;
                if (tmpPointData[labelInd] == null) {
                    tmpPointData[labelInd] = [];
                }
                var newData = null;
                if (tmpPointData[labelInd][annotations] == null) {
                    newData = lineDataLine;
                    newData.index = pointInd;
                    pointInd++;
                    pointData.push(newData);
                    zMax = Math.max(zMax, newData.ids.length);
                    tmpPointData[labelInd][annotations] = newData;
                } else {
                    newData = tmpPointData[labelInd][annotations];
                    newData.add(imgId);
                    zMax = Math.max(zMax, newData.ids.length);
                }
                netData[j].points[labelInd] = newData;
            }
            netData[j].points.push(netData[j].points[0]);
        }


        mainData.yDomain[0] = -1;
        mainData.yDomain[1] = yMax+1;
        mainData.zDomain[0] = 1;
        mainData.zDomain[1] = zMax;
        // console.log("getRadarData:");
        // console.log(mainData);
        // console.log(mainData.labelNames);
        // console.log(mainData.netData);
        // console.log(mainData.pointData);
        // console.log(mainData.yDomain);
        // console.log(mainData.zDomain);
        return mainData;
    };

    /**
     * computes the data model of the usability tests
     * from a file download event with given parameters
     * @param {Object} event
     * even to get the test results from
     * @param {String} yName
     * parameter for the y-axis
     * @param {Number} group
     * number for grouping values on the y-axis, default is 1 for no grouping
     * @param {Function} callback
     * function to be called with the data model when finished
     */
    this.getTestDataModel = function (event, yName, xName, group, callback) {
        var data = [];
        var files = event.target.files;
        for (var i = 0; i<files.length; i++) {
            this.readLine(files[i], data, this.finishedLoadingTestData,
                yName, xName, group, callback, (i == files.length-1));
        }
    };

    /**
     * read a line from given file,parse the data via JSON and add it to given data array
     * when all lines are read parse the data to the given function
     * @param {Object} file
     * file to read from
     * @param {Array} data
     * array for parsed data
     * @param {Function} finished
     * function to call after last line
     * @param {String} yName
     * parameter for the y-axis
     * @param {Number} group
     * number for grouping values on the y-axis, default is 1 for no grouping
     * @param {Function} callback
     * function to be called with the data model when finished
     * @param {Boolean} last
     * true if current line is the last one
     */
    this.readLine = function (file, data, finished, yName, xName, group, callback, last) {
        var reader = new FileReader();
        reader.onload = (function() {
            return function(e) {
                data.push(JSON.parse(e.target.result));
                if(last)
                    finished(data, yName, xName, group, callback);
            };
        })(file);
        reader.readAsText(file);
    };

    this.updateTestDataModel = function (yName, xName, group, callback) {
        if(self.testData!=null)
            self.finishedLoadingTestData(self.testData, yName, xName, group, callback);
    };

    this.finishedLoadingTestData = function (data, yName, xName, group, callback) {

        self.testData = data;
        if(xName == 'quest')
            self.getTestDataByQuestion(data, yName, group, callback);
        else self.getTestDataByTester(data, yName, group, callback);
    };

    this.getTestDataByQuestion = function(data, yName, group, callback){
        var tmpPointData = [];
        var tmpLineData = [];
        var mainData = new VisualisationLineDataModel();
        mainData.labelX = 'Questions';
        mainData.labelY = 'Mark';
        if(yName=='time')mainData.labelY = 'Time(min)';
        else if(yName=='quota')mainData.labelY = 'Quota(%)';
        var labelNames = mainData.labelNames;
        var pointData = mainData.pointData;
        var lineData = mainData.lineData;
        var yMax = 0;
        var zMax = 0;
        var j, i;
        var values = [];
        for (j = 0; j < data.length; j++) {
            var subData = data[j];
            this.checkTest(subData);
            var res,count,questId;
            // console.log(subData);
            if(yName=='note'){
                if(j==0)
                    values[0] = [];
                for (i = 0; i < subData.evaluation.results.length; i++) {
                    res = subData.evaluation.results[i];
                    count = res.answer;
                    questId = res.quest.name;
                    if(j==0){
                        labelNames.push(i+1);
                        values[0][i] = count;
                    }
                    else values[0][i] += count;
                }
            }else{
                if(j==0)
                    values[0] = [];
                for (i = 0; i < subData.first.results.length; i++) {
                    res = subData.first.results[i];
                    count = res[yName];
                    if(yName == 'time')count /= 60;
                    questId = res.quest.name;
                    if(j==0){
                        labelNames.push(i+1);
                        values[0][i] = count;
                    }
                    else values[0][i] += count;
                }
                if(j==0)
                    values[1] = [];
                for (i = 0; i < subData.second.results.length; i++) {
                    res = subData.second.results[i];
                    count = res[yName];
                    if(yName == 'time')count /= 60;
                    questId = res.quest.name;
                    if(j==0)
                        values[1][i] = count;
                    else values[1][i] += count;
                }
            }
        }
        if(data.length>0 && values.length>0){
            var id = 'E';
            for (j = 0; j < values.length; j++) {
                if(yName=='time')
                    id = 'T'+(j+1);
                if(yName=='quota')
                    id = 'Q'+(j+1);
                var line = values[j];
                var model = new VisualisationLineModel(id);
                lineData.push(model);
                for (i = 0; i < line.length; i++) {
                    var value = line[i]/data.length;
                    var subModel = new VisualisationLinePointModel(i, -1, value, id);
                    model.points.push(subModel);
                    pointData.push(subModel);
                    yMax = Math.max(yMax, value);
                }
            }
        }

        mainData.yName = yName;
        mainData.xDomain[0] = -1;
        mainData.xDomain[1] = labelNames.length;
        mainData.yDomain[0] = 0;
        mainData.yDomain[1] = yMax;
        if(yName=='note')
            mainData.yDomain[1] = 5;
        else if(yName=='quota')
            mainData.yDomain[0] = -0.1;
        mainData.zDomain[0] = 1;
        mainData.zDomain[1] = zMax;
        // console.log('testData');
        // console.log(mainData);
        // console.log('labelNames');
        // console.log(mainData.labelNames);
        // console.log('pointData');
        // console.log(mainData.pointData);
        // console.log('lineData');
        // console.log(mainData.lineData);
        // return mainData;
        callback(mainData);
    };

    this.getRunData = function (group) {
        // group = 100;
        var mainData = new VisualisationLineDataModel();
        mainData.labelX = 'Number of images/annotations';
        mainData.labelY = 'Runtime';
        mainData.yName = 'runtime';
        var yMax = 0;
        var iMax = 5;
        var functions = [
            {
                name: '',
                call: function (i) {return 6*i;}
            },
            {
                name: 'S, H',
                call: function (i) {return 8*i;}
            },
            {
                name: 'R, L',
                call: function (i) {return 131*i+10;}
            },
            {
                name: 'N',
                call: function (i) {return 50*i+10;}
            },
            {
                name: '',
                call: function (i) {return 140*i;}
            }
        ];
        var j, i;

        for (j = 0; j < functions.length; j++){
            var func = functions[j];
            var model = new VisualisationLineModel(func.name);
            mainData.lineData.push(model);
            for (i = 0; i < iMax; i++){
                var y = func.call(i+1);
                if(group>1)
                    y = y/group;
                yMax = Math.max(yMax, y);
                model.points.push(
                    new VisualisationLinePointModel(i+1, -1, y, func.name)
                );
                if(j==0)mainData.labelNames.push(i);
            }
        }
        mainData.labelNames.push(iMax);
        mainData.xDomain[0] = 0;
        mainData.xDomain[1] = iMax+1;
        mainData.yDomain[0] = 0;
        mainData.yDomain[1] = yMax;
        return mainData;
    };

    this.getTestDataByTester = function(data, yName, group, callback){
        var tmpPointData = [];
        var tmpLineData = [];
        var mainData = new VisualisationLineDataModel();
        mainData.labelX = 'Tester';
        if(yName=='time')mainData.labelY = 'Time(min)';
        else if(yName=='quota')mainData.labelY = 'Quota(%)';
        var labelNames = mainData.labelNames;
        var pointData = mainData.pointData;
        var lineData = mainData.lineData;
        mainData.comments = [];
        var yMax = 0;
        var zMax = 0;
        var j, i;
        for (j = 0; j < data.length; j++) {
            var subData = data[j];
            this.checkTest(subData);
            labelNames.push(subData.name);
            var lineName = 'q1';
            if(yName=='time')lineName = 't1';
            yMax = Math.max(
                self.getTestDataLines(j, subData.first, lineData, tmpLineData,
                    yName, group, lineName),
                yMax
            );
            lineName = 'q2';
            if(yName=='time')lineName = 't2';
            yMax = Math.max(
                self.getTestDataLines(j, subData.second, lineData, tmpLineData,
                    yName, group, lineName),
                yMax
            );
            if(yName == 'note') {
                yMax = Math.max(
                    self.getEvalDataLines(j, subData.evaluation, lineData, tmpLineData,
                        subData.first.results.length * 2),
                    yMax
                );
                var mainCount = subData.note;
                var evalName = 'E';
                if (tmpLineData[evalName] == null) {
                    tmpLineData[evalName] = new VisualisationLineModel(evalName);
                    lineData.push(tmpLineData[evalName]);
                }
                tmpLineData[evalName].labelName = evalName;
                tmpLineData[evalName].points.push(
                    new VisualisationLinePointModel(j, -1, mainCount, evalName));
                yMax = Math.max(yMax, mainCount);
            }
            mainData.answerLength = subData.first.results.length;
            if(subData.opinion.results[0].answer != null &&
                subData.opinion.results[0].answer != '')
                mainData.comments.push({
                    tester: subData.name,
                    text: [subData.opinion.results[0].answer]
                });
        }
        if(mainData.comments.length>0){
            console.log('comments:');
            console.log(JSON.stringify(mainData.comments));
        }else console.log('no comments.');
        var pointInd = 0;
        for (j = 0; j < lineData.length; j++) {
            for (i = 0; i < lineData[j].points.length; i++) {
                var lineDataLine = lineData[j].points[i];
                var labelInd = lineDataLine.label;
                var annotations = lineDataLine.count;
                if (tmpPointData[labelInd] == null) {
                    tmpPointData[labelInd] = [];
                }
                var newData = null;
                if (tmpPointData[labelInd][annotations] == null) {
                    newData = lineDataLine;
                    newData.index = pointInd;
                    pointInd++;
                    pointData.push(newData);
                    zMax = Math.max(zMax, newData.ids.length);
                    tmpPointData[labelInd][annotations] = newData;
                } else {
                    newData = tmpPointData[labelInd][annotations];
                    newData.add(lineData[j].labelName);
                    zMax = Math.max(zMax, newData.ids.length);
                }
                lineData[j].points[labelInd] = newData;
            }
        }
        mainData.yName = yName;
        mainData.xDomain[0] = -1;
        mainData.xDomain[1] = labelNames.length;
        mainData.yDomain[0] = 0;
        mainData.yDomain[1] = yMax;
        if(yName=='note')
            mainData.yDomain[1] = 5;
        else if(yName=='quota')
            mainData.yDomain[0] = -0.1;
        mainData.zDomain[0] = 1;
        mainData.zDomain[1] = zMax;
        // console.log('testData');
        // console.log(mainData);
        // console.log('labelNames');
        // console.log(mainData.labelNames);
        // console.log('pointData');
        // console.log(mainData.pointData);
        // console.log('lineData');
        // console.log(mainData.lineData);
        // return mainData;
        callback(mainData);
    };

    this.getEvalDataLines = function(j, evaluation, lineData, tmpLineData, length){
        var yMax = 0;

        for (var i = 0; i < evaluation.results.length; i++) {
            var res = evaluation.results[i];
            var count = res.answer;
            var questId = res.quest.name;
            var labelName = 'E'+(questId+1-length);
            if (tmpLineData[questId] == null) {
                tmpLineData[questId] = new VisualisationLineModel(questId);
                tmpLineData[questId].labelName = labelName;
                lineData.push(tmpLineData[questId]);
            }
            tmpLineData[questId].points.push(
                new VisualisationLinePointModel(j, -1, count, labelName));
            yMax = Math.max(yMax, count);
        }


        return yMax;
    };

    this.getTestDataLines = function (j, data, lineData, tmpLineData, yName, group, testName) {
        var yMax = 0;
        if(yName == 'note')return yMax;

        var mainCount;
        if(yName == 'quota'){
            mainCount = data[yName];
            mainCount = Math.floor(mainCount*100)/100;
            if(group>1)
                mainCount = Math.floor(mainCount/group);
            if (tmpLineData[testName] == null) {
                tmpLineData[testName] = new VisualisationLineModel(testName);
                lineData.push(tmpLineData[testName]);
            }
            tmpLineData[testName].labelName = testName;
            tmpLineData[testName].points.push(
                new VisualisationLinePointModel(j, -1, mainCount, testName));
            yMax = Math.max(yMax, mainCount);
        }

        if(yName == 'time') {
            // testName += '*';
            mainCount = data[yName] / data.results.length / 60;
            if (group > 1) mainCount = Math.floor(mainCount / group);
            if (tmpLineData[testName] == null) {
                tmpLineData[testName] = new VisualisationLineModel(testName);
                lineData.push(tmpLineData[testName]);
            }
            tmpLineData[testName].labelName = testName;
            tmpLineData[testName].points.push(
                new VisualisationLinePointModel(j, -1, mainCount, testName));
            yMax = Math.max(yMax, mainCount);
        }
        return yMax;
    };

    this.checkTest = function (test, errors) {
        this.checkSubTest(test.first, errors);
        this.checkSubTest(test.second, errors);
        // var quota = (test.first.quota+test.second.quota)/2;
        // if(quota != test.quota){
        //     console.log('quota != test.quota');
        //     console.log('expected:'+quota);
        //     console.log('test:'+test.quota);
        //     // test.quota = quota;
        //     errors. push('quota != test.quota');
        // }

        // var note = 0;
        // for (var j = 0; j < test.evaluation.results.length; j++) {
        //     // console.log(test.evaluation.results[j]);
        //     note += test.evaluation.results[j].answer;
        // }
        // note/=test.evaluation.results.length;
        // if(note != test.note){
        //     // console.log('note != test.note');
        //     // console.log('expected:'+note);
        //     // console.log('test:'+test.note);
        //     // test.note = note;
        //     errors. push('note != test.note');
        // }
    };

    /**
     * check given test iteration for correct result computing
     * @param {Object} test
     * result from a test iteration
     */
    this.checkSubTest = function (test, errors) {
        // console.log('checkTest');
        var right = 0;
        var wrong = 0;
        var quotaSum = 0;
        var timeSum = 0;
        for (var j = 0; j < test.results.length; j++) {
            var res = test.results[j];
            var expected = res.quest.answer;
            var answer = [];
            for (var i = 0; i < res.answer.length; i++)
                answer.push(res.answer[i].value);
            var quota = this.checkAnswer(expected, answer);
            if(quota == 1){
                right++;
            }else{
                wrong++;
            }
            if(quota != res.quota){
                console.log('quota != res.quota');
                console.log('expected:'+quota);
                console.log('test:'+res.quota);
                console.log('quest:'+expected);
                console.log('answer:'+answer);
                // console.log('');
                // res.quota = quota;
                errors. push('quota != res.quota');
            }
            quotaSum += quota;

            // console.log('time:'+res.time);
            timeSum += res.time;
            timeSum = Math.round(timeSum*1000)/1000;
            // console.log('timeSum:'+timeSum);

        }

        test.quotaSum = quotaSum / test.results.length;
        if(right != test.right || wrong != test.wrong){
            console.log('expected:');
            console.log('right:'+right);
            console.log('wrong:'+wrong);
            console.log('test:');
            console.log('right:'+test.right);
            console.log('wrong:'+test.wrong);
            console.log('');
            // test.right = right;
            // test.wrong = wrong;
            // test.quota = right/(right+wrong);
            errors. push('right != test.right || wrong != test.wrong');
        }
        if(timeSum != test.time){
            console.log('expected:');
            console.log('time:'+timeSum);
            console.log('test:');
            console.log('time:'+test.time);
            console.log('');
        }
    };

    /**
     * check given answer for correct result computing
     * @param {Array} expected
     * expected answer
     * @param {Array} answer
     * given answer
     * @returns {number}
     * quote of this answer
     */
    this.checkAnswer =function(expected, answer){
        var found = 0;
        for (var j = 0; j < expected.length; j++) {
            for (var i = 0; i < answer.length; i++)
                if(expected[j] == answer[i])
                    found++;
        }
        return found/Math.max(answer.length, expected.length);
    };

}


// module.exports = VisualisationDataHandler;