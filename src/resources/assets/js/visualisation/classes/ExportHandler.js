/**
 * class for handling export of svg in different formats
 * @param {String} name
 * current volume name
 * @param {Object} model
 * model of name input field
 * @constructor
 */
function VisualisationExportHandler(name, model) {
    this.volumeName = name;
    this.model = model;

    /**
     * export svg for given type
     * @param {String} type
     * can be 'svg' or 'png'
     * @param {Array} labels
     * array of selected labels for name
     */
    this.export = function (type, labels) {
        switch (type){
            case 'svg':
                this.exportSVG(labels);
                break;
            case 'png':
                this.exportPNG(labels);
                break;
        }
    };

    /**
     * export with svg format
     */
    this.exportSVG = function () {
        var chart = d3.select("#visualisation-svg");
        var fullWidth = +chart.node().getBoundingClientRect().width;
        var fullHeight = +chart.node().getBoundingClientRect().height;
        chart.attr("width", fullWidth);
        chart.attr("height", fullHeight);

        var svg = document.getElementById("visualisation-svg");
        var serializer = new XMLSerializer();
        var svgData = serializer.serializeToString(svg);
        var svgBlob = new Blob([svgData], {type:"image/svg+xml;charset=utf-8"});
        var svgUrl = URL.createObjectURL(svgBlob);
        this.triggerDownload(svgUrl, this.getName('.svg'));
    };

    /**
     * export with png format
     */
    this.exportPNG = function () {
        var chart = d3.select("#visualisation-svg");
        var fullWidth = +chart.node().getBoundingClientRect().width;
        var fullHeight = +chart.node().getBoundingClientRect().height;
        chart.attr("width", fullWidth);
        chart.attr("height", fullHeight);

        var svg = document.getElementById("visualisation-svg");
        var serializer = new XMLSerializer();
        var svgData = serializer.serializeToString(svg);
        var svgBlob = new Blob([svgData], {type:"image/svg+xml;charset=utf-8"});
        var svgUrl = URL.createObjectURL(svgBlob);

        var canvas = document.getElementById('visualisation-canvas');
        canvas.width = fullWidth;
        canvas.height = fullHeight;
        var ctx = canvas.getContext('2d');
        var DOMURL = window.URL || window.webkitURL || window;
        var img = new Image();

        var self = this;
        img.onload = function () {
            ctx.drawImage(img, 0, 0);
            DOMURL.revokeObjectURL(svgUrl);

            var imgURI = canvas
                .toDataURL('image/png')
                .replace('image/png', 'image/octet-stream');

            self.triggerDownload(imgURI, self.getName('.png'));
        };

        img.src = svgUrl;
    };

    /**
     * update model value for given name and labels
     * @param {String} name
     * name of current visualisation
     * @param {Array} labels
     * array of selected labels
     */
    this.updateName = function (name, labels) {
        if(name == VIS_BLANK)
            this.model.value = 'visualisation';
        else{
            var s = this.volumeName+'_';
            for(var i=0;i<labels.length;i++)
                s += labels[i].name+'_';
            this.model.value = s+name;
        }
    };

    /**
     * get name with given ending
     * @param {String} ending
     * ending to attach to model value
     * @returns {String}
     */
    this.getName = function (ending) {
        if(this.model.value != null && this.model.value.length > 0)
            return this.model.value+ending;
        return 'visualisation';
    };

    /**
     * trigger a download for given uri and filename
     * @param {String} uri
     * uri to attach as href to created anchor
     * @param {String} filename
     * name to attach as download to created anchor
     */
    this.triggerDownload = function(uri, filename) {
        var evt = new MouseEvent('click', {
            view: window,
            bubbles: false,
            cancelable: true
        });

        var a = document.createElement('a');
        a.setAttribute('download', filename);
        a.setAttribute('href', uri);
        a.setAttribute('target', '_blank');

        a.dispatchEvent(evt);
    };
}