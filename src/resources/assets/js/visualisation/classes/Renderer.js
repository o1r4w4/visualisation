/**
 * class for basic svg render and scaling functions
 * @param {Object} colors
 * object with color strings for rendering different objects
 * @param {Object} settings
 * object with settings for rendering different objects
 * @constructor
 */
function VisualisationRenderer(colors, settings){
    this.defColor = '#000000';
    this.colors = colors;
    this.settings = settings;
    this.scales = {};

    /**
     * get color string for given group and name
     * @param {String} group
     * group to check colors for
     * @param {String} name
     * name to check group for
     * @returns {String}
     * color for given group and name, or default color if not found
     */
    this.getColor = function(group, name){
        if(this.colors[group]==null){
            console.log("ERROR: group '"+group+"' not found!");
            return this.defColor;
        }
        if(this.colors[group][name]==null){
            console.log("ERROR: color '"+name+"' in group '"+group+"' not found!");
            return this.defColor;
        }
        return this.colors[group][name];
    };

    /**
     * makes svg element fit manual size values,
     * if USE_SIZE was set, else svg element fits parent size.
     * full-width/-height and width/height with margin are computed.
     * clear svg element and render given heading
     * @param {String} heading
     * new heading to draw at top
     */
    this.initChart = function(heading){
        this.chart = d3.select("#visualisation-svg")
            .style("background-color", this.getColor(CHART, CHART));

        if(this.settings[USE_SIZE]){
            var width = this.settings[WIDTH];
            var height = this.settings[HEIGHT];
            if(width<0)width *= -1;
            if(height<0)height *= -1;
            this.fullWidth = +width;
            this.fullHeight = +height;
            this.chart.attr("width", width);
            this.chart.attr("height", height);
        }else{
            this.chart.attr("width", "100%");
            this.chart.attr("height", "100%");
            this.fullWidth = +this.chart.node().getBoundingClientRect().width;
            this.fullHeight = +this.chart.node().getBoundingClientRect().height;
        }
        this.width = this.fullWidth - this.settings[MARGIN_LEFT] - this.settings[MARGIN_RIGHT];
        this.height = this.fullHeight - this.settings[MARGIN_TOP] - this.settings[MARGIN_BOT];


        this.chart.selectAll("*").remove();
        this.g = this.chart.append("g")
            .attr("transform", "translate(" + this.settings[MARGIN_LEFT] +
                "," + this.settings[MARGIN_TOP] + ")");

        this.chart.append("text")
            .attr("x", this.fullWidth / 2)
            .attr("y", this.settings[FONT_SIZE_HEAD])
            .attr("fill", this.getColor(CHART, COL_FONT_HEADING))
            .attr("stroke", this.getColor(CHART, COL_FONT_HEADING))
            .style("font-size", this.settings[FONT_SIZE_HEAD] +"px")
            .attr("text-anchor", "middle")
            .text(heading);
    };

    /**
     * check scale.type for new scale type
     * and attach given range and domain to new scale.
     * new scale is saved in scale property with given axis name.
     * for band scale the padding of the given scale is set as padding.
     * for log scale a temporary scale is used to get a axis with 0 point.
     * @param {String} axis
     * @param {Object} scale
     * @param {Array} range
     * @param {Array} domain
     */
    this.setScale = function(axis, scale, range, domain) {
        var newScale = null;
        switch (scale.type) {
            case 'band':
                newScale = d3.scaleBand().rangeRound(range)
                    .padding(scale.padding);
                newScale.domain(domain);
                break;
            case 'log':
                var tmpScale = d3.scaleLog()
                    .base(scale.base)
                    .rangeRound(range);
                var tmpDomain = [
                    1,
                    domain[1]
                ];
                tmpScale.domain(tmpDomain);
                var xOffset = tmpScale(1)-tmpScale(scale.base);
                var tmpRange = [
                    range[0]-xOffset,
                    range[1]
                ];
                newScale = d3.scaleLog()
                    .base(scale.base)
                    .rangeRound(tmpRange);
                newScale.clamp(true);
                newScale.nice();
                newScale.domain(tmpDomain);
                break;
            default:
                newScale = d3.scaleLinear().rangeRound(range);
                newScale.domain(domain);
                break;
        }
        this.scales[axis] = newScale;
    };

    /**
     * make a axis for given scale
     * @param {Object} scale
     * @param {String} axisClass
     * @param {String} transform
     * @param {String} tickTrans
     * @param {String} tickAnchor
     * @param {Function} tickText
     */
    this.makeAxis = function(scale,axisClass,transform,tickTrans,tickAnchor,tickText){
        var line = this.g.append("g");
        if (axisClass != null)line.attr("class", axisClass);
        if (transform != null)line.attr("transform", transform);
        line.call(scale);
        var texts = line.selectAll("text");
        texts.style("font-size", this.settings[FONT_SIZE_TICK] +"px")
            .attr("fill", this.getColor(CHART, COL_FONT_TICK));
        if (tickTrans != null ) texts.attr("transform", tickTrans);
        if (tickAnchor != null) texts.attr("text-anchor", tickAnchor);
        if (tickText != null) texts.text(tickText);
    };

    /**
     * make axis label at given position with given text
     * @param {Number} x
     * @param {Number} y
     * @param {String} text
     * @param {String} transform
     */
    this.makeAxisLabel = function(x, y, text, transform){
        var label = this.g.append("text")
            .style("font-size", this.settings[FONT_SIZE_LABEL] +"px")
            .attr("fill", this.getColor(CHART, COL_FONT_LABEL))
            .attr("stroke", this.getColor(CHART, COL_FONT_LABEL))
            .attr("text-anchor", "start")
            .attr("x", x)
            .attr("y", y)
            .text(text);
        if (transform != null)label.attr("transform", transform);
    };

    /**
     * make bars for histogram based on given data
     * @param {Array} data
     * array with VisualisationBlockModel entries
     * @param {boolean} showValues
     * true if text with value should be drawn on bars
     * @param {Function} clicked
     */
    this.makeBars = function(data, showValues, clicked) {
        var self = this;
        var bar = this.g.selectAll(".bar")
            .data(data)
            .enter();

        bar.append("rect")
            .attr("class", "bar_back")
            .attr("id", function (d) {
                return "back_bar_" + d.count;
            })
            .attr("x", function (d) {
                return self.scales.x(d.count);
            })
            .attr("y", 0)
            .attr("width", this.scales.x.bandwidth())
            .attr("height", this.height)
            .attr("fill", this.getColor(VIS_BLOCK, COL_SEC))
            .on("click", clicked);

        bar.append("rect")
            .attr("class", "bar")
            .attr("id", function (d) {
                return "bar_" + d.count;
            })
            .attr("x", function (d) {
                return self.scales.x(d.count);
            })
            .attr("y", function (d) {
                return self.scales.y(d.ids.length);
            })
            .attr("width", this.scales.x.bandwidth())
            .attr("height", function (d) {
                if(d.ids.length==0)return 0;
                return self.height -
                    self.scales.y(d.ids.length);
            })
            .attr("fill", this.getColor(VIS_BLOCK, COL_MAIN))
            .on("click", clicked);

        if(showValues){
            var fontSize = Math.min(32, this.scales.x.bandwidth());
            bar.append("text")
                .attr("class", "text")
                .attr("transform", "rotate(-90)")
                .attr("y", function (d) {
                    return self.scales.x(d.count) +
                        self.scales.x.bandwidth() / 2+fontSize/2;
                })
                .attr("x", 0)
                .attr("fill", this.getColor(CHART, COL_FONT_ELEMENT))
                .attr("stroke", this.getColor(CHART, COL_FONT_ELEMENT))
                .style("font-size",  fontSize+"px")
                .attr("text-anchor", "end")
                .text(function (d) {
                    return d.ids.length;
                })
                .on("click", clicked);
        }

    };

    /**
     * make line for parallel coordinates based on given data
     * @param {Array} data
     * array with VisualisationLinePointModel entries
     * @param {Function} mode
     * d3.js curve function for line
     * @returns {Object}
     * created line object
     */
    this.makeLine = function(data, mode) {
        var self = this;
        var path = d3.line()
            .x(function (d) {
                return self.scales.x(d.label);
            })
            .y(function (d) {
                return self.scales.y(d.count);
            })
            .curve(mode);
        var line = this.g.append("path")
            .datum(data)
            .attr("fill", "none")
            .attr("stroke", self.getColor(VIS_LINE, COL_SEC))
            .attr("stroke-width", 2)
            .style("stroke-opacity", 0.5)
            .attr("d", path);
        line.pointCounter = 0;
        return line;
    };

    /**
     * make points for parallel coordinates based on given data
     * @param {Array} data
     * array with VisualisationLinePointModel entries
     * @param {boolean} showValues
     * true if text with value should be drawn on bars
     * @param {Function} clicked
     */
    this.makeLineDots = function(data, showValues, clicked) {
        var self = this;
        var dot = this.g.selectAll(".dot")
            .data(data)
            .enter();

        dot.append("circle")
            .attr("class", "dot")
            .attr("id", function (d) {
                return "circle_" + d.index;
            })
            .attr("cx", function (d) {
                // console.log(d);
                return self.scales.x(d.label);
            })
            .attr("cy", function (d) {
                return self.scales.y(d.count);
            })
            .attr("r", function (d) {
                return self.scales.z(d.ids.length);
            })
            .attr("fill", this.getColor(VIS_LINE, COL_MAIN))
            .on("click", clicked);
        if(showValues)
            dot.append("text")
                .attr("class", "text")
                .attr("y", function (d) {
                    return self.scales.y(d.count)+4;
                })
                .attr("x", function (d) {
                    return self.scales.x(d.label);
                })
                .attr("fill", this.getColor(CHART, COL_FONT_ELEMENT))
                .attr("stroke", this.getColor(CHART, COL_FONT_ELEMENT))
                // .attr("dy", "16px")
                .style("font-size",  "16px")
                .attr("text-anchor", "middle")
                .text(function (d) {
                    return d.ids.length;
                })
                .on("click", clicked)
            ;
    };

    /**
     * make points for gdp based on given data
     * @param {Array} data
     * array with VisualisationPointModel entries
     * @param {boolean} showValues
     * true if text with value should be drawn on bars
     * @param {Function} clicked
     */
    this.makeDots = function(data, showValues, clicked) {
        var self = this;

        var dot = this.g.selectAll(".dot")
            .data(data)
            .enter();

        dot.append("circle")
            .attr("class", "dot")
            .attr("id", function (d) {
                return "circle_" + d.index;
            })
            .attr("cx", function (d) {
                return self.scales.x(d.label);
            })
            .attr("cy", function (d) {
                return self.scales.y(d.count);
            })
            .attr("r", function (d) {
                return self.scales.z(d.ids.length);
            })
            .attr("fill", this.getColor(VIS_SCATTER, COL_MAIN))
            .on("click", clicked);
        if(showValues)
            dot.append("text")
                .attr("class", "text")
                .attr("y", function (d) {
                    return self.scales.y(d.count)+4;
                })
                .attr("x", function (d) {
                    return self.scales.x(d.label);
                })
                .attr("fill", this.getColor(CHART, COL_FONT_ELEMENT))
                .attr("stroke", this.getColor(CHART, COL_FONT_ELEMENT))
                .style("font-size",  "16px")
                .attr("text-anchor", "middle")
                .text(function (d) {
                    return d.ids.length;
                })
                .on("click", clicked)
            ;
    };

    /**
     * make points for net chart based on given data
     * @param {Array} data
     * array with VisualisationLinePointModel entries
     * @param {Number} rWidth
     * @param {Number} group
     * @param {Function} clicked
     */
    this.makeNetDots = function(data, rWidth, group, clicked){
        var self = this;
        var dot = this.g.selectAll(".netdot")
            .data(data)
            .enter();

        dot.append("circle")
            .attr("class", "netdot_back")
            .attr("cx", function (d) {return d.x;})
            .attr("cy", function (d) {return d.y;})
            .attr("r", rWidth)
            .attr("fill", 'white');

        dot.append("circle")
            .attr("class", "netdot")
            .attr("id", function (d) {
                return "circle_" + d.label + "_" + d.index;
            })
            .attr("cx", function (d) {return d.x;})
            .attr("cy", function (d) {return d.y;})
            .attr("r", rWidth)
            .attr("fill", function (d) {
                return self.colorWheel[d.label%self.colorWheel.length];
            })
            .on('click', clicked);
        dot.append("text")
            .attr("class", "text")
            .attr("x", function (d) {return d.x;})
            .attr("y", function (d) {return d.y+4;})
            .attr("fill", this.getColor(CHART, COL_FONT_ELEMENT))
            .attr("stroke", this.getColor(CHART, COL_FONT_ELEMENT))
            .style("font-size",  "16px")
            .attr("text-anchor", "middle")
            .text(function (d) {
                if(group==1)
                    return d.count;
                var min = d.count*group;
                var max = min+group;
                return '<'+max;
            })
            .on('click', clicked);
    };

    /**
     * make points for net chart based on given data
     * @param {Array} data
     * array with VisualisationPointModel entries
     * @param {Number} radius
     * @param {Number} scale
     * @returns {Object}
     */
    this.makeNet = function(data, radius, scale) {
        var self = this;
        var path = d3.line()
            .x(function (d) {
                var x = radius+self.getNetX(d.index*scale-Math.PI/2, radius);
                d.x = x;
                return d.x;
            })
            .y(function (d) {
                var y = radius+self.getNetY(d.index*scale-Math.PI/2, radius);
                d.y = y;
                return d.y;
            });
        var net = this.g.append("path")
            .datum(data.points)
            .attr("fill", self.getColor(VIS_NET, COL_SEC))
            .attr("stroke", self.getColor(VIS_NET, COL_MAIN))
            .attr("stroke-width", 2)
            .style("stroke-opacity", 0.5)
            .style("fill-opacity", 0.5)
            .attr("d", path);
        net.pointCounter = 0;
        return net;
    };

    /**
     * make line for radar chart based on given data
     * @param {Array} data
     * array with VisualisationLinePointModel entries
     * @param {Number} radius
     * @param {Number} scale
     * @returns {Object}
     */
    this.makeRadNet = function (data, radius, scale) {
        var self = this;
        var path = d3.line()
            .x(function (d) {
                var x = radius+self.getNetX(
                    d.label*scale-Math.PI/2,
                    self.scales.y(d.count)
                );
                d.x = x;
                return d.x;
            })
            .y(function (d) {
                var y = radius+self.getNetY(
                    d.label*scale-Math.PI/2,
                    self.scales.y(d.count)
                );
                d.y = y;
                return d.y;
            });
        var net = this.g.append("path")
            .datum(data.points)
            .attr("fill", 'none')
            .attr("stroke", self.getColor(VIS_RADAR, COL_SEC))
            .attr("stroke-width", 2)
            .style("stroke-opacity", 0.5)
            .style("fill-opacity", 0.5)
            .attr("d", path);
        net.pointCounter = 0;
        return net;
    };

    /**
     * make points for radar chart based on given data
     * @param {Array} data
     * array with VisualisationPointModel entries
     * @param {Function} clicked
     */
    this.makeRadDots = function (data, clicked) {
        var dot = this.g.selectAll(".raddot")
            .data(data)
            .enter();

        dot.append("circle")
            .attr("class", "raddot")
            .attr("id", function (d) {
                return "circle_" + d.label + "_" + d.index;
            })
            .attr("cx", function (d) {return d.x;})
            .attr("cy", function (d) {return d.y;})
            .attr("r", 4)
            .attr("fill", this.getColor(VIS_RADAR, COL_MAIN))
            .on('click', clicked);
    };

    /**
     * make legend for net chart
     * @param {Array} labelNames
     * @param {Number} rWidth
     * @param {Number} radius
     */
    this.makeNetLegend = function(labelNames, rWidth, radius) {
        for (var j = 0; j < labelNames.length; j++)
            this.makeNetLegendEntry(labelNames[j],rWidth, radius,j);
    };

    /**
     * make legend entry for net chart
     * @param {String} labelName
     * @param {Number} rWidth
     * @param {Number} radius
     * @param {Number} index
     */
    this.makeNetLegendEntry = function(labelName, rWidth, radius, index) {
        var self = this;
        this.g.append("circle")
            .attr("cx", radius*2+rWidth*2)
            .attr("cy", function () {return rWidth*index*2;})
            .attr("r", rWidth)
            .attr("fill", function () {
                return self.colorWheel[index%self.colorWheel.length];
            });

        this.g.append("text")
            .attr("class", "text")
            .attr("x", radius*2+rWidth*3)
            .attr("y", function () {return rWidth*index*2+4;})
            .attr("fill", this.getColor(CHART, COL_FONT_LABEL))
            .style("font-size",  "16px")
            .attr("text-anchor", "start")
            .text(labelName);
    };

    /**
     * make legend for radar chart
     * @param {Array} labelNames
     * @param {Number} radius
     * @param {Number} angle
     */
    this.makeRadLegend = function (labelNames, radius, angle) {
        var self = this;
        for (var j = 0; j < labelNames.length; j++){
            this.makeNetLegendEntry(labelNames[j],16, radius,j);
            var x = radius + this.getNetX(angle*j-Math.PI/2, radius);
            var y = radius + this.getNetY(angle*j-Math.PI/2, radius);
            this.g.append("circle")
                .attr("fill", self.colorWheel[j%self.colorWheel.length])
                .attr("cx", x)
                .attr("cy", y)
                .attr("r", 8);
        }

    };

    /**
     * make a black circle without fill at given position
     * @param {Number} x
     * @param {Number} y
     * @param {Number} radius
     */
    this.makeCircle = function(x, y, radius){
        this.g.append("circle")
            .attr("stroke", "black")
            .attr("fill", "none")
            .attr("cx", x)
            .attr("cy", y)
            .attr("r", radius);
    };

    /**
     * make a grid for given data
     * @param xTick
     * @param yTick
     */
    this.makeGrid = function(xTick, yTick){
        if(xTick!=null)
            this.g.append("g")
                .attr("class", "grid")
                .attr("transform", "translate(0," + this.height + ")")
                .call(d3.axisBottom(this.scales.x)
                    .ticks(xTick)
                    .tickSize(-this.height)
                    .tickFormat("")
                );
        else this.g.append("g")
            .attr("class", "grid")
            .attr("transform", "translate(0," + this.height + ")")
            .call(d3.axisBottom(this.scales.x)
                .tickSize(-this.height)
                .tickFormat("")
            );

        if(yTick!=null)
            this.g.append("g")
                .attr("class", "grid")
                .call(d3.axisLeft(this.scales.y)
                    .ticks(yTick)
                    .tickSize(-this.width)
                    .tickFormat("")
                );
        else this.g.append("g")
                .attr("class", "grid")
                .call(d3.axisLeft(this.scales.y)
                    .tickSize(-this.width)
                    .tickFormat("")
                );
    };

    this.makeGDPButtons = function (next, last) {
        this.g.append("text")
            .attr("class", "text")
            .attr("y", this.fullHeight-80)
            .attr("x", 0)
            .attr("fill", '#4682b4')
            .attr("stroke", '#4682b4')
            .style("font-size",  "16px")
            .attr("text-anchor", "start")
            .text("prev")
            .on("click", last)
        ;
        this.g.append("text")
            .attr("class", "text")
            .attr("y", this.fullHeight-80)
            .attr("x", this.width)
            .attr("fill", '#4682b4')
            .attr("stroke", '#4682b4')
            .style("font-size",  "16px")
            .attr("text-anchor", "end")
            .text("next")
            .on("click", next)
        ;
    };

    /**
     * get circle x-position for given angle and radius
     * @param {Number} angle
     * @param {Number} radius
     * @returns {Number}
     */
    this.getNetX = function(angle, radius) {
        return Math.cos(angle) * radius;
    };

    /**
     * get circle y-position for given angle and radius
     * @param {Number} angle
     * @param {Number} radius
     * @returns {Number}
     */
    this.getNetY = function(angle, radius) {
        return Math.sin(angle) * radius;
    };

    /**
     * make color palette for given count
     * @param {Number} count
     */
    this.makeColorWheel = function(count){
        this.makeColorWheel(count, "#0000cc");
    };

    /**
     * make color palette for given count
     * @param {Number} count
     * @param {String} color
     */
    this.makeColorWheel = function(count, color){
        var self = this;
        self.colorWheel = [];
        (new KolorWheel("#0000cc")).abs( 0,-1,-1,count).each(function() {
            self.colorWheel.push(this.getHex());
        });
    };

    /**
     * update color of given bar and background
     * @param {Object} bar
     * @param {Object} back
     * @param {Boolean} sel
     */
    this.updateBar = function(bar, back, sel) {
        if(sel){
            bar.style("fill", this.getColor(VIS_BLOCK, COL_MAIN_SEL));
            back.style("fill", this.getColor(VIS_BLOCK, COL_SEC_SEL));
        }else{
            bar.style("fill", this.getColor(VIS_BLOCK, COL_MAIN));
            back.style("fill", this.getColor(VIS_BLOCK, COL_SEC));
        }
    };

    /**
     * update color of given point
     * @param {Object} circle
     * @param {String} name
     * @param {Boolean} sel
     */
    this.updateDot = function (circle, name, sel) {
        if(sel){
            circle.style("fill", this.getColor(name, COL_MAIN_SEL));
        }else{
            circle.style("fill", this.getColor(name, COL_MAIN));
        }
    };

    /**
     * update color of given line
     * @param {Object} line
     * @param {String} name
     * @param {Boolean} sel
     * @param {Boolean} main
     */
    this.updateLine = function (line, name, sel, main) {
        if(sel){
            if(main){
                line.attr("stroke", this.getColor(name, COL_MAIN_SEL));
                line.attr("fill", this.getColor(name, COL_SEC_SEL));
            }
            else line.attr("stroke", this.getColor(name, COL_SEC_SEL));
        }else{
            if(main){
                line.attr("stroke", this.getColor(name, COL_MAIN));
                line.attr("fill", this.getColor(name, COL_SEC));
            }
            else line.attr("stroke", this.getColor(name, COL_SEC));
        }
    };



    this.makeTestLine = function(data, length, name, index) {
        var self = this;
        var color = '#000';
        if(name == 'runtime')
            color = self.colorWheel[index];
        else if(data.id === 'q1' || data.id === 'q1*' ||
            data.id === 't1' || data.id === 't1*'||
            data.id === 'T1' || data.id === 'Q1')
            color = '#f00';
        else if(data.id === 'q2' || data.id === 'q2*'||
            data.id === 't2' || data.id === 't2*'||
            data.id === 'T2' || data.id === 'Q2')
            color = '#00f';
        else if(data.id === 'E')
            color = '#0f0';
        else{
            var start = '#f';
            var mid = '';
            var end = '';
            var id = data.id;
            if(id >= length){
                if(id < length*2){
                    start = '#';
                    end = 'f';
                }else{
                    start = '#';
                    mid = 'f';
                }
                id %= length;
            }
            id *= 2;
            color = start+id+mid+id+end;
        }
        var path = d3.line()
            .x(function (d) {
                return self.scales.x(d.label);
            })
            .y(function (d) {
                return self.scales.y(d.count);
            })
            .curve(d3.curveCardinal);
        this.g.append("path")
            .datum(data.points)
            .attr("fill", "none")
            .attr("stroke", color)
            .attr("stroke-width", 2)
            .attr("d", path);

        var point = data.points[data.points.length-1];
        this.g.append("text")
            .attr("class", "text")
            .attr("y", function () {
                return self.scales.y(point.count);
            })
            .attr("x", function () {
                return self.scales.x(point.label);
            })
            .attr("fill", '#000')
            // .attr("stroke", '#000')
            .style("font-size",  "16px")
            .attr("text-anchor", "start")
            .text(function () {
                // return data.id;
                if(data.id == 'E')
                    return '';
                if(data.id == 'Q1' || data.id == 'Q2' ||
                    data.id == 'T1' || data.id == 'T2')
                    return data.id;
                return point.ids;
            })
        ;

    };
}