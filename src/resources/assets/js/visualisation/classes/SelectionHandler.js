// var Constants = require('../Constants');
// var SETT_AND_SEL = Constants.SETT_AND_SEL;

/**
 * class for handling the selection of svg elements
 * @param {Object} controller
 * visualisation controller
 * @constructor
 */
function VisualisationSelectionHandler(controller){
    this.controller = controller;
    this.settings = controller.settings;
    this.renderer = controller.renderer;

    this.selected = [];
    this.selectedElements = [];
    var self = this;

    /**
     * callback function for click on bars in histogram
     * @param {Object} data
     */
    this.clickBar = function (data) {
        var bar = d3.select("#bar_" + data.count);
        var backBar = d3.select("#back_bar_" + data.count);
        if (bar.classed("selected_bar")) {
            bar.classed("selected_bar", false);
            backBar.classed("selected_bar", false);
            self.renderer.updateBar(bar, backBar, false);
            self.updateSelected(data.ids, false);
        } else {
            bar.classed("selected_bar", true);
            backBar.classed("selected_bar", true);
            self.renderer.updateBar(bar, backBar, true);
            self.updateSelected(data.ids, true);
        }
    };

    /**
     * callback function for click on points in gdp
     * @param {Object} data
     */
    this.clickScatter = function (data) {
        var circle = d3.select("#circle_" + data.index);
        if (circle.classed("selected_dot")) {
            circle.classed("selected_dot", false);
            self.renderer.updateDot(circle, VIS_SCATTER, false);
            self.updateSelected(data.ids, false);
        } else {
            circle.classed("selected_dot", true);
            self.renderer.updateDot(circle, VIS_SCATTER, true);
            self.updateSelected(data.ids, true);
        }
    };

    /**
     * callback function for click on points in parallel coordinates
     * @param {Object} data
     */
    this.clickLine = function (data) {
        var circle = d3.select("#circle_" + data.index);
        if (circle.classed("selected_dot")) {
            circle.classed("selected_dot", false);
            self.renderer.updateDot(circle, VIS_LINE, false);
            self.updateMultiSelect(data, false, VIS_LINE);
        } else {
            circle.classed("selected_dot", true);
            self.renderer.updateDot(circle, VIS_LINE, true);
            self.updateMultiSelect(data, true, VIS_LINE);
        }
        if(self.settings[SETT_AND_SEL])
            self.updateMultiAndSelect(self.data, VIS_LINE);
    };

    /**
     * callback function for click on points in net chart
     * @param {Object} data
     */
    this.clickNet = function (data) {
        var circle = d3.select("#circle_" + data.label + "_" + data.index);
        if (circle.classed("selected_dot")) {
            circle.attr("fill-opacity", 1);
            circle.classed("selected_dot", false);
            self.updateMultiSelect(data, false, VIS_NET);
        } else {
            circle.attr("fill-opacity", 0.5);
            circle.classed("selected_dot", true);
            self.updateMultiSelect(data, true, VIS_NET);
        }
        if(self.settings[SETT_AND_SEL])
            self.updateMultiAndSelect(self.data, VIS_NET, true);
    };

    /**
     * callback function for click on points in radar chart
     * @param {Object} data
     */
    this.clickRad = function (data) {
        var circle = d3.select("#circle_" + data.label + "_" + data.index);
        if (circle.classed("selected_dot")) {
            circle.classed("selected_dot", false);
            self.renderer.updateDot(circle, VIS_RADAR, false);
            self.updateMultiSelect(data, false, VIS_RADAR);
        } else {
            circle.classed("selected_dot", true);
            self.renderer.updateDot(circle, VIS_RADAR, true);
            self.updateMultiSelect(data, true, VIS_RADAR);
        }
        if(self.settings[SETT_AND_SEL])
            self.updateMultiAndSelect(self.data, VIS_RADAR);
    };


    /**
     * updated selected ids with given ids
     * @param {Array} ids
     * @param {Boolean} add
     * @param {Boolean} clear
     */
    this.updateSelected = function(ids, add, clear) {
        if(clear)this.selected = [];
        if(add)this.selected = this.selected.concat(ids);
        else
            this.selected = this.selected.filter(
                function(value){
                    return ids.indexOf(value) == -1;
                }
            );
        // console.log('id count:'+this.selected.length);
        this.controller.emitSelected();
    };

    /**
     * updated selected ids with given data for OR logic
     * @param {Array} data
     * @param {Boolean} sel
     * @param {String} vis
     */
    this.updateMultiSelect = function(data, sel, vis) {
        if (sel){
            var add = true;
            for(var i =0;i<self.selectedElements.length;i++){
                if(self.selectedElements[i].label == data.label){
                    add = false;
                    break;
                }
            }
            if(add)self.selectedElements.push(data);
        }
        else{
            var index = self.selectedElements.indexOf(data);
            if(index >= 0)
                self.selectedElements.splice(index, 1);
        }

        var imageIds = [];
        for (var j = 0; j < data.lines.length; j++) {
            var line = data.lines[j];
            if (sel) {
                if (line.pointCounter == 0 && !self.settings[SETT_AND_SEL]) {
                    self.renderer.updateLine(line, vis, true);
                    imageIds.push(line.id);
                }
                line.pointCounter++;
            } else {
                line.pointCounter--;
                if (line.pointCounter == 0 && !self.settings[SETT_AND_SEL]) {
                    self.renderer.updateLine(line, vis, false);
                    imageIds.push(line.id);
                }
            }
        }

        if (!self.settings[SETT_AND_SEL])
            self.updateSelected(imageIds, sel);
    };

    /**
     * updated selected ids with given data for OR logic
     * @param {Array} data
     * @param {String} vis
     * @param {Boolean} main
     */
    this.updateMultiAndSelect = function(data, vis, main) {
        // console.log('updateMultiAndSelect');
        var imageIds = [];
        for(var i=0;i<data.lineData.length;i++){
            var line = data.lineData[i];
            if(line.line.pointCounter >= self.selectedElements.length &&
                self.selectedElements.length != 0 ||
                self.selectedElements.length == 0 &&
                line.line.pointCounter > 0){
                self.renderer.updateLine(line.line, vis, true, main);
                imageIds.push(line.line.id);
            }else self.renderer.updateLine(line.line, vis, false, main);
        }
        self.updateSelected(imageIds, true, true);
    };
}

// module.exports = VisualisationSelectionHandler;