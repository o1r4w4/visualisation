// var Constants = require('../Constants');
// var CHART = Constants.CHART;
// var GROUP = Constants.GROUP;
// var CHECK = Constants.CHECK;
// var COLOR = Constants.COLOR;
// var RADIO = Constants.RADIO;
// var NUMBER = Constants.NUMBER;

function VisualisationModel(name){
    this.name = name;
    this.show = true;
}

function VisualisationGroupModel(name, text, def){
    VisualisationModel.call(this, name);
    this.models = {};
    this.text = text;
    this.titles = {};
    this.default = def;
    this.type = GROUP;
    this.visualisation = CHART;
    this.add = function (model) {
        this.models[model.name] = model;
    };
    this.get = function (name) {
        return this.models[name];
    };
    this.clear = function () {
        this.models = {};
    };
    this.setTitles = function (update, reset) {
        this.titles.update = update;
        this.titles.reset = reset;
    };
}
VisualisationGroupModel.prototype = VisualisationModel.prototype;
VisualisationGroupModel.prototype.constructor = VisualisationGroupModel.constructor;

function VisualisationValueModel(name, value, type, text, title, clazz){
    VisualisationModel.call(this, name);
    this.value = value;
    this.default = value;
    this.type = type;
    this.text = text;
    this.title = title;
    this.visualisation = CHART;
    if(clazz != null)
        this.class = clazz;
    else {
        switch(this.type){
            case CHECK:
                this.class =  'vis_check_box_input';
                break;
            case COLOR:
                this.class =  'vis_color_input';
                break;
            case RADIO:
                this.class =  'vis_radio_input';
                break;
            case NUMBER:
                this.class =  'vis_number_input';
                break;
        }
    }
}
VisualisationValueModel.prototype = VisualisationModel.prototype;
VisualisationValueModel.prototype.constructor = VisualisationValueModel.constructor;


function VisualisationRadioModel(name, value, text, title, clazz){
    VisualisationValueModel.call(this, name, value, RADIO, text, title, clazz);
    this.radios = [];
    this.addRadio = function (value, text, title) {
        this.radios.push({
            value: value,
            text: text,
            title: title
        });
    };
}
VisualisationRadioModel.prototype = VisualisationValueModel.prototype;
VisualisationRadioModel.prototype.constructor = VisualisationRadioModel.constructor;



function VisualisationTestDataModel(name, date, id) {
    VisualisationModel.call(this, name);
    this.date =  date;
    this.volumeID = id;
    this.first = {
        right: 0,
        wrong: 0,
        results: []
    };
    this.second ={
        right: 0,
        wrong: 0,
        results: []
    };
    this.evaluation ={
        results: []
    };
    this.opinion = {
        results: []
    };
}
VisualisationTestDataModel.prototype = VisualisationModel.prototype;
VisualisationTestDataModel.prototype.constructor = VisualisationTestDataModel.constructor;

var testInd = 0;
function VisualisationTestModel(){
    VisualisationModel.call(this, testInd++);
    this.questions = [];
    this.version = '2_2';
}
VisualisationTestModel.prototype = VisualisationValueModel.prototype;
VisualisationTestModel.prototype.constructor = VisualisationTestModel.constructor;

var questInd = 0;
function VisualisationQuestModel(quest, answer){
    VisualisationModel.call(this, questInd++);
    this.question = quest;
    this.answer = answer;
}
VisualisationQuestModel.prototype = VisualisationValueModel.prototype;
VisualisationQuestModel.prototype.constructor = VisualisationQuestModel.constructor;

function VisualisationMultiQuestModel(conds, answer){
    var text = 'Find all image(s) with ';
    for(var i=0;i<conds.length;i++){
        text += conds[i];
        if(i<conds.length-1)
            text += ' and ';
    }
    text += ' annotation(s).';
    VisualisationQuestModel.call(this, text, answer);
}
VisualisationMultiQuestModel.prototype = VisualisationQuestModel.prototype;
VisualisationMultiQuestModel.prototype.constructor = VisualisationMultiQuestModel.constructor;

function VisualisationBlockModel(count, id){
    if(count == null || id == null){
        this.count = 0;
        this.ids = [];
    }else{
        this.count = count;
        this.ids = [id];
    }
    this.add = function (id) {
        this.ids.push(id);
    };
}
function VisualisationPointModel(label, index, count, id){
    VisualisationBlockModel.call(this, count, id);
    this.label = label;
    this.index = index;
}
VisualisationPointModel.prototype = VisualisationBlockModel.prototype;
VisualisationPointModel.prototype.constructor = VisualisationPointModel.constructor;

function VisualisationLinePointModel(label, index, count, id){
    VisualisationPointModel.call(this, label, index, count, id);
    this.lines = [];
}
VisualisationLinePointModel.prototype = VisualisationPointModel.prototype;
VisualisationLinePointModel.prototype.constructor = VisualisationLinePointModel.constructor;

function VisualisationLineModel(id){
    this.id = id;
    this.points = [];
}

function VisualisationBlockDataModel(name){
    this.labelName = name;
    this.data = [];
    this.xDomain = [];
    this.yDomain = [0, 0];
}
function VisualisationScatterDataModel(){
    this.labelNames = [];
    this.data = [];
    this.xDomain = [];
    this.yDomain = [];
    this.zDomain = [];
}
function VisualisationLineDataModel(){
    this.labelNames = [];
    this.lineData = [];
    this.pointData = [];
    this.xDomain = [];
    this.yDomain = [];
    this.zDomain = [];
}


// module.exports = {
//     VisualisationModel: VisualisationModel,
//     VisualisationGroupModel: VisualisationGroupModel,
//     VisualisationValueModel: VisualisationValueModel,
//     VisualisationRadioModel: VisualisationRadioModel,
//     VisualisationTestDataModel: VisualisationTestDataModel,
//     VisualisationTestModel: VisualisationTestModel,
//     VisualisationQuestModel: VisualisationQuestModel,
//     VisualisationBlockModel: VisualisationBlockModel,
//     VisualisationPointModel: VisualisationPointModel,
//     VisualisationLinePointModel: VisualisationLinePointModel,
//     VisualisationLineModel: VisualisationLineModel,
//     VisualisationBlockDataModel: VisualisationBlockDataModel,
//     VisualisationScatterDataModel: VisualisationScatterDataModel,
//     VisualisationLineDataModel: VisualisationLineDataModel,
// };