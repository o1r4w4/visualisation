/**
 * class for handling select, remove and clear of labels.
 * also handles the request via imagesWithLabel for new labels
 * @param {ImagesWithLabel} imagesWithLabel
 * api resource to get label data from
 * @constructor
 */
function VisualisationLabelHandler(imagesWithLabel){
    this.selectedLabels = [];

    /**
     * callback function for handling select in label tree.
     * if label data is cached addLabel is called.
     * else a get request via the imagesWithLabel resource is done.
     * on success addLabel is called, else remove label.
     * @param {Object} label
     * selected label
     * @param {Object} events
     * events object for emitting loading event
     * @param {Object} messages
     * messages object for printing error on fail
     * @param {Number} volumeId
     * the volume id for get request
     * @param {Object} controller
     * visualisation controller
     */
    this.updateLabel = function (label, events, messages, volumeId, controller) {
        var self = this;
        if (!controller.dataHandler.hasCached(label.id)) {
            events.$emit('loading.start');
            imagesWithLabel.get({tid: volumeId, lid: label.id}, {}).bind(this)
                .then(function (response) {
                    controller.dataHandler.cache(label.id, response.data);
                    self.addLabel(label,controller);
                }, function (response) {
                    self.removeLabel(label,controller);
                    messages.handleErrorResponse(response);
                }).finally(function () {
                events.$emit('loading.stop');
            });
        } else self.addLabel(label,controller);
    };

    /**
     * callback function for handling deselect in label tree.
     * removes label from selectedLabels
     * @param {Object} label
     * deselected label
     * @param {Object} controller
     * visualisation controller
     */
    this.removeLabel= function (label,controller) {
        var index = this.selectedLabels.indexOf(label);
        if (index !== -1) {
            this.selectedLabels.splice(index, 1);
            controller.updateSelectModels();
        }
    };

    /**
     * callback function for handling clear in label tree.
     * @param {Object} controller
     * visualisation controller
     */
    this.clearLabels= function (controller) {
        this.selectedLabels.splice(0);
        controller.updateSelectModels();
    };

    /**
     * adds given label to selectedLabels if not already contained
     * @param {Object} label
     * label to add
     * @param {Object} controller
     * visualisation controller
     */
    this.addLabel= function (label,controller) {
        if (this.selectedLabels.indexOf(label) === -1) {
            this.selectedLabels.push(label);
            controller.updateSelectModels();
        }
    };
}

// module.exports = VisualisationLabelHandler;