biigle.$component('visualisation.components.visualisationValueEdit', {
    template:
    '<div class="visualisation-value-edit">'+
    '<input :type="model.type" :class="getClass" '+
    'v-model="model.value" :name="model.name" :title="title" v-on:change="change">'+
    '<label :name="model.name"> {{ text }} </label>'+
    '</div>',
    props: {
        model: {
            type: Object,
            required: true,
        },
    },
    computed: {
        getClass: function () {
            if(this.model.class !=null)
                return  'form-control '+this.model.class;
            return  'form-control';
        },
        text: function () {
            return this.model.text || '';
        },
        title: function () {
            return this.model.title || this.text;
        }
    },
    methods: {
        change: function () {
            if(this.model.range != null){
                if(this.model.range.min != null)
                    this.model.value=Math.max(this.model.range.min,this.model.value);
                if(this.model.range.max != null)
                    this.model.value=Math.min(this.model.range.max,this.model.value);
            }
            if (this.model.value != null)
            this.$emit('update', this.model);
        }
    }
});