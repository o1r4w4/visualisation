biigle.$component('visualisation.components.visualisationChart', {
    template:
    '<div class="visualisation-chart">' +
    '<svg width="100%" height="100%" id="visualisation-svg"></svg>' +
    '</div>',
    data: {
        data: null,
        chart: null,
    },
    props: {
        colors: {
            type: Object,
            required: true,
        },
        settings: {
            type: Object,
            required: true,
        },
    },
    methods: {
        updateChart: function (heading, data) {
            this.selectHandler.selected = [];
            this.selectHandler.selectedElements = [];
            this.selectHandler.data = data;
            this.renderer.initChart(heading);
            if(!this.settings[USE_SIZE])
                this.$emit('size_updated',
                    this.renderer.fullWidth, this.renderer.fullHeight);
        },
        updateBlankChart: function (data) {
            this.updateChart(TEXT_HEADING_BLANK, data);

            this.data = data;
            this.curChart = this.updateBlankChart;
        },
        updateBlockChart: function (data) {
            this.updateChart(NAME_BLOCK, data);
            var self = this;
            this.renderer.setScale('x', this.settings[SETT_BLOCK_SCALE_X],
                [0, this.renderer.width],
                data.xDomain
            );
            this.renderer.setScale('y', this.settings[SETT_BLOCK_SCALE_Y],
                [this.renderer.height, 0],
                data.yDomain
            );
            var tickText = null;
            if(self.settings[SETT_GROUP]>1)
                tickText =function (d) {
                    var group = +self.settings[SETT_GROUP];
                    return '<'+(d*group+group);
                };
            this.renderer.makeAxis(
                d3.axisBottom(this.renderer.scales.x).ticks(data.data.length - 1, ""),
                'axis',
                'translate(0,' + this.renderer.height + ')',
                null,null,tickText
            );
            this.renderer.makeAxisLabel(
                -this.renderer.height,
                this.renderer.width+(+this.settings[FONT_SIZE_LABEL]),
                data.labelName + TEXT_SPACE+TEXT_ANNO_PER_IMG,
                'rotate(-90)');

            this.renderer.makeAxis(
                d3.axisLeft(this.renderer.scales.y),
                'axis'
            );
            this.renderer.makeAxisLabel(
                0,
                -this.settings[FONT_SIZE_LABEL],
                TEXT_NUMB_OF_IMGS
            );

            this.renderer.makeBars(data.data, this.settings[SETT_SHOW_VALUES],
                this.selectHandler.clickBar);

            this.data = data;
            this.curChart = this.updateBlockChart;
        },

        updateScatterChart: function (data) {
            this.updateChart(NAME_SCATTER, data);

            this.renderer.setScale('x', this.settings[SETT_SCATTER_SCALE_X],
                [0, this.renderer.width],
                data.xDomain
            );
            this.renderer.setScale('y', this.settings[SETT_SCATTER_SCALE_Y],
                [this.renderer.height, 0],
                data.yDomain
            );

            var zMax = Math.min(
                this.renderer.scales.x(1) - this.renderer.scales.x(0),
                this.renderer.scales.y(0) - this.renderer.scales.y(1)) / 2;
            zMax = Math.min(this.settings[SETT_RADIUS_MAX], zMax);
            this.renderer.setScale('z', 'linear',
                [
                    this.settings[SETT_RADIUS_MIN], zMax
                ],
                data.zDomain
            );
            this.renderer.makeGrid(data.labelNames);

            this.renderer.makeAxis(
                d3.axisBottom(this.renderer.scales.x).ticks(data.labelNames.length + 1, ""),
                'axis',
                'translate(0,' + this.renderer.height + ')',
                'translate (-10, 16) rotate(-45)',
                'end',
                function (d) {
                    if (d < 0 || d >= data.labelNames.length)
                        return '';
                    return data.labelNames[d];
                }
            );
            this.renderer.makeAxisLabel(
                -this.renderer.height,
                this.renderer.width+(+this.settings[FONT_SIZE_LABEL]),
                TEXT_LABELS,
                'rotate(-90)');
            var self = this;
            var tickText = null;
            if(self.settings[SETT_GROUP]>1)
                tickText =function (d) {
                    var group = +self.settings[SETT_GROUP];
                    return '<'+(d*group+group);
                };
            this.renderer.makeAxis(
                d3.axisLeft(this.renderer.scales.y).ticks(data.yDomain[1], ""),
                'axis',null,null,null,tickText
            );
            this.renderer.makeAxisLabel(
                0,
                -this.settings[FONT_SIZE_LABEL],
                TEXT_ANNO_PER_IMG
            );

            this.renderer.makeDots(data.data, this.settings[SETT_SHOW_VALUES],
                this.selectHandler.clickScatter);

            this.data = data;
            this.curChart = this.updateScatterChart;
        },


        updateGDPChart: function (data, index) {
            if(index == null)index = 0;
            this.gdpIndex = index;
            this.updateChart(NAME_GDP, data);
            this.updateGDPSubChart(data.data[index]);
            this.data = data;
            this.curChart = this.updateGDPChart;
        },

        updateGDPSubChart: function (data, x, y, width, height) {


            this.renderer.setScale('x', this.settings[SETT_SCATTER_SCALE_X],
                [0, this.renderer.width],
                data.xDomain
            );
            this.renderer.setScale('y', this.settings[SETT_SCATTER_SCALE_Y],
                [this.renderer.height, 0],
                data.yDomain
            );

            var zMax = Math.min(
                this.renderer.scales.x(1) - this.renderer.scales.x(0),
                this.renderer.scales.y(0) - this.renderer.scales.y(1)) / 2;
            zMax = Math.min(this.settings[SETT_RADIUS_MAX], zMax);
            this.renderer.setScale('z', 'linear',
                [
                    this.settings[SETT_RADIUS_MIN], zMax
                ],
                data.zDomain
            );

            this.renderer.makeGrid();

            var self = this;
            var tickText = function (d) {
                if(d<0)return '';
                return d;
            };
            if(self.settings[SETT_GROUP]>1)
                tickText =function (d) {
                    if(d<0)return '';
                    var group = +self.settings[SETT_GROUP];
                    return '<'+(d*group+group);
                };
            this.renderer.makeAxis(
                d3.axisBottom(this.renderer.scales.x).ticks(data.xDomain[1], ""),
                'axis',
                'translate(0,' + this.renderer.height + ')',
                null,null,tickText
            );
            this.renderer.makeAxisLabel(
                -this.renderer.height,
                this.renderer.width+(+this.settings[FONT_SIZE_LABEL]),
                data.labelNames[0]+ TEXT_SPACE+TEXT_ANNO_PER_IMG,
                'rotate(-90)');
            this.renderer.makeAxis(
                d3.axisLeft(this.renderer.scales.y).ticks(data.yDomain[1], ""),
                'axis',
                null,null,null,tickText
            );
            this.renderer.makeAxisLabel(
                0,
                -this.settings[FONT_SIZE_LABEL],
                data.labelNames[1]+ TEXT_SPACE+TEXT_ANNO_PER_IMG
            );

            this.renderer.makeDots(data.data, this.settings[SETT_SHOW_VALUES],
                this.selectHandler.clickScatter);

            this.renderer.makeGDPButtons(this.nextGDP, this.lastGDP);
        },

        nextGDP:function () {
            this.gdpIndex++;
            this.gdpIndex %= this.data.data.length;
            this.updateGDPChart(this.data, this.gdpIndex);
        },

        lastGDP:function () {
            this.gdpIndex--;
            if(this.gdpIndex<0)this.gdpIndex += this.data.data.length;
            this.updateGDPChart(this.data, this.gdpIndex);
        },

        updateLineChart: function (data) {
            this.updateChart(NAME_LINE, data);
            this.renderer.setScale('x', this.settings[SETT_LINE_SCALE_X],
                [0, this.renderer.width],
                data.xDomain
            );
            this.renderer.setScale('y', this.settings[SETT_LINE_SCALE_Y],
                [this.renderer.height, 0],
                data.yDomain
            );

            var zMax = Math.min(
                this.renderer.scales.x(1) - this.renderer.scales.x(0),
                this.renderer.scales.y(0) - this.renderer.scales.y(1)) / 4;
            zMax = Math.min(this.settings[SETT_RADIUS_MAX], zMax);
            this.renderer.setScale('z', 'linear',
                [
                    this.settings[SETT_RADIUS_MIN], zMax
                ],
                data.zDomain
            );

            this.renderer.makeGrid(data.labelNames.length+1);

            this.renderer.makeAxis(
                d3.axisBottom(this.renderer.scales.x).ticks(data.labelNames.length + 1, ""),
                'axis',
                'translate(0,' + this.renderer.height + ')',
                'translate (-10, 16) rotate(-45)',
                'end',
                function (d) {
                    if (d < 0 || d >= data.labelNames.length)
                        return '';
                    return data.labelNames[d];
                }
            );
            this.renderer.makeAxisLabel(
                -this.renderer.height,
                this.renderer.width+(+this.settings[FONT_SIZE_LABEL]),
                TEXT_LABELS,
                'rotate(-90)');

            var self = this;
            var tickText = null;
            if(self.settings[SETT_GROUP]>1)
                tickText =function (d) {
                    var group = +self.settings[SETT_GROUP];
                    return '<'+(d*group+group);
                };
            this.renderer.makeAxis(
                d3.axisLeft(this.renderer.scales.y).ticks(data.yDomain[1], ""),
                'axis',null,null,null,tickText
            );
            this.renderer.makeAxisLabel(
                0,
                -this.settings[FONT_SIZE_LABEL],
                TEXT_ANNO_PER_IMG
            );

            for (var j = 0; j < data.lineData.length; j++) {
                var imgId = data.lineData[j].id;
                var line = this.renderer.makeLine(data.lineData[j].points,
                    this.modes[this.settings[SETT_LINE_DRAW]]);
                data.lineData[j].line = line;
                line.id = imgId;
                for (var i = 0; i < data.lineData[j].points.length; i++)
                    data.lineData[j].points[i].lines.push(line);
            }
            this.renderer.makeLineDots(data.pointData, this.settings[SETT_SHOW_VALUES],
                this.selectHandler.clickLine);
            this.data = data;
            this.curChart = this.updateLineChart;
        },
        updateNetChart: function (data) {
            this.updateChart(NAME_NET, data);
            var radius = Math.min(this.renderer.width, this.renderer.height)/2;
            var radiants = Math.PI/data.pointData.length*2;
            var degree = 360/data.pointData.length;
            degree = Math.max(degree, 8);
            degree = Math.min(degree, 16);

            this.renderer.makeCircle(radius, radius, radius);

            this.renderer.makeColorWheel(data.labelNames.length);

            for (var j = 0; j < data.lineData.length; j++) {
                var imgId = data.lineData[j].id;
                var net = this.renderer.makeNet(data.lineData[j], radius, radiants);
                data.lineData[j].line = net;
                net.id = imgId;
                for (var i = 0; i < data.lineData[j].points.length; i++)
                    if(data.lineData[j].points[i].lines.indexOf(net)==-1)
                        data.lineData[j].points[i].lines.push(net);
            }

            this.renderer.makeNetDots(data.pointData, degree, +this.settings[SETT_GROUP],
                this.selectHandler.clickNet);

            this.renderer.makeNetLegend(data.labelNames, degree, radius);

            this.data = data;
            this.curChart = this.updateNetChart;
        },
        updateRadarChart: function (data) {
            this.updateChart(NAME_RADAR, data);
            var radius = Math.min(this.renderer.width, this.renderer.height)/2;

            this.renderer.setScale('y', this.settings[SETT_RADAR_SCALE],
                [0, radius],
                data.yDomain
            );
            this.renderer.makeCircle(radius, radius, radius);

            var radiants = Math.PI/data.labelNames.length*2;
            for (var j = 0; j < data.lineData.length; j++) {
                var imgId = data.lineData[j].id;
                var net = this.renderer.makeRadNet(data.lineData[j], radius,radiants);
                data.lineData[j].line = net;
                net.id = imgId;
                for (var i = 0; i < data.lineData[j].points.length; i++)
                    if(data.lineData[j].points[i].lines.indexOf(net)==-1)
                        data.lineData[j].points[i].lines.push(net);
            }

            var angle = 0;
            var degree = 360/data.labelNames.length;
            var self = this;

            var firstTickText = function (d) {
                if (d < 0 || d>data.yDomain[1]-1)return '';
                return d;
            };
            if(self.settings[SETT_GROUP]>1)
                firstTickText =function (d) {
                    if (d < 0)return '';
                    var group = +self.settings[SETT_GROUP];
                    return '<'+(d*group+group);
                };
            var otherTickText = function () {
                return '';
            };
            for (j = 0; j < data.labelNames.length; j++){
                var tickText = firstTickText;
                if(j>0)tickText = otherTickText;
                this.renderer.makeAxis(
                    d3.axisBottom(this.renderer.scales.y).ticks(data.yDomain[1] + 1, ""),
                    'axis',
                    'translate(' + radius + ','+ radius +') rotate('+(angle-90)+')',
                    'translate (10, 10) rotate(90)',
                    'start',
                    tickText
                );
                angle += degree;
            }

            this.renderer.makeRadDots(data.pointData, this.selectHandler.clickRad);

            this.renderer.makeColorWheel(data.labelNames.length);
            this.renderer.makeRadLegend(data.labelNames, radius, radiants);

            this.data = data;
            this.curChart = this.updateRadarChart;
        },
        emitSelected: function () {
            this.$nextTick(function () {
                this.$emit('update_selected', this.selectHandler.selected);
            });
        },
        redraw: function () {
            if(this.curChart != null)
                this.curChart(this.data);
        },
        updateTestChart: function (data, group) {
            this.updateChart('', data);
            if(group == null)group = data.group;
            else data.group = group;
            this.renderer.setScale('x', this.settings[SETT_LINE_SCALE_X],
                [0, this.renderer.width],
                data.xDomain
            );
            this.renderer.setScale('y', this.settings[SETT_LINE_SCALE_Y],
                [this.renderer.height, 0],
                data.yDomain
            );

            this.renderer.makeGrid();

            this.renderer.makeAxis(
                d3.axisBottom(this.renderer.scales.x).ticks(data.labelNames.length + 1, ""),
                'axis',
                'translate(0,' + this.renderer.height + ')',
                null, null,
                function (d) {
                    if(data.yName == "runtime")
                        return data.labelNames[d];
                    if (d < 0 || d >= data.labelNames.length)
                        return '';
                    return data.labelNames[d];
                }
            );
            this.renderer.makeAxisLabel(
                -this.renderer.height,
                this.renderer.width+(+this.settings[FONT_SIZE_LABEL]),
                data.labelX,
                'rotate(-90)');

            var tickText = function (d) {
                if(d<0 || data.yName == 'quota' && d>1)return '';
                if(data.yName == 'quota')
                    return d*100;
                return d;
            };
            if(group>1)
                tickText = function (d) {
                    group = +group;
                    return '<'+(d*group+group);
                };
            this.renderer.makeAxis(
                d3.axisLeft(this.renderer.scales.y).ticks(data.yDomain[1]+1, ""),
                'axis',null,null,null,tickText
            );
            this.renderer.makeAxisLabel(
                0,
                -this.settings[FONT_SIZE_LABEL],
                data.labelY
            );

            if(data.yName == 'runtime')
                this.renderer.makeColorWheel(data.lineData.length);

            for (var j = 0; j < data.lineData.length; j++)
                this.renderer.makeTestLine(
                    data.lineData[j],
                    data.answerLength,
                    data.yName, j);

            this.data = data;
            this.curChart = this.updateTestChart;
        },
    },
    created: function () {
        this.renderer = new VisualisationRenderer(this.colors, this.settings);
        this.selectHandler = new VisualisationSelectionHandler(this);
        this.curChart = this.updateBlankChart;

        this.scales = {};
        this.scales[LIN] = SCALE_LIN;
        this.scales[BAND] = SCALE_BAND;
        this.scales[LOG] = SCALE_LOG;

        this.modes = {};
        this.modes[LIN] = d3.curveLinear;
        this.modes[CARD] = d3.curveCardinal;
    }
});