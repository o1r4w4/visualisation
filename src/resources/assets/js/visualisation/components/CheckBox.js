biigle.$component('visualisation.components.visualisationCheckBox', {
    template:
    '<div class="visualisation-check-box">'+
    '<input type="checkbox" v-on:change="change" :class="model.class" ' +
    'v-model="model.value" :name="model.name" :title="title">'+
    '<label :name="model.name"> {{ text }} </label>'+
    '</div>',
    props: {
        model: {
            type: Object,
            required: true,
        },
    },
    methods: {
        change: function () {
            if (this.model.value != null)
            this.$emit('update', this.model);
        }
    },
    computed: {
        text: function () {
            return this.model.text || '';
        },
        title: function () {
            return this.model.title || this.text;
        }
    }
});