biigle.$component('visualisation.components.visualisationSelect', {
    template:
    '<div class="visualisation-select">' +
    '<select class="form-control" ' +
    'v-on:change="change" v-model="model.value" required>' +
    '<option v-for="subModel in model.models" ' +
    ':value="subModel" v-text="subModel.text"></option>' +
    '</select>'+
    '</div>',
    props: {
        model: {
            type: Object,
            required: true,
        },
    },
    methods: {
        change: function () {
            if (this.model.value != null)
                this.$emit('update', this.model);
        },
    },
});
