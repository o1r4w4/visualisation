biigle.$component('visualisation.components.visualisationRadio', {
    template:
    '<div class="visualisation-radio">'+
    '<h5>{{ text }}</h5>'+
    '<div v-for="radio in model.radios">'+
    '<input type="radio" v-on:change="change" :class="model.class" ' +
    'v-model="model.value" :name="model.name" :value="radio.value" :title="radio.title">'+
    '<label > {{ radio.text }} </label>'+
    '</div>'+
    '</div>',
    props: {
        model: {
            type: Object,
            required: true,
        },
    },
    methods: {
        change: function () {
            if (this.model.value != null)
                this.$emit('update', this.model);
        }
    },
    computed: {
        text: function () {
            return this.model.text || '';
        },
    }
});