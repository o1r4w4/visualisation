biigle.$component('visualisation.components.visualisationValueEditGroup', {
    template:
    '<div class="visualisation-value-edit-group"' +
    ' v-if="model.show">'+

    '<h4> {{text}} </h4>' +

    '<button v-if="showUpdate" type="submit" class="btn btn-default"'+
    'v-on:click="update" :title="updateTitle">'+
    '<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>'+
    '</button>'+

    '<button v-if="showReset" type="submit" class="btn btn-default"'+
    'v-on:click="reset" :title="resetTitle">'+
    '<span class="glyphicon glyphicon-repeat" aria-hidden="true"></span>'+
    '</button>'+

    '<div v-for="model in model.models">' +

    '<visualisation-value-edit ' +
    'v-if="isType(model, NUMBER)||isType(model, COLOR)"' +
    ':model="model" v-on:update="updateValue">' +
    '</visualisation-value-edit>'+

    '<visualisation-check-box v-if="isType(model, CHECK)"' +
    ':model="model" v-on:update="updateValue">' +
    '</visualisation-check-box>'+

    '<visualisation-radio v-if="isType(model, RADIO)"' +
    ':model="model" v-on:update="updateValue">' +
    '</visualisation-radio>'+

    '<visualisation-value-edit-group v-if="isType(model, GROUP)"' +
    ':model="model" v-on:update="updateGroup">' +
    '</visualisation-value-edit-group>'+

    '</div>'+

    '</div>',
    components: {
        visualisationValueEdit:
            biigle.$require('visualisation.components.visualisationValueEdit'),
        visualisationValueEditGroup:
            biigle.$require('visualisation.components.visualisationValueEditGroup'),
        visualisationCheckBox:
            biigle.$require('visualisation.components.visualisationCheckBox'),
        visualisationRadio:
            biigle.$require('visualisation.components.visualisationRadio'),
    },
    props: {
        model: {
            type: Object,
            required: true,
        },
    },
    computed: {
        showUpdate: function () {
            if(this.model.showUpdate==null)
                return true;
            return this.model.showUpdate;
        },
        showReset: function () {
            if(this.model.showReset==null)
                return true;
            return this.model.showReset;
        },
        updateTitle: function () {
            if(this.model.titles!=null)
                return this.model.titles.update || 'Update';
            return '';
        },
        resetTitle: function () {
            if(this.model.titles!=null)
                return this.model.titles.reset || 'Reset';
            return '';
        },
        text: function () {
            return this.model.text || '';
        }
    },
    methods: {
        updateValue: function (model) {
            if(this.model.updateValue)
                this.$emit('update', model, false);
        },
        updateGroup: function (name, model) {
            if(this.model.updateGroup)
                this.$emit('update', model, true);
        },
        isType: function (model, type) {
            return model.type == type;
        },
        update: function () {
            this.$emit('update', this.model, true);
        },
        reset: function () {
            if(this.model.default){
                for (var key in this.model.models)
                    this.model.models[key].value = this.model.models[key].default;
                this.update();
            }
            else this.$emit('reset', this.model);
        },
        setValue: function (name, value) {
            if(this.model.models[name]!=null)
                this.model.models[name].value = +value;
        }
    },
});