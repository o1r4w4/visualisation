/**
 * visualisation filter for the volume overview filters.
 */
(function () {
    var key = 'biigle.visualisation.imageSequence.' + biigle.$require('volumes.volumeId');

    var filter = {
        id: 'visualisation',
        label: 'visualisation selection',
        help: "All images that were (not) selected in a visualisation.",
        listComponent: {
            mixins: [biigle.$require('volumes.components.filterListComponent')],
            data: function () {
                return {name: 'visualisation selection'};
            },
            created: function () {
                var self = this;
                window.addEventListener('storage', function () {
                    self.$emit('refresh', self.rule);
                });
            },
        },
        getSequence: function (volumeId) {
            return new Vue.Promise(function (resolve, reject) {
                resolve({data: JSON.parse(localStorage.getItem(key)) || []});
            });
        }
    };

    biigle.$require('volumes.stores.filters').push(filter);
})();
