@extends('manual.base')

@section('manual-title') Visualisations @stop

@section('manual-content')
    <div class="row">
        <p class="lead">
            How to choose labels and visualisations.
        </p>
        <p>
            Click the labels tab in the right sidebar
            to get an overview of all label trees
            and their labels of the current volume.
        </p>
        <p class="text-center">
            <img src="{{asset('vendor/visualisation/images/manual/ss_vis_0.png')}}"
                 width="75%">
        </p>
        <p>
            By clicking on the label names You can select and deselect the label
            or clear all selected labels by clicking the clear-selected-labels-button.
            You can change the order of selected labels by the order You click them.
        </p>
        <p class="text-center">
            <img src="{{asset('vendor/visualisation/images/manual/ss_label_0.png')}}"
                 width="75%">
        </p>
        <p>
            After adding a label the visualisation-select is updated
            with the available visualisations
            and the last chosen or default visualisation is drawn.
            By choosing a visualisation from the visualisation-select
            You can change the drawn visualisation.
        </p>
        <p class="text-center">
            <img src="{{asset('vendor/visualisation/images/manual/ss_vis_sel_0.png')}}"
                 width="75%">
        </p>
        <ul>
            <li>
                <b>Histogram</b>
                <p>Available with 1 label selected.</p>
                <p>Best suited to get a quick overview for a labels annotations.</p>
                <p>
                    Bars are displayed in a 2D coordinate system
                    with number of annotation on x-
                    and number of images on y-axis.</p>
            </li>
            <li>
                <b>Generalized drafter's plot</b>
                <p>Available with 2 labels selected.</p>
                <p>Best suited to get an overview for the annotations of 2 labels.</p>
                <p>
                    Points are displayed in a 2D coordinate system with
                    number of annotation for first label on x-
                    and number of annotation for second label on y-axis.
                    Point radius represents number of images.
                </p>
            </li>
            <li>
                <b>Parallel coordinates</b>
                <p>Available with 2 or more labels selected.</p>
                <p>
                    Best suited to get an overview for the annotations
                    of more then 2 labels.
                </p>
                <p>
                    Points are displayed in a 2D coordinate system with labels on x-
                    and number of annotation on y-axis.
                    Point radius represents number of images.
                    Every image is displayed as a line connecting its points.
                </p>
            </li>
            <li>
                <b>Netmap display</b>
                <p>Available with 3 or more labels selected.</p>
                <p>
                    Number of annotations per label are arranged as points in a circle.
                    Every image is displayed as a polygon connecting its points.
                </p>
            </li>
            <li>
                <b>Radar display</b>
                <p>Available with 3 or more labels selected.</p>
                <p>
                    Points are displayed on multiple axis
                    for every labels number of annotations,
                    wich are arranged in a circle.
                    Every image is displayed as a polygon connecting its points.
                </p>
            </li>
        </ul>
        @include('visualisation::manual.tutorials.list', ['name'=>'visualisations'])
    </div>
@endsection
