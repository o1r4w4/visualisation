@extends('manual.base')

@section('manual-title') Size @stop

@section('manual-content')
    <div class="row">
        <p class="lead">
            How to edit size values.
        </p>
        <p>
            In the sidebar the second tab is for editing size values of the chart.
        </p>
        <p>
            By default the chart always fits the parent element,
            but You can set width and height of the chart manually,
            by editing the input-fields an clicking the update-button.
            If the chart's size gets bigger than the parent element a scrollbar will be displayed,
            so the full chart can still be viewed.
            By clicking the reset button the default size behaviour of the chart is enabled.
        </p>
        <p>
            The margin values determin the space between the chart's edges and the drawn visualisation.
            You can edit the margin for top, right, left and bottom
            by editing the input-fields an clicking the update-button.
            By clicking the reset button the default values are restored.
        </p>
        <p>
            The font values determin font size of several text elements of the chart,
            like the heading above the drawn visualisation,
            the text at the end or at the ticks of an axis.
            You can edit the font size for heading, axis-label and -tick
            by editing the input-fields an clicking the update-button.
            By clicking the reset button the default values are restored.
        </p>
        <p>
            Any changes You make for the size settings will be stored in the local storage of Your browser and
            loaded with the next usage of this module.
        </p>
        @include('visualisation::manual.tutorials.list', ['name'=>'size'])
    </div>
@endsection
