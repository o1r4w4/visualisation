@extends('manual.base')

@section('manual-title') Getting Started @stop

@section('manual-content')
    <div class="row">
        <p class="lead">
            A quick introduction to the visualisation module.
        </p>
        <p>
            The visualisation module can be used to get a visual representation for the annotations
            of a volume for a selected label or labels.
            Furthermore this module enables a filter for the image-view of a volume
            by selecting elements of the visualisations.
        </p>
        <p>
            To access this module enter the image-view of a volume
            and click on the visual-filter-tab in the left sidebar,
            which will direct You to the page of this module
        </p>
        <p class="text-center">
            <img src="{{asset('vendor/visualisation/images/manual/ss_volume_0.png')}}"
                 width="75%">
        </p>
        <p>
            In the top-bar You will find a link to the current volume's image-view,
            which can be used to get back to the overview and to enable the visualisation-filter.
            On the left side there are the visualisation-select for choosing the displayed visualisation
            and a chart for displaying the selected visualisation.
            On the right side there is a sidebar with several tabs for choosing labels,
            changing colors and settings, downloads and usability-tests.
        </p>
        @include('visualisation::manual.tutorials.list', ['name'=>'getting-started'])
    </div>
@endsection
