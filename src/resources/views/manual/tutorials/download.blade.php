@extends('manual.base')

@section('manual-title') Download @stop

@section('manual-content')
    <div class="row">
        <p class="lead">
            How to download the chart.
        </p>
        <p>
            In the sidebar the fifth tab is for downloading the chart.
        </p>
        <p>
            You can download the displayed chart as SVG or PNG by clicking the corresponding button.
        </p>
        @include('visualisation::manual.tutorials.list', ['name'=>'download'])
    </div>
@endsection
