@extends('manual.base')

@section('manual-title') Usability test @stop

@section('manual-content')
    <div class="row">
        <p class="lead">
            How to make a usability test.
        </p>
        <p>
            First You must be a member of the Visualisation Usability Tests project
            to access the volume Usability Selection.
            Open this modules page for that volume and select the last tab from the sidebar.
            Here You must enter a pseudonym
            and can start the tests by clicking on the play button.
            <b>
                It is advised to set all settings to the default value
                before starting the test.
            </b>
        </p>
        <p>
            After that You will have to make one test iteration
            without and a second one with the help of this module.
            This means, that the label selection will be disabled for the first iteration
            and enabled after completing that iteration.
            <b>
                It is advised to click the clear labels button
                when starting the second iteration.
            </b>
            Each iteration consists of five questions,
            for which You will have to find several images
            with annotations for different label(s).
            You can select the images from a drop down input
            and add them to Your answer by clicking the plus button.
            This will add a button with the name of the image,
            which can be clicked to remove the image from Your answer.
            If You are satisfied with Your answer click the play button
            to get the next question.
            The following shows some example questions:
        </p>
        <ul>
            <li>
                Find all image(s) with exactly 1 Brisingida annotation(s).
            </li>
            <li>
                Find all image(s) with less then 4 Paxillosida annotation(s).
            </li>
            <li>
                Find all image(s) with more then 2 Velatida annotation(s).
            </li>
        </ul>
        <p>
            For the first question only those images with exactly 1 Brisingida
            annotation should be given as answer,
            while for the second question only images with 1, 2 or 3 Paxillosida annotations
            and for the last question only images with 3, 4, 5, ...
            Velatida annotations should be given as answer.
        </p>
        <p>
            After those two iterations several questions
            about Your opinion of this module are asked and You can leave a comment.
        </p>
        <p>
            The test data is added to an email, which should open in Your default email client.
            Please send this email to the given address.
        </p>

        @include('visualisation::manual.tutorials.list', ['name'=>'test'])
    </div>
@endsection
