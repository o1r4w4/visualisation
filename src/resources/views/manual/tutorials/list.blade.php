<?php
$links = array(
    array(
        "route"=>"getting-started",
        "text"=>"1. A quick introduction to the visualisation module."
    ),
    array(
        "route"=>"visualisations",
        "text"=>"2. How to choose labels and visualisations."
    ),
    array(
        "route"=>"filter",
        "text"=>"3. How to select elements and filter images."
    ),
    array(
        "route"=>"size",
        "text"=>"4. How to edit size values."
    ),
    array(
        "route"=>"color",
        "text"=>"5. How to edit color values."
    ),
    array(
        "route"=>"settings",
        "text"=>"6. How to edit settings."
    ),
    array(
        "route"=>"download",
        "text"=>"7. How to download the chart."
    ),
    array(
        "route"=>"test",
        "text"=>"8. How to make a usability test."
    ),
);
?>
{{--@include('visualisation::manual.tutorials.tutorials')--}}
<ul>
    <?php
    foreach ($links as $link){
    if($link["route"] != $name){
    ?>
    <li>
        <a href="{{route('manual-tutorials', ['visualisation', $link["route"]])}}">
            <?php echo $link["text"];?>
        </a>
    </li>
    <?php
    }else{?>
        <li>
            <p>
                <?php echo $link["text"];?>
            </p>
        </li>
        <?php
            }
    }
    ?>
</ul>