@extends('manual.base')

@section('manual-title') Color @stop

@section('manual-content')
    <div class="row">
        <p class="lead">
            How to edit color values.
        </p>
        <p>
            In the sidebar the third tab is for editing color values of the chart.
        </p>
        <p>
            You can edit every color value for the different elements of the visualisation
            as well as some values used by every visualisation, like background and font colors.
            To edit a color value click on the colored square and You will be prompted with a color picker.
            After choosing a new color click the update button and the new color will be used.
            By clicking the reset button the default values are restored.
        </p>
        <p>
            The chart group is for background and font colors of serveral text elements,
            like the heading above the drawn visualisation,
            the text at the end or at the ticks of an axis
            and the text within a selectable element of a visualisation.
        </p>
        <p>
            The groups for the visualisations contain a selected and not selected color
            for every element of this visualisation.
        </p>
        <p>
            Any changes You make for the color settings will be stored in the local storage of Your browser and
            loaded with the next usage of this module.
        </p>
        @include('visualisation::manual.tutorials.list', ['name'=>'color'])
    </div>
@endsection
