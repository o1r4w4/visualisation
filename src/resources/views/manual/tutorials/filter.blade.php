@extends('manual.base')

@section('manual-title') Filter @stop

@section('manual-content')
    <div class="row">
        <p class="lead">
            How to select elements and filter images.
        </p>
        <p>
            Once a visualisation is displayed You can select elements of the visualisation by clicking on them.
            You can select the bars of the histogram, the circles in the scatter-plot
            and the points in parallel-coordinates, netmap- and radar-display.
        </p>
        <p class="text-center">
            <img src="{{asset('vendor/visualisation/images/manual/ss_vis_sel_1.png')}}"
                 width="75%">
        </p>
        <p>
            To enable the visualisation-filter for a image-view of a volume enter the image-view
            in another tab or browser, while this modules page is still open.
            In the filter-tab ob the image-view select the entry called "visualisation selection"
            from the drop down menu and add the rule.
        </p>
        <p class="text-center">
            <img src="{{asset('vendor/visualisation/images/manual/ss_volume_1.png')}}"
                 width="75%">
        </p>
        <p>
            After enabling the visualisation-filter any selection of elemets in a visualisation
            will filter the shown images in the image-view accordingly.
        </p>
        <p class="text-center">
            <img src="{{asset('vendor/visualisation/images/manual/ss_volume_filter.png')}}"
                 width="75%">
        </p>
        <ul>
            <li>
                <b>Histogram, Generalized drafter's plot</b>
                <p>
                    By clicking one of the bars or points
                    the corresponding images ids are added
                    or removed from the current selection.
                </p>
            </li>
            <li>
                <b>Parallel coordinates, Netmap display, Radar display</b>
                <p>
                    By clicking one of the points
                    the corresponding line ids are added or removed.
                    By default the lines are updated with a AND-logic,
                    which means that only the ids of those lines
                    connecting all selected points are added or removed,
                    except if some of those points are for the same label.
                    In that case an OR-logic is used for those points.
                </p>
            </li>
        </ul>
        @include('visualisation::manual.tutorials.list', ['name'=>'filter'])
    </div>
@endsection
