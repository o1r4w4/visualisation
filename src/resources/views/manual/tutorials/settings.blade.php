@extends('manual.base')

@section('manual-title') Settings @stop

@section('manual-content')
    <div class="row">
        <p class="lead">
            How to edit settings.
        </p>
        <p>
            In the sidebar the fourth tab is for editing settings.
        </p>
        <p>
            The chart group contains settings every visualisation uses.
            Most of this values are update on change,
            only the number values must be updated by clicking the update button.
        </p>
        <ul>
            <li>
                Show values:
                Enables drawing of a text within the elements of a visualisation with the value of the element.
            </li>
            <li>
                Show 0 values:
                Enables computing of 0 values for the visualisation,
                eg. if images, which don't have annotations with the selected label(s), should also be shown.
            </li>
            <li>
                AND selection:
                Enables a logical AND for selecting lines in parallel coordinates, netmap- and radar-diaplay.
                Otherwise a logical OR is used for all points.
            </li>
            <li>
                Anno/Area:
                Enables computing of annotations per image area, if area data from laser point detection is available.
                Otherwise an area of 0 is assumed.
            </li>
            <li>
                Grouping:
                Enables the grouping of values on an axis.
                X-axis for histogram, Y-axis for scatterplot,
                parallel coordinates and radar-display and circle-grouping for netmap-display.
            </li>
            <li>
                Radius range:
                Set the radius min and max value for the the circles and points in all
                visualisations accept histogram.
            </li>
        </ul>
        <p>
            The other groups contain settings specific for one visualisation.
        </p>
        <ul>
            <li>
                Y scale:
                Switch between linear and logarithmic display of the Y-axis for the histogram.
            </li>
            <li>
                Draw mode:
                Switch between linear and cardinal draw mode of the line in parallel coordinates.
            </li>
        </ul>
        <p>
            Any changes You make for the settings will be stored in the local storage of Your browser and
            loaded with the next usage of this module.
        </p>
        @include('visualisation::manual.tutorials.list', ['name'=>'settings'])
    </div>
@endsection
