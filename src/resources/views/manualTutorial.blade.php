<h3>Visualisation</h3>

<h4>
    <a href="{{route('manual-tutorials', ['visualisation', 'getting-started'])}}">Getting Started</a>
</h4>
<p>
    A quick introduction to the visualisation module.
</p>

<h4>
    <a href="{{route('manual-tutorials', ['visualisation', 'visualisations'])}}">Visualisations</a>
</h4>
<p>
    How to choose labels and visualisations.
</p>

<h4>
    <a href="{{route('manual-tutorials', ['visualisation', 'filter'])}}">Filter</a>
</h4>
<p>
    How to select elements and filter images.
</p>

<h4>
    <a href="{{route('manual-tutorials', ['visualisation', 'size'])}}">Size</a>
</h4>
<p>
    How to edit size values.
</p>

<h4>
    <a href="{{route('manual-tutorials', ['visualisation', 'size'])}}">Color</a>
</h4>
<p>
    How to edit color values.
</p>

<h4>
    <a href="{{route('manual-tutorials', ['visualisation', 'settings'])}}">Settings</a>
</h4>
<p>
    How to edit settings.
</p>

<h4>
    <a href="{{route('manual-tutorials', ['visualisation', 'download'])}}">Download</a>
</h4>
<p>
    How to download the chart.
</p>

<h4>
    <a href="{{route('manual-tutorials', ['visualisation', 'test'])}}">Usability test</a>
</h4>
<p>
    How to make a usability test.
</p>