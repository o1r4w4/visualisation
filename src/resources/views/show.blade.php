@extends('app')

@section('title', $volume->name)

@push('styles')
    <link href="{{ cachebust_asset('vendor/label-trees/styles/main.css') }}" rel="stylesheet">
    <link href="{{ cachebust_asset('vendor/visualisation/styles/main.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="{{ cachebust_asset('vendor/visualisation/scripts/d3.min.js') }}"></script>
    <script src="{{ cachebust_asset('vendor/visualisation/scripts/kolorwheel.min.js') }}"></script>
    <script src="{{ cachebust_asset('vendor/label-trees/scripts/main.js') }}"></script>
    <script src="{{ cachebust_asset('vendor/visualisation/scripts/main.js') }}"></script>
    <script type="text/javascript">
        biigle.$declare('visualisation.images', {!! $images !!});
        biigle.$declare('visualisation.volume', {!! $volume !!});
        biigle.$declare('visualisation.labelTrees', {!! $trees !!});
    </script>
@endpush

@section('content')
    <main id="visualisation-controller">
        <div class="visualisation-container">
            <visualisation-select v-on:update="handleSelectChanged"
                                  :model="selectModel"
                                  class="visualisation-main-select">
            </visualisation-select>
            <visualisation-chart :colors="colors" :settings="settings"
                                 v-on:update_selected="updateSelectedImages"
                                 v-on:size_updated="chartSizeUpdate"
                                 ref="visualisation_chart">
            </visualisation-chart>
        </div>
        <sidebar id="visualisation-sidebar" v-cloak
                 v-on:open="sideBarToggle"
                 v-on:close="sideBarToggle">
            <sidebar-tab name="labels" icon="tags" title="Labels">
                <label-trees :trees="labelTrees" :multiselect="true"
                             v-on:select="handleSelectedLabel"
                             v-on:deselect="handleDeselectedLabel"
                             v-on:clear="handleClearedLabels"></label-trees>
            </sidebar-tab>

            <sidebar-tab name="size" icon="resize-full" title="Size">
                <visualisation-value-edit-group v-for="size in sizeModels"
                                                :model="size"
                                                v-on:update="handleSettingsChange"
                                                v-on:reset="handleResetSettings">
                </visualisation-value-edit-group>
            </sidebar-tab>

            <sidebar-tab name="colors" icon="pencil" title="Color">
                <visualisation-value-edit-group v-for="color in colorModels"
                                                :model="color"
                                                v-on:update="handleColorChange">

                </visualisation-value-edit-group>
            </sidebar-tab>

            <sidebar-tab name="settings" icon="cog" title="Settings">

                <visualisation-value-edit-group v-for="setting in settingsModels"
                                                :model="setting"
                                                v-on:update="handleSettingsChange">
                </visualisation-value-edit-group>
            </sidebar-tab>

            <sidebar-tab name="download" icon="download-alt" title="Download">
                <h4>Download</h4>
                <visualisation-value-edit :model="exportModel">
                </visualisation-value-edit>
                <button type="submit" class="btn btn-default"
                        v-on:click="handleExport('svg')" title="Export visualisation as svg">
                    <span class="glyphicon glyphicon-save" aria-hidden="true"></span>
                    SVG
                </button>
                <button type="submit" class="btn btn-default"
                        v-on:click="handleExport('png')" title="Export visualisation as png">
                    <span class="glyphicon glyphicon-save" aria-hidden="true"></span>
                    PNG
                </button>
                <canvas id="visualisation-canvas"></canvas>
            </sidebar-tab>
        </sidebar>
    </main>
@endsection

@section('navbar')
    <div id="visualisation-navbar" class="navbar-text navbar-volumes-breadcrumbs">
        @include('volumes::partials.projectsBreadcrumb', ['projects' => $volume->projects]) /
        <a href="{{route('volume', $volume->id)}}" target="_blank">{{$volume->name}}</a> /
        <strong>Visualisation</strong> @include('volumes::partials.annotationSessionIndicator')
        <span v-if="loading" class="loader loader--active"></span>
        <span v-else v-cloak>(<span v-text="number"></span> images)</span>
    </div>
@endsection
