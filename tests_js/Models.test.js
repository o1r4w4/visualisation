var tests = require('./tests');
var Constants = require('../src/resources/assets/js/visualisation/Constants');
var Models = require('../src/resources/assets/js/visualisation/classes/Models');
var CHART = Constants.CHART;
var GROUP = Constants.GROUP;
var CHECK = Constants.CHECK;
var COLOR = Constants.COLOR;
var RADIO = Constants.RADIO;
var NUMBER = Constants.NUMBER;

describe('Model tests', function(){

    describe('model = new VisualisationModel("testName")', function(){
        var name = 'testName';
        var model;
        beforeEach(function() {
            model = new Models.VisualisationModel(name);
        });

        tests.checkObject(function(){return model;},
            'model');
        tests.testString(function(){return model;},
            'model', 'name',
            function(){return name;}, name);
        tests.testBool(function(){return model;},
            'model', 'show', true);

    });

    describe('model = new VisualisationGroupModel("testName", "testText", true)', function() {
        var name = 'testName';
        var text = 'testText';
        var model;
        beforeEach(function() {
            model = new Models.VisualisationGroupModel(name, text, true);
        });

        tests.checkObject(function(){return model;},
            'model');
        tests.testObject(function(){return model;},
            'model', 'models');
        tests.testString(function(){return model;},
            'model', 'text',
            function(){return text;}, text);
        tests.testObject(function(){return model;},
            'model', 'titles');
        tests.testBool(function(){return model;},
            'model', 'default', true);
        tests.testString(function(){return model;},
            'model', 'type',
            function(){return GROUP;}, GROUP);

        tests.testFunc(function(){return model;},
            'model', 'add');
        tests.testFunc(function(){return model;},
            'model', 'get');
        tests.testFunc(function(){return model;},
            'model', 'clear');
        tests.testFunc(function(){return model;},
            'model', 'setTitles');


        describe('model.setTitles("update", "reset"))', function(){
            var update = "update";
            var reset = "reset";
            beforeEach(function() {
                model.setTitles(update, reset);
            });
            tests.testString(function(){return model.titles;},
                'titles', 'update',
                function(){return update;}, update);
            tests.testString(function(){return model.titles;},
                'titles', 'reset',
                function(){return reset;}, reset);
        });
        describe('subModel = new VisualisationModel("subTestName")', function(){
            var subName = 'subTestName';
            var subModel;
            beforeEach(function() {
                subModel = new Models.VisualisationModel(subName);
                model.add(subModel);
            });
            describe('model.add(subModel)', function(){
                tests.testObject(function(){return model.models;},
                    'model.models', subName);
            });
            describe('model.get(subModel.name)', function(){
                tests.checkObject(function(){return subModel;},
                    'subModel', function(){return model.get(subModel.name);},
                    'model.get(subModel.name)');
            });
            describe('model.clear()', function(){
                beforeEach(function() {
                    model.clear();
                });
                tests.checkObject(function(){return model.models;},
                    'model.models', null, null, true);
            });
        });
    });

    describe('model = new VisualisationValueModel("testName", 0, NUMBER, "testText", ' +
        '"testTitle", "testClass")',function(){
        var name = 'testName';
        var text = 'testText';
        var title = 'testTitle';
        var clazz = 'testClass';
        var model;
        beforeEach(function() {
            model = new Models.VisualisationValueModel(name, 0, NUMBER, text, title, clazz);
        });

        tests.checkObject(function(){return model;},
            'model');

        tests.testNumber(function(){return model;},
            'model', 'value',
            function(){return 0;}, 0);
        tests.testString(function(){return model;},
            'model', 'type',
            function(){return NUMBER;}, NUMBER);
        tests.testString(function(){return model;},
            'model', 'text',
            function(){return text;}, text);
        tests.testString(function(){return model;},
            'model', 'title',
            function(){return title;}, title);
        tests.testString(function(){return model;},
            'model', 'visualisation',
            function(){return CHART;}, CHART);
        tests.testString(function(){return model;},
            'model', 'class',
            function(){return clazz;}, clazz);

    });

    describe('model = new VisualisationRadioModel("testName", 0, "testText", ' +
        '"testTitle")', function(){
        var name = 'testName';
        var text = 'testText';
        var title = 'testTitle';
        var model;
        beforeEach(function() {
            model = new Models.VisualisationRadioModel(name, 0, text, title);
        });

        tests.checkObject(function(){return model;},
            'model');

        tests.testString(function(){return model;},
            'model', 'type',
            function(){return RADIO;}, RADIO, false, true);
        tests.testString(function(){return model;},
            'model', 'class',
            function(){return 'vis_radio_input';}, 'vis_radio_input');
        tests.testArrayLength(function(){return model;},
            'model', 'radios', 0);
        tests.testFunc(function(){return model;},
            'model', 'addRadio');

        describe('model.addRadio(0, "radText0", "radTitle0")', function(){
            var radText0 = 'radText0';
            var radTitle0 = 'radTitle0';
            beforeEach(function() {
                model.addRadio(0, radText0, radTitle0);
            });
            tests.testArrayLength(function(){return model;},
                'model', 'radios', 1, true);
            tests.checkObject(function(){return model.radios[0];},
                'model.radios[0]');
            tests.testNumber(function(){return model.radios[0];},
                'radios[0]', 'value',
                function(){return 0;}, 0);
            tests.testString(function(){return model.radios[0];},
                'radios[0]', 'text',
                function(){return radText0;}, radText0);
            tests.testString(function(){return model.radios[0];},
                'radios[0]', 'title',
                function(){return radTitle0;}, radTitle0);
        });

        describe('model.addRadio(1, "radText1", "radTitle1")', function(){
            var radText0 = 'radText0';
            var radTitle0 = 'radTitle0';
            var radText1 = 'radText1';
            var radTitle1 = 'radTitle1';
            beforeEach(function() {
                model.addRadio(0, radText0, radTitle0);
                model.addRadio(1, radText1, radTitle1);
            });
            tests.testArrayLength(function(){return model;},
                'model', 'radios', 2, true);
            tests.checkObject(function(){return model.radios[1];},
                'model.radios[1]');
            tests.testNumber(function(){return model.radios[1];},
                'radios[1]', 'value',
                function(){return 1;}, 1);
            tests.testString(function(){return model.radios[1];},
                'radios[1]', 'text',
                function(){return radText1;}, radText1);
            tests.testString(function(){return model.radios[1];},
                'radios[1]', 'title',
                function(){return radTitle1;}, radTitle1);
        });
    });

    describe('model = new VisualisationTestDataModel("testName","testDate")', function(){
        var model;
        var name = 'testName';
        var date = 'testDate';
        beforeEach(function() {
            model = new Models.VisualisationTestDataModel(name, date);
        });
        tests.checkObject(function(){return model;},
            'model');

        tests.testString(function(){return model;},
            'model', 'date', function(){return date;}, date);

        tests.testObject(function(){return model;},
            'model', 'first');
        tests.testNumber(function(){return model.first;},
            'first', 'right', function(){return 0;}, 0);
        tests.testNumber(function(){return model.first;},
            'first', 'wrong', function(){return 0;}, 0);
        tests.testArrayLength(function(){return model.first;},
            'first', 'results', 0);

        tests.testObject(function(){return model;},
            'model', 'second');
        tests.testNumber(function(){return model.second;},
            'second', 'right', function(){return 0;}, 0);
        tests.testNumber(function(){return model.second;},
            'second', 'wrong', function(){return 0;}, 0);
        tests.testArrayLength(function(){return model.second;},
            'second', 'results', 0);

        tests.testObject(function(){return model;},
            'model', 'evaluation');
        tests.testArrayLength(function(){return model.evaluation;},
            'evaluation', 'results', 0);

        tests.testObject(function(){return model;},
            'model', 'opinion');
        tests.testArrayLength(function(){return model.evaluation;},
            'evaluation', 'results', 0);

    });

    describe('model = new VisualisationTestModel()', function(){
        var model;
        beforeEach(function() {
            model = new Models.VisualisationTestModel();
        });
        tests.checkObject(function(){return model;},
            'model');

        tests.testArrayLength(function(){return model;},
            'model', 'questions', 0);
        tests.testNumber(function(){return model;},
            'model', 'name', function(){return 2;}, 2);
    });

    describe('model = new VisualisationQuestModel("testQuest", [0])', function(){
        var quest = 'testQuest';
        var answer = [0];
        var model;
        beforeEach(function() {
            model = new Models.VisualisationQuestModel(quest, answer);
        });
        tests.checkObject(function(){return model;},
            'model');

        tests.testString(function(){return model;},
            'titles', 'question',
            function(){return quest;}, quest);
        tests.testArray(function(){return model;},
            'model', 'answer',
            function(){return answer;}, answer);
    });


    describe('model = new VisualisationBlockModel()', function(){
        var model;
        beforeEach(function() {
            model = new Models.VisualisationBlockModel();
        });
        tests.checkObject(function(){return model;},
            'model');

        tests.testNumber(function(){return model;},
            'model', 'count',
            function(){return 0;}, 0);
        tests.testArrayLength(function(){return model;},
            'model', 'ids', 0);
    });
    describe('model = new VisualisationBlockModel(1, 2)', function(){
        var model;
        beforeEach(function() {
            model = new Models.VisualisationBlockModel(1, 2);
        });
        tests.checkObject(function(){return model;},
            'model');

        tests.testNumber(function(){return model;},
            'model', 'count',
            function(){return 1;}, 1);
        tests.testArrayLength(function(){return model;},
            'model', 'ids', 1);
        tests.checkNumber(function(){return model.ids[0];},
            'ids[0]', 2);
    });
    describe('model = new VisualisationPointModel(1, 2, 3, 4)', function(){
        var model;
        beforeEach(function() {
            model = new Models.VisualisationPointModel(1, 2, 3, 4);
        });
        tests.checkObject(function(){return model;},
            'model');

        tests.checkNumber(function(){return model.count;},
            'model.count', 3);
        tests.checkNumber(function(){return model.ids[0];},
            'ids[0]', 4);


        tests.testNumber(function(){return model;},
            'model', 'label',
            function(){return 1;}, 1);
        tests.testNumber(function(){return model;},
            'model', 'index',
            function(){return 2;}, 2);
    });
    describe('model = new VisualisationLinePointModel(1, 2, 3, 4)', function(){
        var model;
        beforeEach(function() {
            model = new Models.VisualisationLinePointModel(1, 2, 3, 4);
        });
        tests.checkObject(function(){return model;},
            'model');

        tests.testArrayLength(function(){return model;},
            'model', 'lines', 0);
    });
    describe('model = new VisualisationLineModel(1)', function(){
        var model;
        beforeEach(function() {
            model = new Models.VisualisationLineModel(1);
        });
        tests.checkObject(function(){return model;},
            'model');

        tests.testNumber(function(){return model;},
            'model', 'id',
            function(){return 1;}, 1);
        tests.testArrayLength(function(){return model;},
            'model', 'points', 0);
    });
    describe('model = new VisualisationBlockDataModel("testName")', function(){
        var model;
        var name = 'testName';
        beforeEach(function() {
            model = new Models.VisualisationBlockDataModel(name);
        });
        tests.checkObject(function(){return model;},
            'model');

        tests.testString(function(){return model;},
            'model', 'labelName',
            function(){return name;}, name);

        tests.testArrayLength(function(){return model;},
            'model', 'data', 0);
        tests.testArrayLength(function(){return model;},
            'model', 'xDomain', 0);
        tests.testArrayLength(function(){return model;},
            'model', 'yDomain', 2);
        tests.checkNumber(function(){return model.yDomain[0];},
            'yDomain[0]', 0);
        tests.checkNumber(function(){return model.yDomain[1];},
            'yDomain[1]', 0);
    });
    describe('model = new VisualisationScatterDataModel()', function(){
        var model;
        beforeEach(function() {
            model = new Models.VisualisationScatterDataModel();
        });
        tests.checkObject(function(){return model;},
            'model');

        tests.testArrayLength(function(){return model;},
            'model', 'labelNames', 0);
        tests.testArrayLength(function(){return model;},
            'model', 'data', 0);
        tests.testArrayLength(function(){return model;},
            'model', 'xDomain', 0);
        tests.testArrayLength(function(){return model;},
            'model', 'yDomain', 0);
        tests.testArrayLength(function(){return model;},
            'model', 'zDomain', 0);
    });
    describe('model = new VisualisationLineDataModel()', function(){
        var model;
        beforeEach(function() {
            model = new Models.VisualisationLineDataModel();
        });
        tests.checkObject(function(){return model;},
            'model');

        tests.testArrayLength(function(){return model;},
            'model', 'labelNames', 0);
        tests.testArrayLength(function(){return model;},
            'model', 'lineData', 0);
        tests.testArrayLength(function(){return model;},
            'model', 'pointData', 0);
        tests.testArrayLength(function(){return model;},
            'model', 'xDomain', 0);
        tests.testArrayLength(function(){return model;},
            'model', 'yDomain', 0);
        tests.testArrayLength(function(){return model;},
            'model', 'zDomain', 0);
    });
});