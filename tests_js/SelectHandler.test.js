var tests = require('./tests');
var Constants = require('../src/resources/assets/js/visualisation/Constants');
var Handler =
    require('../src/resources/assets/js/visualisation/classes/SelectHandler');
var VisualisationHandler = Handler.VisualisationSelectHandler;
var VisualisationSubHandler = Handler.VisualisationSelectSubHandler;
var VIS_BLANK = Constants.VIS_BLANK;

describe('VisualisationSelectSubHandler tests', function(){
    var handler;
    var controller = {
        showVisualisation: function () {

        }
    };
    describe('visArray = []', function() {
        describe('handler = new VisualisationSelectSubHandler(visArray)', function() {
            var visArray = [];
            beforeEach(function() {
                handler = new VisualisationSubHandler(visArray);
            });
            tests.checkObject(function () {return handler;},
                'handler');
            tests.testArray(function(){return handler;},
                'handler', 'visualisations',
                function(){return visArray;},visArray);
            tests.testString(function(){return handler;},
                'handler', 'curVisName',
                function(){return VIS_BLANK;},VIS_BLANK);
            // tests.testObject(function(){return handler;},
            //     'handler', 'curVis');

            tests.testFunc(function(){return handler;},
                'handler', 'parseVisualisation');
            tests.testFunc(function(){return handler;},
                'handler', 'updateVisualisation');
            tests.testFunc(function(){return handler;},
                'handler', 'getCurVisName');
            tests.testFunc(function(){return handler;},
                'handler', 'getVis');
            tests.testFunc(function(){return handler;},
                'handler', 'getCurVis');
        });
    });

    describe('visArray = [{name:"testName",text:"testText"}]', function() {
        describe('handler = new VisualisationSelectSubHandler(visArray)', function() {
            var name = 'testName';
            var text = 'testText';
            var visArray = [{name:name,text:text}];
            beforeEach(function() {
                handler = new VisualisationSubHandler(visArray);
            });
            tests.checkObject(function () {return handler;},
                'handler');
            tests.testArray(function(){return handler;},
                'handler', 'visualisations',
                function(){return visArray;},'visArray', true, true);
            tests.testObject(function(){return handler;},
                'handler', 'curVis',
                function(){return visArray[0];},'visArray[0]');
            tests.testString(function(){return handler;},
                'handler', 'curVisName',
                function(){return name;},name, false, true);

            tests.checkString(function(){return handler.getCurVisName()},
                'handler.getCurVisName()',
                function(){return name;},name, false, true);
            tests.checkObject(function(){return handler.getCurVis()},
                'handler.getCurVis()',
                function(){return visArray[0];},'visArray[0]');
            // tests.testFunc(function(){return handler;},
            //     'handler', 'getCurVisName',
            //     function(){return name;},name);
        });
    });

    describe(
        'visArray = [{name:"testName1",text:"testText1"},' +
        '{name:"testName2",text:"testText2"}]',
        function() {
            describe('handler = new VisualisationSelectSubHandler(visArray)',
                function() {
                    var name = 'testName1';
                    var text = 'testText1';
                    var name1 = 'testName2';
                    var text1 = 'testText2';
                    var visArray = [{name:name,text:text},{name:name1,text:text1}];
                    beforeEach(function() {
                        handler = new VisualisationSubHandler(visArray);
                    });

                    tests.testArray(function(){return handler;},
                        'handler', 'visualisations',
                        function(){return visArray;},
                        'visArray',
                        true, true);

                    describe('handler.parseVisualisation(true, null, controller)',
                        function() {
                            beforeEach(function() {
                                handler.parseVisualisation(true, null, controller);
                            });
                            tests.testString(function(){return handler;},
                                'handler', 'curVisName',
                                function(){return name;},name, false, true);
                        });
                    describe('handler.parseVisualisation(true, "testName2", controller)',
                        function() {
                            beforeEach(function() {
                                handler.parseVisualisation(true, name1, controller);
                            });
                            tests.testString(function(){return handler;},
                                'handler', 'curVisName',
                                function(){return name1;},name1, false, true);
                            tests.testObject(function(){return handler;},
                                'handler', 'curVis',
                                function(){return visArray[1];},'visArray[1]', true);
                        });
                    describe('handler.updateVisualisation(null, controller)',
                        function() {
                            beforeEach(function() {
                                handler.updateVisualisation(null, controller);
                            });
                            tests.testString(function(){return handler;},
                                'handler', 'curVisName',
                                function(){return name;},name, false, true);
                        });
                    describe('handler.updateVisualisation(visArray[1], controller)',
                        function() {
                            beforeEach(function() {
                                handler.updateVisualisation(visArray[1], controller);
                            });
                            tests.testString(function(){return handler;},
                                'handler', 'curVisName',
                                function(){return name1;},name1, false, true);
                            tests.testObject(function(){return handler;},
                                'handler', 'curVis',
                                function(){return visArray[1];},'visArray[1]', true);
                        });
                });
        });
});

describe('VisualisationSelectHandler tests', function() {
    var handler;
    var controller = {
        showVisualisation: function () {

        }
    };
    describe('handler = new VisualisationSelectHandler()', function () {
        beforeEach(function () {
            handler = new VisualisationHandler();
        });
        tests.checkObject(function () {return handler;},
            'handler');

        tests.testObject(function () {return handler;},
            'handler', 'blankHandler');
        tests.testObject(function () {return handler;},
            'handler', 'singleHandler');
        tests.testObject(function () {return handler;},
            'handler', 'dualHandler');
        tests.testObject(function () {return handler;},
            'handler', 'multiHandler');
        tests.testObject(function () {return handler;},
            'handler', 'currentHandler',
            function () {return handler.blankHandler;},'blankHandler');


        tests.testFunc(function(){return handler;},
            'handler', 'updateModels');
        tests.testFunc(function(){return handler;},
            'handler', 'getCurrent');
        tests.testFunc(function(){return handler;},
            'handler', 'checkVisualisationName');

        tests.checkObject(function () {return handler.getCurrent();},
            'handler.getCurrent()',
            function () {return handler.blankHandler;},'blankHandler');

        tests.checkBool(function () {return handler.checkVisualisationName('chart');},
            'handler.checkVisualisationName("chart")', true);
        tests.checkBool(function () {return handler.checkVisualisationName('block');},
            'handler.checkVisualisationName("block")', false);

        describe('handler.updateModels(1, controller)', function () {
            beforeEach(function () {
                handler.updateModels(1, controller);
            });
            tests.checkObject(function () {return handler.getCurrent();},
                'handler.getCurrent()',
                function () {return handler.singleHandler;},'singleHandler');

            tests.checkBool(function () {return handler.checkVisualisationName('block');},
                'handler.checkVisualisationName("block")', true);
        });
        describe('handler.updateModels(2, controller)', function () {
            beforeEach(function () {
                handler.updateModels(2, controller);
            });
            tests.checkObject(function () {return handler.getCurrent();},
                'handler.getCurrent()',
                function () {return handler.dualHandler;},'dualHandler');

            tests.checkBool(function () {return handler.checkVisualisationName('scatter');},
                'handler.checkVisualisationName("scatter")', true);
        });
        describe('handler.updateModels(3, controller)', function () {
            beforeEach(function () {
                handler.updateModels(3, controller);
            });
            tests.checkObject(function () {return handler.getCurrent();},
                'handler.getCurrent()',
                function () {return handler.multiHandler;},'multiHandler');

            tests.checkBool(function () {return handler.checkVisualisationName('line');},
                'handler.checkVisualisationName("line")', true);

            describe('handler.getCurrent().updateVisualisation(' +
                'handler.getCurrent().visualisations[1], controller)', function () {

                beforeEach(function () {
                    handler.getCurrent().updateVisualisation(
                        handler.getCurrent().visualisations[1], controller);
                });

                tests.checkBool(function () {return handler.checkVisualisationName('net');},
                    'handler.checkVisualisationName("net")', true);


                describe('handler.updateModels(2, controller)', function () {
                    beforeEach(function () {
                        handler.updateModels(2, controller);
                    });
                    tests.checkObject(function () {return handler.getCurrent();},
                        'handler.getCurrent()',
                        function () {return handler.dualHandler;},'dualHandler');

                    tests.checkBool(function () {return handler.checkVisualisationName('net');},
                        'handler.checkVisualisationName("net")', true);
                });
            });
        });
    });
});