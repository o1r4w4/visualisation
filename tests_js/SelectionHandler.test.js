var tests = require('./tests');
var Constants = require('../src/resources/assets/js/visualisation/Constants');
var SETT_AND_SEL = Constants.SETT_AND_SEL;
var VisualisationSelectionHandler =
    require('../src/resources/assets/js/visualisation/classes/SelectionHandler');

describe('VisualisationSelectionHandler tests', function(){
    var handler;
    var controller = {
        settings:{

        },
        renderer:{
            updateLine: function () {

            }
        },
        emitSelected: function () {

        }
    };

    describe('handler = new VisualisationSelectionHandler(controller)', function() {
        // var visArray = [];
        beforeEach(function() {
            controller.settings[SETT_AND_SEL] = false;
            handler = new VisualisationSelectionHandler(controller);
        });
        tests.checkObject(function () {return handler;},
            'handler');
        tests.testArrayLength(function(){return handler;},
            'handler', 'selected', 0);
        tests.testArrayLength(function(){return handler;},
            'handler', 'selectedElements', 0);

        tests.testFunc(function(){return handler;},
            'handler', 'clickBar');
        tests.testFunc(function(){return handler;},
            'handler', 'clickScatter');
        tests.testFunc(function(){return handler;},
            'handler', 'clickLine');
        tests.testFunc(function(){return handler;},
            'handler', 'clickNet');
        tests.testFunc(function(){return handler;},
            'handler', 'clickRad');
        tests.testFunc(function(){return handler;},
            'handler', 'updateSelected');
        tests.testFunc(function(){return handler;},
            'handler', 'updateMultiSelect');
        tests.testFunc(function(){return handler;},
            'handler', 'updateMultiAndSelect');

        describe('handler.updateSelected([0, 1], true)', function() {

            beforeEach(function() {
                handler.updateSelected([0, 1], true);
            });
            tests.testArray(function(){return handler;},
                'handler', 'selected',
                function(){return [0, 1];},[0, 1],
                false, true);


            describe('handler.updateSelected([2], true)', function() {

                beforeEach(function() {
                    handler.updateSelected([2], true);
                });
                tests.testArray(function(){return handler;},
                    'handler', 'selected',
                    function(){return [0, 1, 2];},[0, 1, 2],
                    false, true);


                describe('handler.updateSelected([1], false)', function() {

                    beforeEach(function() {
                        handler.updateSelected([1], false);
                    });
                    tests.testArray(function(){return handler;},
                        'handler', 'selected',
                        function(){return [0, 2];},[0, 2],
                        false, true);
                });
            });

            describe('handler.updateSelected([3, 4], true, true)', function() {

                beforeEach(function() {
                    handler.updateSelected([3, 4], true, true);
                });
                tests.testArray(function(){return handler;},
                    'handler', 'selected',
                    function(){return [3, 4];},[3, 4],
                    false, true);
            });
        });

        describe('handler.updateMultiSelect(point0, true) without AND select', function() {
            var l0, l1, l2, point0;
            beforeEach(function() {
                l0 = {id:0,pointCounter:0};
                l1 = {id:1,pointCounter:0};
                l2 = {id:2,pointCounter:0};
                point0 = {
                    label: 0,
                    lines: [l0, l1, l2]
                };
                handler.updateMultiSelect(point0, true);
            });

            tests.testArray(function(){return handler;},
                'handler', 'selected',
                function(){return [0, 1, 2];},[0, 1, 2],
                false, true);

            describe('handler.updateMultiSelect(point1, true) without AND select', function() {
                var l3, l4, point1;
                beforeEach(function() {
                    l3 = {id:3,pointCounter:0};
                    l4 = {id:4,pointCounter:0};
                    point1 = {
                        label: 1,
                        lines: [l1, l3, l4]
                    };
                    handler.updateMultiSelect(point1, true);
                });

                tests.testArray(function(){return handler;},
                    'handler', 'selected',
                    function(){return [0, 1, 2, 3, 4];},[0, 1, 2, 3, 4],
                    false, true);

                describe('handler.updateMultiSelect(point0, false) without AND select', function() {
                    beforeEach(function() {
                        handler.updateMultiSelect(point0, false);
                    });

                    tests.testArray(function(){return handler;},
                        'handler', 'selected',
                        function(){return [1, 3, 4];},[1, 3, 4], false, true);

                    describe('handler.updateMultiSelect(point1, false) without AND select', function() {
                        beforeEach(function() {
                            handler.updateMultiSelect(point1, false);
                        });

                        tests.testArrayLength(function(){return handler;},
                            'handler', 'selected',0, true);
                    });
                });
            });
        });

        describe('handler.updateMultiSelect(point0, true);handler.updateMultiAndSelect(data)', function() {
            var l0, l1, l2, l3, l4, point0, point1, point2, data;
            beforeEach(function() {
                controller.settings[SETT_AND_SEL] = true;
                l0 = {id:0,pointCounter:0};
                l1 = {id:1,pointCounter:0};
                l2 = {id:2,pointCounter:0};
                l3 = {id:3,pointCounter:0};
                l4 = {id:4,pointCounter:0};
                point0 = {
                    label: 0,
                    lines: [l0, l1, l2]
                };
                point1 = {
                    label: 1,
                    lines: [l0, l3]
                };
                point2 = {
                    label: 0,
                    lines: [l4]
                };
                data = {
                    lineData: [
                        {line:l0},
                        {line:l1},
                        {line:l2},
                        {line:l3},
                        {line:l4},
                    ]
                };
                handler.updateMultiSelect(point0, true);
                handler.updateMultiAndSelect(data);
            });

            tests.testArray(function(){return handler;},
                'handler', 'selected',
                function(){return [0, 1, 2];},[0, 1, 2],
                false, true);

            tests.testArray(function(){return handler;},
                'handler', 'selectedElements',
                function(){return [point0];},['point0'],
                false, true);


            describe('handler.updateMultiSelect(point1, true);handler.updateMultiAndSelect(data)', function() {

                beforeEach(function() {
                    handler.updateMultiSelect(point1, true);
                    handler.updateMultiAndSelect(data);
                });

                tests.testArray(function(){return handler;},
                    'handler', 'selected',
                    function(){return [0];},[0],
                    false, true);

                tests.testArray(function(){return handler;},
                    'handler', 'selectedElements',
                    function(){return [point0,point1];},['point0','point1'],
                    false, true);


                describe('handler.updateMultiSelect(point2, true);handler.updateMultiAndSelect(data)', function() {

                    beforeEach(function() {
                        handler.updateMultiSelect(point2, true);
                        handler.updateMultiAndSelect(data);
                    });

                    tests.testArray(function(){return handler;},
                        'handler', 'selected',
                        function(){return [0];},[0],
                        false, true);

                    tests.testArray(function(){return handler;},
                        'handler', 'selectedElements',
                        function(){return [point0,point1];},['point0','point1'],
                        false, true);


                    describe('handler.updateMultiSelect(point0, false);handler.updateMultiAndSelect(data)', function() {

                        beforeEach(function() {
                            handler.updateMultiSelect(point0, false);
                            handler.updateMultiAndSelect(data);
                        });

                        tests.testArray(function(){return handler;},
                            'handler', 'selected',
                            function(){return [0, 3, 4];},[0, 3, 4],
                            false, true);

                        tests.testArray(function(){return handler;},
                            'handler', 'selectedElements',
                            function(){return [point1];},['point1'],
                            false, true);


                        describe('handler.updateMultiSelect(point1, false);handler.updateMultiAndSelect(data)', function() {

                            beforeEach(function() {
                                handler.updateMultiSelect(point1, false);
                                handler.updateMultiAndSelect(data);
                            });

                            tests.testArray(function(){return handler;},
                                'handler', 'selected',
                                function(){return [4];},[4],
                                false, true);

                            tests.testArrayLength(function(){return handler;},
                                'handler', 'selectedElements',0, true);


                            describe('handler.updateMultiSelect(point2, false);handler.updateMultiAndSelect(data)', function() {

                                beforeEach(function() {
                                    handler.updateMultiSelect(point2, false);
                                    handler.updateMultiAndSelect(data);
                                });

                                tests.testArrayLength(function(){return handler;},
                                    'handler', 'selected',0, true);

                                tests.testArrayLength(function(){return handler;},
                                    'handler', 'selectedElements',0, true);

                            });

                        });

                    });

                });

            });
        });
    });

});