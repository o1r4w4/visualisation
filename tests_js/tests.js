var test = require('unit.js');

var testBool = function (owner, ownerName, name, equal, short) {
    var text = ownerName+' should have a boolean equal to '+equal +
        ' as "'+name+'" property';
    if(short)
        text = ownerName+'.'+name+' should be equal to '+equal;
    it(text, function(){
        if(!short)test.object(owner()).hasProperty(name);
        if(equal)test.bool(owner()[name]).isTrue();
        else test.bool(owner()[name]).isFalse();
    });
};
var testString = function (owner, ownerName, name, equal, equalName, noQuotes, short) {
    var text = ownerName+' should have a string equal to "'+equalName+'"' +
        ' as "'+name+'" property';
    if(noQuotes)text = ownerName+' should have a string equal to '+equalName+'' +
        ' as "'+name+'" property';
    if(short){
        text = ownerName+'.'+name+' should be equal to "'+equalName+'"';
        if(noQuotes)text = ownerName+'.'+name+' should be equal to '+equalName;
    }
    it(text,
        function(){
            test.object(owner()).hasProperty(name);
            test.string(owner()[name]).is(equal());
        }
    );
};
var testNumber = function (owner, ownerName, name, equal, equalName, short) {
    var text = ownerName+' should have "'+name+'" property,' +
        ' a number';
    if(equal!=null)text += ' equal to '+equalName;
    if(short)text = ownerName+'.'+name+' should be equal to '+equalName;

    it(text, function(){
        if(!short)test.object(owner()).hasProperty(name);
        if(equal!=null)test.number(owner()[name]).is(equal());
        else test.number(owner()[name]);
    });
};
var testArray = function (owner, ownerName, name, equal, equalName, noBracket, short) {
    var text = ownerName+' should have an array equal to ['+equalName+']' +
        ' as "'+name+'" property';
    if(noBracket)text = ownerName+' should have an array equal to '+equalName+
        ' as "'+name+'" property';
    if(short){
        text = ownerName+'.'+name+' should be equal to ['+equalName+']';
        if(noBracket)text = ownerName+'.'+name+' should be equal to '+equalName;
    }
    it(text, function(){
        test.object(owner()).hasProperty(name);
        test.array(owner()[name]).is(equal());
    });
};
var testArrayLength = function (owner, ownerName, name, length, short) {
    var text = ownerName+' should have an array with length '+length +
        ' as "'+name+'" property';
    // var text = ownerName+' should have "'+name+'" property,' +
    //     ' an array with length '+length;
    if(short)text = ownerName+'.'+name+' should have length '+length;
    it(text, function(){
        test.object(owner()).hasProperty(name);
        test.array(owner()[name]).hasLength(length);
    });
};
var checkObject = function (object, name, equal, equalName, empty) {
    var text = name+' should be an object';
    if(equal !=null)text = name+' should be an object equal to '+equalName;
    else if(empty)text = name+' should be an empty object';
    it(text, function(){
        if(equal !=null)test.object(object()).is(equal());
        else {
            if(empty)test.object(object()).isEmpty();
            else test.object(object());
        }
    });
};
var checkNumber = function (func, name, equal) {
    var text = name+' should be a number';
    if(equal !=null)text = name+' should be a number equal to '+equal;
    it(text, function(){
        if(equal !=null){
            test.number(func()).is(equal);
        }
        else  test.number(func());
    });
};
var checkArrayLength = function (func, name, equal) {
    var text = name+' should be an array';
    if(equal !=null)text = name+' should be an array with length '+equal;
    it(text, function(){
        if(equal !=null){
            test.array(func()).hasLength(equal);
        }
        else  test.array(func());
    });
};
var checkString = function (func, name, equal, equalName) {
    var text = name+' should be a string';
    if(equal !=null)text = name+' should be a string equal to "'+equalName+'"';
    it(text, function(){
        if(equal !=null)test.string(func()).is(equal());
        else  test.string(func());
    });
};

var checkBool = function (func, name, equal) {
    var text = name+' should be a boolean';
    if(equal !=null)text = name+' should be a boolean equal to '+equal;
    it(text, function(){
        if(equal !=null){
            if(equal)test.bool(func()).isTrue();
            else test.bool(func()).isFalse();
        }
        else  test.bool(func());
    });
};
var testObject = function (owner, ownerName, name, equal, equalName, short, empty) {
    var text = ownerName+' should have an object as "'+name+'" property';
    if(empty)text = ownerName+' should have an empty object as "'+name+'" property';
    if(equal!=null)text = ownerName+' should have an object equal to '+equalName+
        ' as "'+name+'" property';
    if(short){
        if(equal!=null)text = ownerName+'.'+name+' should be equal to '+equalName;
        else text = ownerName+'.'+name+' should be empty';
    }
    it(text, function(){
        test.object(owner()).hasProperty(name);
        if(equal==null){
            if(short || empty)test.object(owner()[name]).isEmpty();
            else test.object(owner()[name]);
        }
        else test.object(owner()[name]).is(equal());
    });
};
var testFunc = function (owner, ownerName, name, equal, equalName) {
    var text = ownerName+' should have a function as "'+name+'" property,';
    // if(equal!=null)text = ownerName+'.'+name+' should return '+equalName;
    it(text, function(){
        test.object(owner()).hasProperty(name);
        // if(equal==null)
        test.function(owner()[name]);
        // else {
        //
        // }
    });
};

module.exports = {
    testBool: testBool,
    testNumber: testNumber,
    testString: testString,
    testArray: testArray,
    testArrayLength: testArrayLength,
    checkObject: checkObject,
    testObject: testObject,
    testFunc: testFunc,
    checkString:checkString,
    checkBool: checkBool,
    checkNumber: checkNumber,
    checkArrayLength: checkArrayLength,
};