var tests = require('./tests');
var DataHandler = require('../src/resources/assets/js/visualisation/classes/DataHandler');


describe('DataHandler tests', function(){
    var allImages = [
        {id:0},{id:1},{id:2},{id:3},{id:4},{id:5},
    ];

    // before(function() {
    //     console.log('allImages:');
    //     console.log(allImages);
    // });
    var handler;
    var data;
    beforeEach(function() {
        handler = new DataHandler(allImages);
    });

    describe('handler = new DataHandler(allImages)', function(){

        tests.checkObject(function(){return handler;},
            'handler');

        tests.testObject(function(){return handler;},
            'handler', 'imagesWithLabelCache', null, null, false, true);

        tests.testFunc(function(){return handler;},
            'handler', 'hasCached');
        tests.testFunc(function(){return handler;},
            'handler', 'cache');
        tests.testFunc(function(){return handler;},
            'handler', 'getAreaCount');
        tests.testFunc(function(){return handler;},
            'handler', 'getBlockData');
        tests.testFunc(function(){return handler;},
            'handler', 'getScatterData');
        tests.testFunc(function(){return handler;},
            'handler', 'getLineData');
        tests.testFunc(function(){return handler;},
            'handler', 'getNetData');
        tests.testFunc(function(){return handler;},
            'handler', 'getRadarData');


        tests.checkBool(function(){return handler.hasCached(0);},
            'handler.hasCached(0)', false);

        var testLine0 = {
            count: 3,
            attrs: null,
        };
        tests.checkNumber(function(){return handler.getAreaCount(testLine0);},
            'handler.getAreaCount(testLine0)', 3);

        var testLine1 = {
            count: 3,
            attrs: '{"laserpoints":{}}',
        };
        tests.checkNumber(function(){return handler.getAreaCount(testLine1);},
            'handler.getAreaCount(testLine1)', 3);

        var testLine2 = {
            count: 3,
            attrs: '{"laserpoints":{"area":2}}',
        };
        tests.checkNumber(function(){return handler.getAreaCount(testLine2);},
            'handler.getAreaCount(testLine2)', 1.5);

        describe('handler.cache(0, [])', function(){
            beforeEach(function() {
                handler.cache(0, []);
            });

            tests.checkBool(function(){return handler.hasCached(0);},
                'handler.hasCached(0)', true);
        });

        describe('blockData tests', function(){

            var testBlock = function (name, xMax, yMax, lines) {
                tests.testString(function(){return data;},
                    'data', 'labelName',
                    function(){return name;},name);

                tests.testArrayLength(function(){return data;},
                    'data', 'xDomain', xMax);
                tests.checkNumber(function(){return data.xDomain[0];},
                    'data.xDomain[0]', 0);
                tests.checkNumber(function(){return data.xDomain[xMax-1];},
                    'data.xDomain['+(xMax-1)+']', xMax-1);

                tests.testArrayLength(function(){return data;},
                    'data', 'yDomain', 2);
                tests.checkNumber(function(){return data.yDomain[0];},
                    'data.yDomain[0]', 0);
                tests.checkNumber(function(){return data.yDomain[1];},
                    'data.yDomain[1]', yMax);

                tests.testArrayLength(function(){return data;},
                    'data', 'data', lines.length);

                for(var i=0;i<lines.length;i++){
                    var line = lines[i];
                    testBlockLine(i, line.count, line.ids);
                }
            };

            var testBlockLine = function (index, count, ids) {
                tests.checkNumber(function(){return data.data[index].count;},
                    'data.data['+index+'].count', count);
                tests.testArray(function(){return data.data[index];},
                    'data.data['+index+']', 'ids',
                    function(){return ids;}, ids);
            };

            describe('default test', function(){
                var name = 'testName';
                var testData = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                    {id:3, count:1},
                    {id:4, count:4},
                    {id:5, count:5},
                ];
                // before(function() {
                //     console.log('testData:');
                //     console.log(testData);
                // });
                beforeEach(function() {
                    handler.cache(0, testData);
                    data = handler.getBlockData({id:0,name:name}, 1, false);
                });
                testBlock(name, 6, 2, [
                    {count: 1,ids:[0, 3]},
                    {count: 2,ids:[1]},
                    {count: 3,ids:[2]},
                    {count: 4,ids:[4]},
                    {count: 5,ids:[5]},
                ]);
            });

            describe('test with 0 values', function(){
                var name = 'testName';
                var testData = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                ];
                // before(function() {
                //     console.log('testData:');
                //     console.log(testData);
                // });
                beforeEach(function() {
                    handler.cache(0, testData);
                    data = handler.getBlockData({id:0,name:name}, 1, true);
                });
                testBlock(name, 4, 3, [
                    {count: 1,ids:[0]},
                    {count: 2,ids:[1]},
                    {count: 3,ids:[2]},
                    {count: 0,ids:[3, 4, 5]},
                ]);

            });

            describe('test with grouping of 2', function(){
                var name = 'testName';
                var testData = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                    {id:3, count:1},
                    {id:4, count:4},
                    {id:5, count:5},
                ];
                // before(function() {
                //     console.log('testData:');
                //     console.log(testData);
                // });
                beforeEach(function() {
                    handler.cache(0, testData);
                    data = handler.getBlockData({id:0,name:name}, 2, false);
                });
                testBlock(name, 3, 2, [
                    {count: 0,ids:[0, 3]},
                    {count: 1,ids:[1, 2]},
                    {count: 2,ids:[4, 5]},
                ]);
            });
        });

        describe('scatterData tests', function(){

            var testScatter = function (names, xMax, yMax, zMax, lines) {
                tests.testArrayLength(function(){return data;},
                    'data', 'labelNames', names.length);
                // for(var i=0;i<names.length;i++){
                //     tests.checkString(function(i){return data.labelNames[i];},
                //         'labelNames['+i+']',
                //         function(i){return names[i];},names[i]);
                // }
                tests.checkString(function(){return data.labelNames[0];},
                    'labelNames[0]',
                    function(){return names[0];},names[0]);
                tests.checkString(function(){return data.labelNames[names.length-1];},
                    'labelNames['+(names.length-1)+']',
                    function(){return names[names.length-1];},names[names.length-1]);

                tests.testArrayLength(function(){return data;},
                    'data', 'xDomain', 2);
                tests.checkNumber(function(){return data.xDomain[0];},
                    'data.xDomain[0]', -1);
                tests.checkNumber(function(){return data.xDomain[1];},
                    'data.xDomain[1]', xMax);

                tests.testArrayLength(function(){return data;},
                    'data', 'yDomain', 2);
                tests.checkNumber(function(){return data.yDomain[0];},
                    'data.yDomain[0]', 0);
                tests.checkNumber(function(){return data.yDomain[1];},
                    'data.yDomain[1]', yMax);

                tests.testArrayLength(function(){return data;},
                    'data', 'zDomain', 2);
                tests.checkNumber(function(){return data.zDomain[0];},
                    'data.zDomain[0]', 1);
                tests.checkNumber(function(){return data.zDomain[1];},
                    'data.zDomain[1]', zMax);

                tests.testArrayLength(function(){return data;},
                    'data', 'data', lines.length);
                for(var i=0;i<lines.length;i++){
                    var line = lines[i];
                    testScatterLine(i, line.label, line.count,
                        line.cInd, line.ids);
                }
            };

            var testScatterLine = function (index, label, count, countInd, ids) {
                tests.checkNumber(function(){return data.data[index].label;},
                    'data.data['+index+'].label', label);
                tests.checkNumber(function(){return data.data[index].count;},
                    'data.data['+index+'].count', count);
                tests.checkNumber(function(){return data.data[index].index;},
                    'data.data['+index+'].index', countInd);
                tests.testArray(function(){return data.data[index];},
                    'data.data['+index+']', 'ids',
                    function(){return ids;}, ids);
            };

            describe('test with 2 labels', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var testData0 = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                    {id:3, count:1},
                ];
                var testData1 = [
                    {id:3, count:2},
                    {id:4, count:4},
                    {id:5, count:6},
                ];
                // before(function() {
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    data = handler.getScatterData(
                        [{id:0,name:name0},{id:1,name:name1}], 1, false, false);
                });

                testScatter([name0, name1], 2, 7, 2, [
                    {ind:0, label:0, count:1, cInd:0, ids:[0, 3]},
                    {ind:1, label:0, count:2, cInd:1, ids:[1]},
                    {ind:2, label:0, count:3, cInd:2, ids:[2]},
                    {ind:3, label:1, count:2, cInd:0, ids:[3]},
                    {ind:4, label:1, count:4, cInd:1, ids:[4]},
                    {ind:5, label:1, count:6, cInd:2, ids:[5]},
                ]);
            });

            describe('test with 2 labels and 0 values', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var testData0 = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                ];
                var testData1 = [
                    {id:3, count:2},
                    {id:4, count:4},
                    {id:5, count:6},
                ];
                // before(function() {
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    data = handler.getScatterData(
                        [{id:0,name:name0},{id:1,name:name1}], 1, false, true);
                });

                testScatter([name0, name1], 2, 7, 3, [
                    {ind:0, label:0, count:1, cInd:0, ids:[0]},
                    {ind:1, label:0, count:2, cInd:1, ids:[1]},
                    {ind:2, label:0, count:3, cInd:2, ids:[2]},
                    {ind:3, label:0, count:0, cInd:3, ids:[3, 4, 5]},
                    {ind:4, label:1, count:2, cInd:0, ids:[3]},
                    {ind:5, label:1, count:4, cInd:1, ids:[4]},
                    {ind:6, label:1, count:6, cInd:2, ids:[5]},
                    {ind:7, label:1, count:0, cInd:3, ids:[0, 1, 2]},
                ]);
            });

            describe('test with 2 labels and grouping of 2', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var testData0 = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                    {id:3, count:1},
                    {id:4, count:1},
                ];
                var testData1 = [
                    {id:3, count:2},
                    {id:4, count:4},
                    {id:5, count:6},
                ];
                // before(function() {
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    data = handler.getScatterData(
                        [{id:0,name:name0},{id:1,name:name1}], 2, false, false);
                });

                testScatter([name0, name1], 2, 4, 3, [
                    {ind:0, label:0, count:0, cInd:0, ids:[0, 3, 4]},
                    {ind:1, label:0, count:1, cInd:1, ids:[1, 2]},
                    {ind:2, label:1, count:1, cInd:0, ids:[3]},
                    {ind:3, label:1, count:2, cInd:1, ids:[4]},
                    {ind:4, label:1, count:3, cInd:2, ids:[5]},
                ]);
            });

            describe('test with 2 labels and area calculation', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var testData0 = [
                    {id:0, count:1, attrs: '{"laserpoints":{"area":2}}'},
                    {id:1, count:2, attrs: '{"laserpoints":{"area":3}}'},
                    {id:2, count:3, attrs: '{"laserpoints":{"area":2}}'},
                    {id:3, count:1, attrs: '{"laserpoints":{"area":5}}'},
                ];
                var testData1 = [
                    {id:3, count:2, attrs: '{"laserpoints":{"area":5}}'},
                    {id:4, count:4, attrs: '{"laserpoints":{"area":2}}'},
                    {id:5, count:6, attrs: '{"laserpoints":{"area":7}}'},
                ];
                // before(function() {
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    data = handler.getScatterData(
                        [{id:0,name:name0},{id:1,name:name1}], 1, true, false);
                });

                testScatter([name0, name1], 2, 3, 1, [
                    {label:0, count:0.5, cInd:0, ids:[0]},
                    {label:0, count:2/3, cInd:1, ids:[1]},
                    {label:0, count:1.5, cInd:2, ids:[2]},
                    {label:0, count:1/5, cInd:3, ids:[3]},
                    {label:1, count:2/5, cInd:0, ids:[3]},
                    {label:1, count:2, cInd:1, ids:[4]},
                    {label:1, count:6/7, cInd:2, ids:[5]},
                ]);
            });
        });

        describe('lineData tests', function(){

            var testLine = function (names, xMax, yMax, zMax,
                                     lines, pointLines) {
                tests.testArrayLength(function(){return data;},
                    'data', 'labelNames', names.length);
                tests.checkString(function(){return data.labelNames[0];},
                    'labelNames[0]',
                    function(){return names[0];},names[0]);
                tests.checkString(function(){return data.labelNames[names.length-1];},
                    'labelNames['+(names.length-1)+']',
                    function(){return names[names.length-1];},names[names.length-1]);

                tests.testArrayLength(function(){return data;},
                    'data', 'xDomain', 2);
                tests.checkNumber(function(){return data.xDomain[0];},
                    'data.xDomain[0]', -1);
                tests.checkNumber(function(){return data.xDomain[1];},
                    'data.xDomain[1]', xMax);

                tests.testArrayLength(function(){return data;},
                    'data', 'yDomain', 2);
                tests.checkNumber(function(){return data.yDomain[0];},
                    'data.yDomain[0]', 0);
                tests.checkNumber(function(){return data.yDomain[1];},
                    'data.yDomain[1]', yMax);

                tests.testArrayLength(function(){return data;},
                    'data', 'zDomain', 2);
                tests.checkNumber(function(){return data.zDomain[0];},
                    'data.zDomain[0]', 1);
                tests.checkNumber(function(){return data.zDomain[1];},
                    'data.zDomain[1]', zMax);

                var i, line;
                tests.testArrayLength(function(){return data;},
                    'data', 'pointData', pointLines.length);
                for(i=0;i<pointLines.length;i++){
                    line = pointLines[i];
                    testPoint(i, line.label, line.count, line.pInd, line.ids);
                }

                tests.testArrayLength(function(){return data;},
                    'data', 'lineData', lines.length);
                for(i=0;i<lines.length;i++){
                    line = lines[i];
                    testLineLine(i, line.id, line.pInds, names.length);
                }
            };

            var testLineLine = function (index, id, pInds, points) {
                tests.checkNumber(function(){return data.lineData[index].id;},
                    'data.lineData['+index+'].imageId', id);
                tests.checkArrayLength(function(){return data.lineData[index].points;},
                    'data.lineData['+index+'].data', points);
                for(var i=0;i<points;i++){
                    testLinePoint(index, i, pInds[i]);
                }
            };

            var testLinePoint = function (dInd, lInd, pInd) {
                tests.checkNumber(
                    function(){return data.lineData[dInd].points[lInd].index;},
                    'data.lineData['+dInd+'].data['+lInd+'].index',
                    pInd);
            };

            var testPoint = function (index, label, count, pInd, ids) {
                tests.checkNumber(function(){return data.pointData[index].label;},
                    'data.pointData['+index+'].label', label);
                tests.checkNumber(function(){return data.pointData[index].count;},
                    'data.pointData['+index+'].count', count);
                tests.checkNumber(function(){return data.pointData[index].index;},
                    'data.pointData['+index+'].index', pInd);
                tests.testArray(function(){return data.pointData[index];},
                    'data.pointData['+index+']', 'ids',
                    function(){return ids;}, ids);
            };

            describe('test with 2 labels', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var testData0 = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                    {id:3, count:1},
                ];
                var testData1 = [
                    {id:3, count:2},
                    {id:4, count:4},
                    {id:5, count:6},
                ];
                // before(function() {
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    data = handler.getLineData(
                        [{id:0,name:name0},{id:1,name:name1}], 1, false);
                });

                testLine([name0, name1], 2, 7, 3, [
                    {id:0, pInds:[0, 1]},
                    {id:1, pInds:[2, 1]},
                    {id:2, pInds:[3, 1]},
                    {id:3, pInds:[0, 4]},
                    {id:4, pInds:[5, 6]},
                    {id:5, pInds:[5, 7]},
                ], [
                    {label:0, count:1, pInd:0, ids:[0, 3]},
                    {label:1, count:0, pInd:1, ids:[0, 1, 2]},
                    {label:0, count:2, pInd:2, ids:[1]},
                    {label:0, count:3, pInd:3, ids:[2]},
                    {label:1, count:2, pInd:4, ids:[3]},
                    {label:0, count:0, pInd:5, ids:[4, 5]},
                    {label:1, count:4, pInd:6, ids:[4]},
                    {label:1, count:6, pInd:7, ids:[5]},
                ]);
            });

            describe('test with 2 labels and grouping of 2', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var testData0 = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                    {id:3, count:1},
                ];
                var testData1 = [
                    {id:3, count:2},
                    {id:4, count:4},
                    {id:5, count:6},
                ];
                // before(function() {
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    data = handler.getLineData(
                        [{id:0,name:name0},{id:1,name:name1}], 2, false);
                });

                testLine([name0, name1], 2, 4, 4, [
                    {id:0, pInds:[0, 1]},
                    {id:1, pInds:[2, 1]},
                    {id:2, pInds:[2, 1]},
                    {id:3, pInds:[0, 3]},
                    {id:4, pInds:[0, 4]},
                    {id:5, pInds:[0, 5]},
                ], [
                    {label:0, count:0, pInd:0, ids:[0, 3, 4, 5]},
                    {label:1, count:0, pInd:1, ids:[0, 1, 2]},
                    {label:0, count:1, pInd:2, ids:[1, 2]},
                    {label:1, count:1, pInd:3, ids:[3]},
                    {label:1, count:2, pInd:4, ids:[4]},
                    {label:1, count:3, pInd:5, ids:[5]},
                ]);
            });

            describe('test with 2 labels and area calculation', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var testData0 = [
                    {id:0, count:1, attrs: '{"laserpoints":{"area":2}}'},
                    {id:1, count:2, attrs: '{"laserpoints":{"area":3}}'},
                    {id:2, count:3, attrs: '{"laserpoints":{"area":2}}'},
                    {id:3, count:1, attrs: '{"laserpoints":{"area":5}}'},
                ];
                var testData1 = [
                    {id:3, count:2, attrs: '{"laserpoints":{"area":5}}'},
                    {id:4, count:4, attrs: '{"laserpoints":{"area":2}}'},
                    {id:5, count:6, attrs: '{"laserpoints":{"area":7}}'},
                ];
                // before(function() {
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    data = handler.getLineData(
                        [{id:0,name:name0},{id:1,name:name1}], 1, true);
                });

                testLine([name0, name1], 2, 3, 3, [
                    {id:0, pInds:[0, 1]},
                    {id:1, pInds:[2, 1]},
                    {id:2, pInds:[3, 1]},
                    {id:3, pInds:[4, 5]},
                    {id:4, pInds:[6, 7]},
                    {id:5, pInds:[6, 8]},
                ], [
                    {label:0, count:0.5, pInd:0, ids:[0]},
                    {label:1, count:0, pInd:1, ids:[0, 1, 2]},
                    {label:0, count:2/3, pInd:2, ids:[1]},
                    {label:0, count:1.5, pInd:3, ids:[2]},
                    {label:0, count:1/5, pInd:4, ids:[3]},
                    {label:1, count:2/5, pInd:5, ids:[3]},
                    {label:0, count:0, pInd:6, ids:[4, 5]},
                    {label:1, count:2, pInd:7, ids:[4]},
                    {label:1, count:6/7, pInd:8, ids:[5]},
                ]);
            });
        });

        describe('gdpData tests', function(){

            var testScatter = function (index, names, xMax, yMax, zMax, lines) {
                tests.testArrayLength(function(){return data.data[index];},
                    'data', 'labelNames', names.length);
                // for(var i=0;i<names.length;i++){
                //     tests.checkString(function(i){return data.labelNames[i];},
                //         'labelNames['+i+']',
                //         function(i){return names[i];},names[i]);
                // }
                tests.checkString(function(){return data.data[index].labelNames[0];},
                    'labelNames[0]',
                    function(){return names[0];},names[0]);
                tests.checkString(function(){return data.data[index].labelNames[names.length-1];},
                    'labelNames['+(names.length-1)+']',
                    function(){return names[names.length-1];},names[names.length-1]);

                tests.testArrayLength(function(){return data.data[index];},
                    'data', 'xDomain', 2);
                tests.checkNumber(function(){return data.data[index].xDomain[0];},
                    'data.xDomain[0]', -1);
                tests.checkNumber(function(){return data.data[index].xDomain[1];},
                    'data.xDomain[1]', yMax);

                tests.testArrayLength(function(){return data.data[index];},
                    'data', 'yDomain', 2);
                tests.checkNumber(function(){return data.data[index].yDomain[0];},
                    'data.yDomain[0]', -1);
                tests.checkNumber(function(){return data.data[index].yDomain[1];},
                    'data.yDomain[1]', yMax);

                tests.testArrayLength(function(){return data.data[index];},
                    'data', 'zDomain', 2);
                tests.checkNumber(function(){return data.data[index].zDomain[0];},
                    'data.zDomain[0]', 1);
                tests.checkNumber(function(){return data.data[index].zDomain[1];},
                    'data.zDomain[1]', zMax);

                tests.testArrayLength(function(){return data.data[index];},
                    'data', 'data', lines.length);
                for(var i=0;i<lines.length;i++){
                    var line = lines[i];
                    testScatterLine(index, i, line.label, line.count,
                        line.cInd, line.ids);
                }
            };

            var testScatterLine = function (ind, index, label, count, countInd, ids) {
                tests.checkNumber(function(){return data.data[ind].data[index].label;},
                    'data.data['+index+'].label', label);
                tests.checkNumber(function(){return data.data[ind].data[index].count;},
                    'data.data['+index+'].count', count);
                tests.checkNumber(function(){return data.data[ind].data[index].index;},
                    'data.data['+index+'].index', countInd);
                tests.testArray(function(){return data.data[ind].data[index];},
                    'data.data['+index+']', 'ids',
                    function(){return ids;}, ids);
            };

            describe('test with 2 labels', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var testData0 = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                    {id:3, count:1},
                    {id:5, count:2},
                    {id:6, count:2},
                    {id:7, count:2},
                ];
                var testData1 = [
                    {id:3, count:2},
                    {id:4, count:4},
                    {id:5, count:6},
                    {id:7, count:6},
                ];
                // before(function() {
                //
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    allImages = [
                        {id:0},{id:1},{id:2},{id:3},{id:4},
                        {id:5},{id:6},{id:7},{id:8},{id:9},
                    ];
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    data = handler.getGDPData(
                        [{id:0,name:name0},{id:1,name:name1}], 1, false, false);
                });

                testScatter(0, [name0, name1], 7, 7, 2, [
                    {ind:0, label:1, count:0, cInd:0, ids:[0]},
                    {ind:1, label:2, count:0, cInd:1, ids:[1, 6]},
                    {ind:2, label:3, count:0, cInd:2, ids:[2]},
                    {ind:3, label:1, count:2, cInd:3, ids:[3]},
                    {ind:4, label:0, count:4, cInd:4, ids:[4]},
                    {ind:5, label:2, count:6, cInd:5, ids:[5, 7]},
                ]);
            });

            describe('test with 2 labels and 0 values', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var testData0 = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                    {id:3, count:1},
                    {id:5, count:2},
                    {id:6, count:2},
                    {id:7, count:2},
                ];
                var testData1 = [
                    {id:3, count:2},
                    {id:4, count:4},
                    {id:5, count:6},
                    {id:7, count:6},
                ];
                // before(function() {
                //
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    allImages = [
                        {id:0},{id:1},{id:2},{id:3},{id:4},
                        {id:5},{id:6},{id:7},{id:8},{id:9},
                    ];
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    data = handler.getGDPData(
                        [{id:0,name:name0},{id:1,name:name1}], 1, false, true);
                });

                testScatter(0, [name0, name1], 7, 7, 2, [
                    {ind:0, label:1, count:0, cInd:0, ids:[0]},
                    {ind:1, label:2, count:0, cInd:1, ids:[1, 6]},
                    {ind:2, label:3, count:0, cInd:2, ids:[2]},
                    {ind:3, label:1, count:2, cInd:3, ids:[3]},
                    {ind:4, label:0, count:4, cInd:4, ids:[4]},
                    {ind:5, label:2, count:6, cInd:5, ids:[5, 7]},
                    {ind:6, label:0, count:0, cInd:6, ids:[8, 9]},
                ]);
            });


            describe('test with 2 labels and grouping of 2', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var testData0 = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                    {id:3, count:1},
                    {id:5, count:2},
                    {id:6, count:2},
                    {id:7, count:2},
                ];
                var testData1 = [
                    {id:3, count:2},
                    {id:4, count:4},
                    {id:5, count:6},
                    {id:7, count:6},
                ];
                // before(function() {
                //
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    allImages = [
                        {id:0},{id:1},{id:2},{id:3},{id:4},
                        {id:5},{id:6},{id:7},{id:8},{id:9},
                    ];
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    data = handler.getGDPData(
                        [{id:0,name:name0},{id:1,name:name1}], 2, false, false);
                });

                testScatter(0, [name0, name1], 4, 4, 3, [
                    {label:0, count:0, cInd:0, ids:[0]},
                    {label:1, count:0, cInd:1, ids:[1, 2, 6]},
                    {label:0, count:1, cInd:2, ids:[3]},
                    {label:0, count:2, cInd:3, ids:[4]},
                    {label:1, count:3, cInd:4, ids:[5, 7]},
                ]);
            });

            describe('test with 2 labels and area calculation', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';

                var testData0 = [
                    {id:0, count:1, attrs: '{"laserpoints":{"area":2}}'},
                    {id:1, count:2, attrs: '{"laserpoints":{"area":4}}'},
                    {id:2, count:3, attrs: '{"laserpoints":{"area":2}}'},
                    {id:3, count:1, attrs: '{"laserpoints":{"area":5}}'},
                ];
                var testData1 = [
                    {id:3, count:2, attrs: '{"laserpoints":{"area":5}}'},
                    {id:4, count:4, attrs: '{"laserpoints":{"area":2}}'},
                    {id:5, count:6, attrs: '{"laserpoints":{"area":7}}'},
                ];
                // before(function() {
                //
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    allImages = [
                        {id:0},{id:1},{id:2},{id:3},{id:4},
                        {id:5},{id:6},{id:7},{id:8},{id:9},
                    ];
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    data = handler.getGDPData(
                        [{id:0,name:name0},{id:1,name:name1}], 1, true, false);
                });

                testScatter(0, [name0, name1], 3, 3, 2, [
                    {label:0.5, count:0, cInd:0, ids:[0, 1]},
                    {label:3/2, count:0, cInd:1, ids:[2]},
                    {label:1/5, count:2/5, cInd:2, ids:[3]},
                    {label:0, count:2, cInd:3, ids:[4]},
                    {label:0, count:6/7, cInd:4, ids:[5]},
                ]);
            });
        });

        describe('netData tests', function(){

            var testNet = function (names, xMax, yMax, zMax,
                                    lines, pointLines) {
                tests.testArrayLength(function(){return data;},
                    'data', 'labelNames', names.length);
                tests.checkString(function(){return data.labelNames[0];},
                    'labelNames[0]',
                    function(){return names[0];},names[0]);
                tests.checkString(function(){return data.labelNames[names.length-1];},
                    'labelNames['+(names.length-1)+']',
                    function(){return names[names.length-1];},names[names.length-1]);

                var i, line;
                tests.testArrayLength(function(){return data;},
                    'data', 'pointData', pointLines.length);
                for(i=0;i<pointLines.length;i++){
                    line = pointLines[i];
                    testPoint(i, line.label, line.count, line.pInd, line.ids);
                }

                tests.testArrayLength(function(){return data;},
                    'data', 'lineData', lines.length);
                for(i=0;i<lines.length;i++){
                    line = lines[i];
                    testLineLine(i, line.id, line.pInds, names.length);
                }
            };

            var testLineLine = function (index, id, pInds, points) {
                tests.checkNumber(function(){return data.lineData[index].id;},
                    'data.lineData['+index+'].id', id);
                tests.checkArrayLength(function(){return data.lineData[index].points;},
                    'data.lineData['+index+'].points', points);
                for(var i=0;i<points;i++){
                    testLinePoint(index, i, pInds[i]);
                }
            };

            var testLinePoint = function (dInd, lInd, pInd) {
                tests.checkNumber(
                    function(){return data.lineData[dInd].points[lInd].index;},
                    'data.lineData['+dInd+'].points['+lInd+'].index',
                    pInd);
            };

            var testPoint = function (index, label, anno, pInd, ids) {
                tests.checkNumber(function(){return data.pointData[index].label;},
                    'data.pointData['+index+'].label', label);
                tests.checkNumber(function(){return data.pointData[index].count;},
                    'data.pointData['+index+'].count', anno);
                tests.checkNumber(function(){return data.pointData[index].index;},
                    'data.pointData['+index+'].index', pInd);
                tests.testArray(function(){return data.pointData[index];},
                    'data.pointData['+index+']', 'ids',
                    function(){return ids;}, ids);
            };

            describe('test with 2 labels', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var testData0 = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                    {id:3, count:2},
                ];
                var testData1 = [
                    {id:1, count:1},
                    {id:2, count:4},
                    {id:3, count:2},
                ];
                // before(function() {
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    data = handler.getNetData(
                        [{id:0,name:name0},{id:1,name:name1}], 1);
                });

                testNet([name0, name1], 2, 7, 3, [
                    {id:1, pInds:[0, 2]},
                    {id:2, pInds:[1, 4]},
                    {id:3, pInds:[0, 3]},
                ], [
                    {label:0, count:2, pInd:0, ids:[1, 3]},
                    {label:0, count:3, pInd:1, ids:[2]},
                    {label:1, count:1, pInd:2, ids:[1]},
                    {label:1, count:2, pInd:3, ids:[3]},
                    {label:1, count:4, pInd:4, ids:[2]},
                ]);
            });

            describe('test with 2 labels and grouping of 2', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var testData0 = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                    {id:3, count:2},
                ];
                var testData1 = [
                    {id:1, count:1},
                    {id:2, count:4},
                    {id:3, count:2},
                ];
                // before(function() {
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    data = handler.getNetData(
                        [{id:0,name:name0},{id:1,name:name1}], 2);
                });

                testNet([name0, name1], 2, 7, 3, [
                    {id:1, pInds:[0, 1]},
                    {id:2, pInds:[0, 3]},
                    {id:3, pInds:[0, 2]},
                ], [
                    {label:0, count:1, pInd:0, ids:[1, 2, 3]},
                    {label:1, count:0, pInd:1, ids:[1]},
                    {label:1, count:1, pInd:2, ids:[3]},
                    {label:1, count:2, pInd:3, ids:[2]},
                    // {label:1, count:2, pInd:4, ids:[2]},
                ]);
            });
        });

        describe('radData tests', function(){

            var testRad = function (names, yMax, zMax,
                                    lines, pointLines) {
                tests.testArrayLength(function(){return data;},
                    'data', 'labelNames', names.length);
                tests.checkString(function(){return data.labelNames[0];},
                    'labelNames[0]',
                    function(){return names[0];},names[0]);
                tests.checkString(function(){return data.labelNames[names.length-1];},
                    'labelNames['+(names.length-1)+']',
                    function(){return names[names.length-1];},names[names.length-1]);

                tests.testArrayLength(function(){return data;},
                    'data', 'yDomain', 2);
                tests.checkNumber(function(){return data.yDomain[0];},
                    'data.yDomain[0]', -1);
                tests.checkNumber(function(){return data.yDomain[1];},
                    'data.yDomain[1]', yMax);

                tests.testArrayLength(function(){return data;},
                    'data', 'zDomain', 2);
                tests.checkNumber(function(){return data.zDomain[0];},
                    'data.zDomain[0]', 1);
                tests.checkNumber(function(){return data.zDomain[1];},
                    'data.zDomain[1]', zMax);

                var i, line;
                tests.testArrayLength(function(){return data;},
                    'data', 'pointData', pointLines.length);
                for(i=0;i<pointLines.length;i++){
                    line = pointLines[i];
                    testPoint(i, line.label, line.count, line.pInd, line.ids);
                }

                tests.testArrayLength(function(){return data;},
                    'data', 'lineData', lines.length);
                for(i=0;i<lines.length;i++){
                    line = lines[i];
                    testLineLine(i, line.id, line.pInds, names.length+1);
                }
            };

            var testLineLine = function (index, id, pInds, points) {
                tests.checkNumber(function(){return data.lineData[index].id;},
                    'data.lineData['+index+'].id', id);
                tests.checkArrayLength(function(){return data.lineData[index].points;},
                    'data.lineData['+index+'].points', points);
                for(var i=0;i<points;i++){
                    testLinePoint(index, i, pInds[i]);
                }
            };

            var testLinePoint = function (dInd, lInd, pInd) {
                tests.checkNumber(
                    function(){return data.lineData[dInd].points[lInd].index;},
                    'data.lineData['+dInd+'].data['+lInd+'].index',
                    pInd);
            };

            var testPoint = function (index, label, count, pInd, ids) {
                tests.checkNumber(function(){return data.pointData[index].label;},
                    'data.pointData['+index+'].label', label);
                tests.checkNumber(function(){return data.pointData[index].count;},
                    'data.pointData['+index+'].count', count);
                tests.checkNumber(function(){return data.pointData[index].index;},
                    'data.pointData['+index+'].index', pInd);
                tests.testArray(function(){return data.pointData[index];},
                    'data.pointData['+index+']', 'ids',
                    function(){return ids;}, ids);
            };

            describe('test with 3 labels', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var name2 = 'testName2';
                var testData0 = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                    {id:3, count:1},
                ];
                var testData1 = [
                    {id:3, count:2},
                    {id:4, count:4},
                ];
                var testData2 = [
                    {id:1, count:1},
                    {id:2, count:1},
                    {id:5, count:3},
                ];
                // before(function() {
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    handler.cache(2, testData2);
                    data = handler.getRadarData(
                        [{id:0,name:name0},{id:1,name:name1},{id:2,name:name2}], 1, false);
                });

                testRad([name0, name1, name2], 5, 4, [
                    {id:0, pInds:[0, 1, 2, 0]},
                    {id:1, pInds:[3, 1, 4, 3]},
                    {id:2, pInds:[5, 1, 4, 5]},
                    {id:3, pInds:[0, 6, 2, 0]},
                    {id:4, pInds:[7, 8, 2, 7]},
                    {id:5, pInds:[7, 1, 9, 7]},
                ], [
                    {label:0, count:1, pInd:0, ids:[0, 3]},
                    {label:1, count:0, pInd:1, ids:[0, 1, 2, 5]},
                    {label:2, count:0, pInd:2, ids:[0, 3, 4]},
                    {label:0, count:2, pInd:3, ids:[1]},
                    {label:2, count:1, pInd:4, ids:[1, 2]},
                    {label:0, count:3, pInd:5, ids:[2]},
                    {label:1, count:2, pInd:6, ids:[3]},
                    {label:0, count:0, pInd:7, ids:[4,5]},
                    {label:1, count:4, pInd:8, ids:[4]},
                    {label:2, count:3, pInd:9, ids:[5]},
                ]);
            });

            describe('test with 3 labels and grouping of 2', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var name2 = 'testName2';
                var testData0 = [
                    {id:0, count:1},
                    {id:1, count:2},
                    {id:2, count:3},
                    {id:3, count:1},
                ];
                var testData1 = [
                    {id:3, count:2},
                    {id:4, count:4},
                ];
                var testData2 = [
                    {id:1, count:1},
                    {id:2, count:1},
                    {id:5, count:3},
                ];
                // before(function() {
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                // });
                beforeEach(function() {
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    handler.cache(2, testData2);
                    data = handler.getRadarData(
                        [{id:0,name:name0},{id:1,name:name1},{id:2,name:name2}], 2, false);
                });

                testRad([name0, name1, name2], 3, 5, [
                    {id:0, pInds:[0, 1, 2, 0]},
                    {id:1, pInds:[3, 1, 2, 3]},
                    {id:2, pInds:[3, 1, 2, 3]},
                    {id:3, pInds:[0, 4, 2, 0]},
                    {id:4, pInds:[0, 5, 2, 0]},
                    {id:5, pInds:[0, 1, 6, 0]},
                ], [
                    {label:0, count:0, pInd:0, ids:[0, 3, 4, 5]},
                    {label:1, count:0, pInd:1, ids:[0, 1, 2, 5]},
                    {label:2, count:0, pInd:2, ids:[0, 1, 2, 3, 4]},
                    {label:0, count:1, pInd:3, ids:[1, 2]},
                    {label:1, count:1, pInd:4, ids:[3]},
                    {label:1, count:2, pInd:5, ids:[4]},
                    {label:2, count:1, pInd:6, ids:[5]},
                ]);
            });

            describe('test with 3 labels and area calculation', function(){
                var name0 = 'testName0';
                var name1 = 'testName1';
                var name2 = 'testName2';
                var testData0 = [
                    {id:0, count:1, attrs: '{"laserpoints":{"area":2}}'},
                    {id:1, count:2, attrs: '{"laserpoints":{"area":3}}'},
                    {id:2, count:2, attrs: '{"laserpoints":{"area":4}}'},
                    {id:3, count:1, attrs: '{"laserpoints":{"area":5}}'},
                ];
                var testData1 = [
                    {id:3, count:2, attrs: '{"laserpoints":{"area":5}}'},
                    {id:4, count:4, attrs: '{"laserpoints":{"area":2}}'},
                    // {id:5, count:6, attrs: '{"laserpoints":{"area":7}}'},
                ];
                var testData2 = [
                    {id:1, count:1, attrs: '{"laserpoints":{"area":3}}'},
                    {id:2, count:1, attrs: '{"laserpoints":{"area":2}}'},
                    {id:5, count:6, attrs: '{"laserpoints":{"area":7}}'},
                ];
                // before(function() {
                //     console.log('testData0:');
                //     console.log(testData0);
                //     console.log('testData1:');
                //     console.log(testData1);
                //     console.log('testData2:');
                //     console.log(testData2);
                // });
                beforeEach(function() {
                    handler.cache(0, testData0);
                    handler.cache(1, testData1);
                    handler.cache(2, testData2);
                    data = handler.getRadarData(
                        [{id:0,name:name0},{id:1,name:name1},{id:2,name:name2}], 1, true);
                });

                testRad([name0, name1, name2], 3, 4, [
                    {id:0, pInds:[0, 1, 2, 0]},
                    {id:1, pInds:[3, 1, 4, 3]},
                    {id:2, pInds:[0, 1, 5, 0]},
                    {id:3, pInds:[6, 7, 2, 6]},
                    {id:4, pInds:[8, 9, 2, 8]},
                    {id:5, pInds:[8, 1, 10, 8]},
                ], [
                    {label:0, count:1/2, pInd:0, ids:[0, 2]},
                    {label:1, count:0, pInd:1, ids:[0, 1, 2, 5]},
                    {label:2, count:0, pInd:2, ids:[0, 3, 4]},
                    {label:0, count:2/3, pInd:3, ids:[1]},
                    {label:2, count:1/3, pInd:4, ids:[1]},
                    {label:2, count:1/2, pInd:5, ids:[2]},
                    {label:0, count:1/5, pInd:6, ids:[3]},
                    {label:1, count:2/5, pInd:7, ids:[3]},
                    {label:0, count:0, pInd:8, ids:[4, 5]},
                    {label:1, count:2, pInd:9, ids:[4]},
                    {label:2, count:6/7, pInd:10, ids:[5]},
                ]);
            });
        });
    });
});