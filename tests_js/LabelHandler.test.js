var tests = require('./tests');
var LabelHandler = require('../src/resources/assets/js/visualisation/classes/LabelHandler');

describe('LabelHandler tests', function(){
    var events = {
        // $emit: function () {
        //     console.log('$emit:');
        //
        // }
    };
    events['$emit'] = function () {
        // console.log('$emit:');

    };
    var messages = {
        handleErrorResponse: function () {

        }
    };
    var imagesWithLabel;
    var controller;
    var handler;

    beforeEach(function() {
        imagesWithLabel = {

            get: function (params) {
                this.params = params;
                // console.log('get:');
                return this;
            },
            bind: function (child) {
                this.child = child;
                // console.log('bind:');
                return this;
            },
            then: function (success, error) {
                this.success = success;
                this.error = error;
                // console.log('then:');
                return this;
            },
            finally: function (fin) {
                this.fin = fin;
                // console.log('finally:');
                return this;
            }
        };
        controller = {
            dataHandler: {
                // has: true,
                hasCached: function (id) {
                    // console.log('hasCached:');
                    return false;
                },
                cache: function () {

                }
            },
            updateSelectModels: function () {
                
            }
        };
        handler = new LabelHandler(imagesWithLabel);
    });


    describe('handler = new LabelHandler(imagesWithLabel)', function() {
        tests.checkObject(function () {return handler;},
            'handler');
        tests.testArrayLength(function(){return handler;},
            'handler', 'selectedLabels', 0);
        tests.testFunc(function(){return handler;},
            'handler', 'updateLabel');
        tests.testFunc(function(){return handler;},
            'handler', 'removeLabel');
        tests.testFunc(function(){return handler;},
            'handler', 'clearLabels');
        tests.testFunc(function(){return handler;},
            'handler', 'addLabel');
    });

    describe('handler.addLabel({id:0})', function() {
        var testLabel0 = {id:0};
        beforeEach(function() {
            handler.addLabel(testLabel0, controller);
        });
        tests.testArrayLength(function(){return handler;},
            'handler', 'selectedLabels', 1, true);
        tests.checkObject(function () {return handler.selectedLabels[0];},
            'selectedLabels[0]',
            function () {return testLabel0;}, '{id:0}');
    });
    describe('handler.addLabel({id:0})', function() {
        var testLabel0 = {id:0};
        beforeEach(function() {
            handler.addLabel(testLabel0, controller);
            handler.addLabel(testLabel0, controller);
        });
        tests.testArrayLength(function(){return handler;},
            'handler', 'selectedLabels', 1, true);
    });
    describe('handler.addLabel({id:1})', function() {
        var testLabel0 = {id:0};
        var testLabel1 = {id:1};
        beforeEach(function() {
            handler.addLabel(testLabel0, controller);
            handler.addLabel(testLabel1, controller);
        });
        tests.testArrayLength(function(){return handler;},
            'handler', 'selectedLabels', 2, true);
        tests.checkObject(function () {return handler.selectedLabels[1];},
            'selectedLabels[1]',
            function () {return testLabel1;}, '{id:1}');
    });
    describe('handler.removeLabel({id:2})', function() {
        var testLabel0 = {id:0};
        var testLabel1 = {id:1};
        var testLabel2 = {id:2};
        beforeEach(function() {
            handler.addLabel(testLabel0, controller);
            handler.addLabel(testLabel1, controller);
            handler.removeLabel(testLabel2, controller);
        });
        tests.testArrayLength(function(){return handler;},
            'handler', 'selectedLabels', 2, true);
    });
    describe('handler.removeLabel({id:0})', function() {
        var testLabel0 = {id:0};
        var testLabel1 = {id:1};
        beforeEach(function() {
            handler.addLabel(testLabel0, controller);
            handler.addLabel(testLabel1, controller);
            handler.removeLabel(testLabel0, controller);
        });
        tests.testArrayLength(function(){return handler;},
            'handler', 'selectedLabels', 1, true);
        tests.checkObject(function () {return handler.selectedLabels[0];},
            'selectedLabels[0]',
            function () {return testLabel1;}, '{id:1}');
    });
    describe('handler.removeLabel({id:0})', function() {
        var testLabel0 = {id:0};
        var testLabel1 = {id:1};
        beforeEach(function() {
            handler.addLabel(testLabel0, controller);
            handler.addLabel(testLabel1, controller);
            handler.removeLabel(testLabel0, controller);
            handler.clearLabels(controller);
        });
        tests.testArrayLength(function(){return handler;},
            'handler', 'selectedLabels', 0, true);
    });
    describe('handler.handleLabel({id:0}, events, messages, 0, controller); ' +
        'imagesWithLabel.error({})', function() {
        var testLabel0 = {id:0};
        beforeEach(function() {
            handler.updateLabel(testLabel0, events, messages, 0, controller);
            imagesWithLabel.error({});
        });
        tests.testArrayLength(function(){return handler;},
            'handler', 'selectedLabels', 0, true);
    });
    describe('handler.handleLabel({id:0}, events, messages, 0, controller); ' +
        'imagesWithLabel.success({})', function() {
        var testLabel0 = {id:0};
        beforeEach(function() {
            handler.updateLabel(testLabel0, events, messages, 0, controller);
            imagesWithLabel.success({});
        });
        tests.testArrayLength(function(){return handler;},
            'handler', 'selectedLabels', 1, true);
    });
});