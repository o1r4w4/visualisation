var tests = require('./tests');
var Constants = require('../src/resources/assets/js/visualisation/Constants');
var Models = require('../src/resources/assets/js/visualisation/classes/Models');
var TestHandler = require('../src/resources/assets/js/visualisation/classes/TestHandler');
var TEST_STATE_FIRST = Constants.TEST_STATE_FIRST;
var TEST_STATE_SECOND = Constants.TEST_STATE_SECOND;
var TEST_STATE_EVAL = Constants.TEST_STATE_EVAL;
var TEST_STATE_END = Constants.TEST_STATE_END;

describe('TestHandler tests', function(){
    var name = 'testName';
    var model;
    var testArray;
    var handler;

    beforeEach(function() {
        model = {
            selections: [],
            radio: {value:null},
            name: {value:null},
            text: {value:null},
        };
        //TODO questions
        var test0 = new Models.VisualisationTestModel();
        test0.questions.push(new Models.VisualisationQuestModel(
            'Find all images with at least 1 animalia annotation.',
            [0]
        ));
        test0.questions.push(new Models.VisualisationQuestModel(
            'Find all images with at least 4 unknown annotations.',
            [0, 1]
        ));
        var test1 = new Models.VisualisationTestModel();
        test1.questions.push(new Models.VisualisationQuestModel(
            'Find all images with at least 1 animalia annotation.',
            [2, 3]
        ));
        test1.questions.push(new Models.VisualisationQuestModel(
            'Find all images with at least 4 unknown annotations.',
            [4]
        ));
        var test2 = new Models.VisualisationTestModel();
        test2.questions.push(new Models.VisualisationQuestModel(
            'How did this module effect Your efficiency?'
        ));
        test2.questions.push(new Models.VisualisationQuestModel(
            'How would You evaluate the usability of this module?'
        ));
        var test3 = new Models.VisualisationTestModel();
        test3.questions.push(new Models.VisualisationQuestModel(
            'How did this module effect Your efficiency?'
        ));
        testArray = [test0, test1, test2, test3];
        handler = new TestHandler(model, testArray, null, {id:0, name:'volume'});
    });

    // describe('handler = new TestHandler(model, testArray)', function(){

    tests.checkObject(function(){return handler;},
        'handler');

    tests.testObject(function(){return handler;},
        'handler', 'model', function(){return model;}, 'model');

    tests.testArray(function(){return handler;},
        'handler', 'tests', function(){return testArray;}, 'testArray', true);

    tests.testBool(function(){return handler;},
        'handler', 'block', false);

    tests.testFunc(function(){return handler;},
        'handler', 'start');
    tests.testFunc(function(){return handler;},
        'handler', 'initTest');
    tests.testFunc(function(){return handler;},
        'handler', 'update');
    tests.testFunc(function(){return handler;},
        'handler', 'initAnswer');
    tests.testFunc(function(){return handler;},
        'handler', 'updateAnswer');
    tests.testFunc(function(){return handler;},
        'handler', 'checkAnswer');
    tests.testFunc(function(){return handler;},
        'handler', 'updateText');
    tests.testFunc(function(){return handler;},
        'handler', 'updateVis');
    tests.testFunc(function(){return handler;},
        'handler', 'computeData');
    tests.testFunc(function(){return handler;},
        'handler', 'parseData');

    // });

    describe('handler.start()', function(){
        beforeEach(function() {
            handler.start();
        });
        tests.testBool(function(){return handler;},
            'handler', 'running', false);
    });

    describe('model.name.value = "testName"; handler.start()', function(){
        beforeEach(function() {
            model.name.value = name;
            handler.start();
        });

        tests.testBool(function(){return handler;},
            'handler', 'running', true);

        tests.testNumber(function(){return handler;},
            'handler', 'testState', function(){return TEST_STATE_FIRST;}, TEST_STATE_FIRST);

        tests.testBool(function(){return handler;},
            'handler', 'block', true, true);

        tests.testObject(function(){return handler;},
            'handler', 'data');

        tests.testString(function(){return handler.data;},
            'data', 'name', function(){return name;}, name);

        var date = new Date();
        tests.testString(function(){return handler.data;},
            'data', 'date', function(){return date.toISOString().substring(0, 10);},
            date.toISOString().substring(0, 10));


        tests.testObject(function(){return handler;},
            'handler', 'test', function(){return testArray[0];}, 'testArray[0]');

        tests.testNumber(function(){return handler;},
            'handler', 'index', function(){return 0;}, 0);

        tests.testString(function(){return model;},
            'handler', 'quest',
            function(){return handler.test.questions[0].question;},
            'test.questions[0].question', true);

        tests.testBool(function(){return model;},
            'handler', 'hideHead', true);
        tests.testBool(function(){return model;},
            'handler', 'hideBody', false);


        describe('handler.update()', function() {
            beforeEach(function () {
                handler.update();
            });
            tests.testNumber(function(){return handler;},
                'handler', 'index', function(){return 0;}, 0, true);
        });
        //TODO first answer
        describe('model.selections.push({value:0}); handler.update()', function() {
            beforeEach(function () {
                model.selections.push({value:0});
                handler.update();
            });
            tests.testNumber(function(){return handler;},
                'handler', 'index', function(){return 1;}, 1, true);


            tests.testNumber(function(){return handler.data.first.results[0];},
                'first.results[0]', 'end');

            tests.testNumber(function(){return handler.data.first.results[0];},
                'first.results[0]', 'time',
                function(){return (handler.data.first.results[0].end-
                    handler.data.first.results[0].start)/1000.0;},
                '(end-start)/1000.0');

            tests.testNumber(function(){return handler.data.first;},
                'first', 'right', function(){return 1;}, 1, true);
            tests.testNumber(function(){return handler.data.first;},
                'first', 'wrong', function(){return 0;}, 0, true);

            tests.testNumber(function(){return handler.data.first.results[0];},
                'first.results[0]', 'quota',
                function(){return 1;},
                1);

            tests.testArrayLength(function(){return handler.data.first;},
                'first', 'results', 2, true);;
            tests.testString(function(){return model;},
                'model', 'quest',
                function(){return handler.test.questions[1].question;},
                'handler.test.questions[1].question', true, true);

            //TODO second answer
            describe('model.selections.push({value:1},{value:0}); ' +
                'handler.update()', function() {
                beforeEach(function () {
                    model.selections.push({value:1});
                    model.selections.push({value:0});
                    handler.update();
                });
                tests.testNumber(function(){return handler;},
                    'handler', 'index', function(){return 0;}, 0, true);

                tests.testNumber(function(){return handler;},
                    'handler', 'testState',
                    function(){return TEST_STATE_SECOND;}, TEST_STATE_SECOND, true);

                tests.testBool(function(){return handler;},
                    'handler', 'block', false, true);

                tests.testNumber(function(){return handler.data.first;},
                    'first', 'right', function(){return 2;}, 2, true);
                tests.testNumber(function(){return handler.data.first;},
                    'first', 'wrong', function(){return 0;}, 0, true);
                tests.testNumber(function(){return handler.data.first;},
                    'first', 'quota', function(){return 1;}, 1, true);

                tests.testNumber(function(){return handler.data.first.results[1];},
                    'first.results[1]', 'quota',
                    function(){return 1;},
                    1);

                tests.testArrayLength(function(){return handler.data.second;},
                    'second', 'results', 1, true);

                tests.testObject(function(){return handler;},
                    'handler', 'test', function(){return testArray[1];},
                    'testArray[1]', true);
                tests.testString(function(){return model;},
                    'model', 'quest',
                    function(){return handler.test.questions[0].question;},
                    'handler.test.questions[0].question', true, true);


                //TODO third answer
                describe('model.selections.push({value:2}); handler.update()', function() {
                    beforeEach(function () {
                        model.selections.push({value:2});
                        handler.update();
                    });

                    tests.testNumber(function(){return handler;},
                        'handler', 'index', function(){return 1;}, 1, true);

                    tests.testNumber(function(){return handler.data.second;},
                        'second', 'right', function(){return 0;}, 0, true);
                    tests.testNumber(function(){return handler.data.second;},
                        'second', 'wrong', function(){return 1;}, 1, true);

                    tests.testNumber(function(){return handler.data.second.results[0];},
                        'second.results[0]', 'quota',
                        function(){return 0.5;},
                        0.5);

                    tests.testArrayLength(function(){return handler.data.second;},
                        'second', 'results', 2, true);
                    tests.testString(function(){return model;},
                        'model', 'quest',
                        function(){return handler.test.questions[1].question;},
                        'handler.test.questions[1].question', true, true);


                    //TODO fourth answer
                    describe('model.selections.push({value:4},{value:5},{value:6}); ' +
                        'model.selections.push(2);' +
                        ' handler.update()', function() {
                        beforeEach(function () {
                            model.selections.push({value:4});
                            model.selections.push({value:5});
                            model.selections.push({value:6});
                            handler.update();
                        });
                        tests.testNumber(function(){return handler;},
                            'handler', 'index', function(){return 0;}, 0, true);

                        tests.testNumber(function(){return handler;},
                            'handler', 'testState',
                            function(){return TEST_STATE_EVAL;}, TEST_STATE_EVAL, true);

                        tests.testNumber(function(){return handler.data.second;},
                            'second', 'right', function(){return 0;}, 0, true);
                        tests.testNumber(function(){return handler.data.second;},
                            'second', 'wrong', function(){return 2;}, 2, true);
                        tests.testNumber(function(){return handler.data.second;},
                            'second', 'quota', function(){return 0;}, 0, true);

                        tests.testNumber(function(){return handler.data.second.results[1];},
                            'second.results[1]', 'quota',
                            function(){return 1/3;},
                            1/3);


                        tests.testNumber(function(){return handler.data;},
                            'data', 'quota'
                            ,
                            function(){return 0.5;},
                            0.5
                        );
                        tests.testNumber(function(){return handler.data;},
                            'data', 'time'
                            ,
                            function(){return handler.data.first.time+
                                handler.data.second.time;},
                            'first.time+second.time'
                        );

                        tests.testArrayLength(function(){return handler.data.second;},
                            'second', 'results', 2, true);
                        tests.testObject(function(){return handler;},
                            'handler', 'test', function(){return testArray[2];},
                            'testArray[2]', true);
                        tests.testString(function(){return model;},
                            'model', 'quest',
                            function(){return handler.test.questions[0].question;},
                            'handler.test.questions[0].question', true, true);


                        describe('handler.update()', function() {
                            beforeEach(function () {
                                handler.update();
                            });
                            tests.testNumber(function(){return handler;},
                                'handler', 'index', function(){return 0;}, 0, true);
                        });


                        //TODO first statement
                        describe('model.radio.value = 1; handler.update()', function() {
                            beforeEach(function () {
                                model.radio.value = 1;
                                handler.update();
                            });

                            tests.testNumber(function(){return handler;},
                                'handler', 'index', function(){return 1;}, 1, true);


                            tests.testNumber(
                                function(){return handler.data.evaluation.results[0];},
                                'evaluation.results[0]', 'answer',
                                function(){return 1;}, 1, true);

                            tests.testArrayLength(
                                function(){return handler.data.evaluation;},
                                'evaluation', 'results', 2, true);
                            tests.testString(function(){return model;},
                                'model', 'quest',
                                function(){return handler.test.questions[1].question;},
                                'handler.test.questions[1].question', true, true);


                            //TODO second statement

                            describe('model.radio.value = 5; handler.update()', function() {
                                beforeEach(function () {
                                    model.radio.value = 5;
                                    handler.update();
                                });

                                tests.testNumber(
                                    function(){return handler.data.evaluation.results[1];},
                                    'evaluation.results[1]', 'answer',
                                    function(){return 5;}, 5, true);
                                tests.testNumber(function(){return handler.data;},
                                    'data', 'note',
                                    function(){return 3;}, 3, true);
                                describe('model.text.value = "comment"; handler.update()', function() {
                                    var text = 'testText';
                                    beforeEach(function () {
                                        model.text.value = text;
                                        var exportHandler = {
                                            exportTest: function () {

                                            }
                                        };
                                        handler.update(exportHandler);
                                    });
                                    tests.testBool(function(){return handler;},
                                        'handler', 'running', false, true);
                                    tests.testBool(function(){return model;},
                                        'model', 'hideHead', false, true);
                                    tests.testBool(function(){return model;},
                                        'model', 'hideBody', true, true);
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});