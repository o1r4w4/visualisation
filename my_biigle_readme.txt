===Ubuntu 16.04.3==========================
swap area Logical ext4 ~2xRAM
root / Logical/Primary ext4 ~10-40GB
home /home Logical ext4 rest
boot /boot Primary ext4 ~500MB

var /var primary ext4 ~2GB
tmp /tmp primary ext4 ~2GB



Vagrant 1.9.5
"wget https://releases.hashicorp.com/vagrant/1.9.5/vagrant_1.9.5_x86_64.deb"
"sudo dpkg -i vagrant_1.9.5_x86_64.deb"
Virtualbox 5.1.22
"sudo apt install -f"
"wget download.virtualbox.org/virtualbox/5.1.22/virtualbox-5.1_5.1.22-115126~Ubuntu~xenial_amd64.deb"
"sudo dpkg -i virtualbox-5.1_5.1.22-115126~Ubuntu~xenial_amd64.deb"
Laravel 3.0.0
"version: 3.0.0" in homestead.yaml eintragen







===for testing===================
PHP
cd tests/php
ln -s ../../vendor/biigle/visualisation/tests/ visualisation-module
./vendor/bin/phpunit --filter VisualisationServiceProviderTest
./vendor/bin/phpunit --filter VisualisationLabelControllerTest
./vendor/bin/phpunit --filter VisualisationVolumeControllerTest
./vendor/bin/phpunit --filter Visualisation

JavaScipt
mocha vendor/biigle/visualisation/tests_js/DataHandler.test.js > vendor/biigle/visualisation/tests_js/DataHandler.txt

mocha vendor/biigle/visualisation/tests_js/LabelHandler.test.js > vendor/biigle/visualisation/tests_js/LabelHandler.txt

mocha vendor/biigle/visualisation/tests_js/Models.test.js > vendor/biigle/visualisation/tests_js/Models.txt

mocha vendor/biigle/visualisation/tests_js/SelectHandler.test.js > vendor/biigle/visualisation/tests_js/SelectHandler.txt

mocha vendor/biigle/visualisation/tests_js/SelectionHandler.test.js > vendor/biigle/visualisation/tests_js/SelectionHandler.txt

mocha vendor/biigle/visualisation/tests_js/TestHandler.test.js > vendor/biigle/visualisation/tests_js/TestHandler.txt

===PHPStorm============================
https://www.linuxbabe.com/desktop-linux/install-phpstorm-ubuntu-15-10
for java:
"sudo add-apt-repository ppa:webupd8team/java"

"sudo apt-get update"

"sudo apt-get install java-common oracle-java8-installer"

"sudo apt-get install oracle-java8-set-default"

"source /etc/profile"

for phpstorm:

"wget https://download-cf.jetbrains.com/webide/PhpStorm-2017.2.4.tar.gz"

"tar xvf PhpStorm-2017.2.4.tar.gz"

"sudo mv PhpStorm-172.4155.41/ /opt/phpstorm/"

"sudo ln -s /opt/phpstorm/bin/phpstorm.sh /usr/local/bin/phpstorm"

"phpstorm"

Laravel integration:

https://confluence.jetbrains.com/display/PhpStorm/Laravel+Development+using+PhpStorm


===For import=====================================
archive in vagrant ordner kopieren

aus vagrants shell "createdb -h 127.0.0.1 -U homestead -W biigle-tiny"

Passwort "secret"

"zcat ~/biigle/biigle_tiny_dump_2017-10-10.sql.gz | psql -h 127.0.0.1 -U homestead -W biigle-tiny"

Zuletzt die Datenbank in .env ändern:

"DB_DATABASE=biigle-tiny"

reload vm

In der DB gibt es den User "joe@example.com" mit Passwort "joespassword".



====install======
Permission denied (publickey)
https://confluence.atlassian.com/bbkb/permission-denied-publickey-302811860.html
Set up an SSH key
https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html
Troubleshoot SSH issues
https://confluence.atlassian.com/bitbucket/troubleshoot-ssh-issues-271943403.html

Your identification has been saved in /home/fts/.ssh/id_rsa.
Your public key has been saved in /home/fts/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:hd8jXb2qSACUdeAvQmteLGlKEAbRAOzvh/2pYLGulac fts@fts-Lenovo-V110-15ISK
The key's randomart image is:
+---[RSA 2048]----+
|O*   .oo..       |
|..o ... ..     . |
|..   o .. .   . .|
| .. . = .o o .  .|
|  .o B =S.o +  . |
|  ..O + o  . ..  |
|  .Boo   .   .   |
|  +o+o  o . .    |
| ..E..oo . .     |
+----[SHA256]-----+









