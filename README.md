# Biigle Visualisation Module

Install the module:

Add the following to the repositories array of your `composer.json`, 
while replacing `<username>` with Your username or `o1r4w4`:
```
{
  "type": "git",
  "url": "https://o1r4w4@bitbucket.org/<username>/visualisation.git"
}
```

1. Run `php composer.phar require visualisation`.
2. Add `Biigle\Modules\Visualisation\VisualisationServiceProvider::class` 
    to the `providers` array in `config/app.php`.
3. Run `php artisan visualisation:publish` to refresh the public assets of this package.
    Do this for every update of the package.

Branch structure:

- master: stable version for installation
- dev: developement branch for new features
- tests: test branch for javascript unit tests
- thesis: fixed branch for thesis
- usability: branch with usability test features

Folder structure:
   
- src/ -> ServiceProvider
    - Console/ -> console commands for publishing    
    - Http/ -> routes    
        - Controllers/        
            - Api/ -> LabelControler        
            - Views/ -> VolumeControler    
    - public/ -> assets for publishing
        - assets/
            - images/ -> images for manual
            - scripts/ -> minified javascript code of module and adidional libraries  
            - styles/ -> minified css code
        - views/ -> blades
            - manual/ -> manual blades
    - resources/ -> core code
        - assets/ -> javascript and css
            - js/ -> javascript code
                - visualisation/ -> javascript code for modules route
                    - api/ -> api code
                    - classes/ -> handler
                    - components/ -> components 
                - volumes/ -> javascript code for volumes route
            - sass/ -> css code
- tests/ -> php unit tests and results
- tests_js/ -> javascript unit tests and results